///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct  8 2016)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __MAINFRAMEBASE_H__
#define __MAINFRAMEBASE_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/intl.h>
#include <wx/string.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/menu.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/aui/auibook.h>
#include <wx/frame.h>
#include <wx/aui/aui.h>

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class MainFrameBase
///////////////////////////////////////////////////////////////////////////////
class MainFrameBase : public wxFrame 
{
	private:
	
	protected:
		wxMenuBar* m_menuBar;
		wxMenu* m_fileMenu;
		wxMenuItem* m_saveMenuItem;
		wxMenu* m_editMenu;
		wxMenuItem* m_cutMenuItem;
		wxMenuItem* m_copyMenuItem;
		wxMenuItem* m_pasteMenuItem;
		wxMenu* m_projectMenu;
		wxMenuItem* m_addScreenMenuItem;
		wxMenu* m_generateMenu;
		wxMenuItem* m_generateHtmlMenuItem;
		wxMenuItem* m_generateImagesMenuItem;
		wxMenu* m_simulationMenu;
		wxMenuItem* m_simulateMenuItem;
		wxMenu* m_helpMenu;
		wxAuiNotebook* m_canvasAuiNotebook;
		
		// Virtual event handlers, overide them in your derived class
		virtual void OnCloseFrame( wxCloseEvent& event ) { event.Skip(); }
		virtual void OnIdle( wxIdleEvent& event ) { event.Skip(); }
		virtual void OnOpen( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSave( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnExit( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnUndo( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnUpdateUndo( wxUpdateUIEvent& event ) { event.Skip(); }
		virtual void OnRedo( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnUpdateRedo( wxUpdateUIEvent& event ) { event.Skip(); }
		virtual void OnCut( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnUpdateCut( wxUpdateUIEvent& event ) { event.Skip(); }
		virtual void OnCopy( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnUpdateCopy( wxUpdateUIEvent& event ) { event.Skip(); }
		virtual void OnPaste( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnUpdatePaste( wxUpdateUIEvent& event ) { event.Skip(); }
		virtual void OnSelectAll( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnUpdateSelectAll( wxUpdateUIEvent& event ) { event.Skip(); }
		virtual void OnAlignLeft( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnUpdateAlign( wxUpdateUIEvent& event ) { event.Skip(); }
		virtual void OnAlignRight( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnAlignTop( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnAlignBottom( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnNewProject( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnAddScreen( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnGenerateHtml( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnGenerateImages( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSimulate( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnAbout( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnNotebookPageClose( wxAuiNotebookEvent& event ) { event.Skip(); }
		
	
	public:
		
		MainFrameBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("emDesigner"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 800,600 ), long style = wxCLOSE_BOX|wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );
		wxAuiManager m_mgr;
		
		~MainFrameBase();
	
};

#endif //__MAINFRAMEBASE_H__
