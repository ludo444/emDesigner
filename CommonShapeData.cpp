/***************************************************************
 * Name:      CommonShapeData.cpp
 * Purpose:   Implementation of CommonShapeData
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   05/06/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#include "CommonShapeData.h"

XS_IMPLEMENT_CLONABLE_CLASS(CommonShapeData, wxSFShapeBase);

CommonShapeData::CommonShapeData(wxString name) : wxSFShapeBase(), m_name(name)
{
    XS_SERIALIZE(m_name, "shape-name");
}

CommonShapeData::CommonShapeData(const CommonShapeData& obj) : wxSFShapeBase(obj)
{
    m_name = obj.m_name;
}

CommonShapeData::CommonShapeData() : wxSFShapeBase()
{
    XS_SERIALIZE(m_name, "shape-name");
}

CommonShapeData::~CommonShapeData()
{
}

EventType CommonShapeData::GetEventTypeOfShape()
{
    wxSFShapeBase* event = (wxSFShapeBase*) GetFirstChild();
    if(event)
        if(event->GetClassInfo() == CLASSINFO(ScreenChangeEvent))
            return SCREEN_CHANGE_EVENT;
        else if(event->GetClassInfo() == CLASSINFO(BitmapChangeEvent))
            return BITMAP_CHANGE_EVENT;

    return NO_EVENT;
}
