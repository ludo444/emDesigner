/***************************************************************
 * Name:      Event.h
 * Purpose:   Declaration of classes that holds various possible
 *            events that can be mapped
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   05/07/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#ifndef EVENT_H
#define EVENT_H

#include <wx/wxsf/wxShapeFramework.h>
#include "Ids.h"

/**
 * @class ScreenChangeEvent
 * @author Marek Hrušovský
 * @brief Even class for screen change
 */
class ScreenChangeEvent : public wxSFShapeBase
{
private:
    XS_DECLARE_CLONABLE_CLASS(ScreenChangeEvent);
    /**
     * @brief Id of screen to which ScreenChange event can lead to
     */
    int m_newScreenId;

    /**
     * @brief Init function for constructors
     */
    void InitEvent();

public:
    /**
     * @brief Constructor
     */
    ScreenChangeEvent();
    /**
     * @brief Copy constructor
     * @param obj Reference to Event object
     */
    ScreenChangeEvent(const ScreenChangeEvent& obj);
    /**
     * @brief Destructor
     */
    ~ScreenChangeEvent();

    /**
     * @brief
     * @param newScreenId Set id of screen event leads to
     * @return Reference to this
     */
    ScreenChangeEvent& SetNewScreenId(int newScreenId)
    {
        this->m_newScreenId = newScreenId;
        return *this;
    }
    /**
     * @brief Get ID of screen to which event leads to
     * @return ID of screen
     */
    int GetNewScreenId() const
    {
        return m_newScreenId;
    }

};

/**
 * @class BitmapChangeEvent
 * @author Marek Hrušovský
 * @brief Change bitmap for another
 */
class BitmapChangeEvent : public wxSFShapeBase
{
private:
    XS_DECLARE_CLONABLE_CLASS(BitmapChangeEvent);
    /**
     * @brief Path to new bitmap
     */
    wxString m_newBitmapPath;
    /**
     * @brief Path to old bitmap
     */
    wxString m_oldBitmapPath;
    /**
     * @brief Init function for constructors
     */
    void InitEvent();

public:
    /**
     * @brief Constructor
     */
    BitmapChangeEvent();
    /**
     * @brief Constructor
     * @param oldBitmapPath Path to old bitmap
     */
    BitmapChangeEvent(wxString oldBitmapPath);
    /**
     * @brief Copy constructor
     * @param obj Reference to Event object
     */
    BitmapChangeEvent(const BitmapChangeEvent& obj);
    /**
     * @brief Destructor
     */
    ~BitmapChangeEvent();

    /**
     * @brief Make new bitmap path relative to path
     * @param path Path
     */
    void MakeNewPathRelativeTo(const wxString& path);
    /**
     * @brief Set path to new bitmap
     * @param newBitmapPath New bitmap path
     * @return Reference to this
     */
    BitmapChangeEvent& SetNewBitmapPath(const wxString& newBitmapPath)
    {
        this->m_newBitmapPath = newBitmapPath;
        return *this;
    }
    /**
     * @brief Set path to old bitmap
     * @param oldBitmapPath Old bitmap path
     * @return Reference to this
     */
    BitmapChangeEvent& SetOldBitmapPath(const wxString& oldBitmapPath)
    {
        this->m_oldBitmapPath = oldBitmapPath;
        return *this;
    }
    /**
     * @brief Get path to new bitmap
     * @return New bitmap path
     */
    const wxString& GetNewBitmapPath() const
    {
        return m_newBitmapPath;
    }
    /**
     * @brief Get path to old bitmap
     * @return Old bitmap path
     */
    const wxString& GetOldBitmapPath() const
    {
        return m_oldBitmapPath;
    }
};

#endif // EVENT_H
