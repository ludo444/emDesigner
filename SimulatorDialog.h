/***************************************************************
 * Name:      SimulatorDialog.h
 * Purpose:   Declaration of SimulatorDialog for showing HTML generated screens
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   05/01/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#ifndef SIMULATORDIALOG_H
#define SIMULATORDIALOG_H
#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/webview.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/string.h>
#include <wx/sizer.h>
#include <wx/dialog.h>

#include "Project.h"

/**
 * @class SimulatorDialog
 * @author Marek Hrušovský
 * @brief Class handling showing of generated HTML screens in dialog, and works as a simulator
 */
class SimulatorDialog : public wxDialog
{
private:
    /**
     * @brief Showing dynamic HTML content
     */
    wxWebView* m_webView;
    /**
     * @brief On close event
     * @param event Event object
     */
    void OnClose(wxCloseEvent& event);

public:
    /**
     * @brief Constructor
     * @param project Project object
     * @param url URL of HTML document
     * @param parent Parent wxWindow
     * @param id wxID
     * @param title Dialog title
     * @param pos Position
     * @param size Size
     * @param style Style
     */
    SimulatorDialog(Project* project, const wxString& url, wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = "Simulation", const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE);
    /**
     * @brief Destructor
     */
    ~SimulatorDialog();

    /**
     * @brief Get web view
     * @return WebView
     */
    wxWebView* GetWebView()
    {
        return m_webView;
    }
    /**
     * @brief Reload web view without cache
     */
    void HardReload();

};

#endif // SIMULATORDIALOG_H
