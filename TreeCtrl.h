/***************************************************************
 * Name:      TreeCtrl.h
 * Purpose:   TreeCtrl class for showing screens and elements hierarchy
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   05/02/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#ifndef TREECTRL_H
#define TREECTRL_H

#include <wx/treectrl.h>

#include "TreeItem.h"
#include "Project.h"
#include "Propgrid.h"

class MainFrame;
/**
 * @class TreeCtrl
 * @author Marek Hrušovský
 * @brief TreeCtrl showing elements and screens hierarchy
 */
class TreeCtrl : public wxTreeCtrl
{
private:
    /**
     * @brief Main wxFrame of application
     */
    MainFrame* m_mainFrame;
    /**
     * @brief Property grid for showing selected elements/screens
     */
    Propgrid* m_propgrid;
    /**
     * @brief When some tree item is double clicked or activated, selects it or opens tab
     * @param event Event object
     */
    void OnTreeItemActivation(wxTreeEvent& event);
    /**
     * @brief When some tree item is clicked, shows propgrid
     * @param event Event object
     */
    void OnTreeItemSelection(wxTreeEvent& event);
    /**
     * @brief On right click of TreeItem
     * @param event Event object
     */
    void OnTreeItemRightClick(wxTreeEvent& event);
    /**
     * @brief Delete item from TreeCtrl
     * @param event Event object
     */
    void OnDeleteTreeItem(wxCommandEvent& event);

public:
    /**
     * @brief Static variable holding last right clicked item in wxTreeCtrl
     */
    static wxTreeItemId s_lastRightClickedItem;
    /**
     * @brief Static flag used to indicate if right-click menu delete action in wxTreeCtrl was activated, and block some other events if true to avoid SEGFAULT
     */
    static bool s_treeCtrlDeleteActivated;
    /**
     * @brief Constructor
     * @param parent Parent window
     * @param size Size of grid
     */
    TreeCtrl(wxWindow *parent, const wxSize& size = wxDefaultSize);
    /**
     * @brief Destructor
     */
    ~TreeCtrl();
    /**
     * @brief Set TreeCtrl content to project data
     * @param project Project object
     */
    void SetProject(Project* project);
    /**
     * @brief Get wxTreeItemId of shape that belongs to screen
     * @param shape Shape of which wxTreeItemId needs to get
     * @param screenId Id of screen in TreeCtrl
     */
    wxTreeItemId GetShapeTreeItem(wxSFShapeBase* shape, wxTreeItemId screenId);
    /**
     * @brief Process screen shapes and remove deleted ones/add missing ones. Useful for undo/redo operations.
     * @param screenId Id of screen
     * @param screen Pointer to screen
     */
    void ProcessScreenShapes(wxTreeItemId screenId, Screen* screen);

};

#endif // TREECTRL_H
