/***************************************************************
 * Name:      ContentGenerator.cpp
 * Purpose:   Implementation of ContentGenerator
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   04/08/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#include "ContentGenerator.h"

#include "wx/filename.h"

ContentGenerator::ContentGenerator(Project* project) : m_project(project)
{
    // project
    m_projectNameRegEx.Compile("({{[[:space:]]*?project\.name[[:space:]]*?}})", wxRE_ADVANCED + wxRE_ICASE);
    m_projectScreenHeightRegEx.Compile("({{[[:space:]]*?project\.screenHeight[[:space:]]*?}})", wxRE_ADVANCED + wxRE_ICASE);
    m_projectScreenWidthRegEx.Compile("({{[[:space:]]*?project\.screenWidth[[:space:]]*?}})", wxRE_ADVANCED + wxRE_ICASE);

    // screen
    m_screenIdRegEx.Compile("({{[[:space:]]*?screen\.id[[:space:]]*?}})", wxRE_ADVANCED + wxRE_ICASE);
    m_screenNameRegEx.Compile("({{[[:space:]]*?screen\.name[[:space:]]*?}})", wxRE_ADVANCED + wxRE_ICASE);
    m_screenBgColorRegEx.Compile("({{[[:space:]]*?screen\.backgroundColor[[:space:]]*?}})", wxRE_ADVANCED + wxRE_ICASE);
    m_screenDiagramRegEx.Compile("({{[[:space:]]*?screen\.diagramManager[[:space:]]*?}})", wxRE_ADVANCED + wxRE_ICASE);

    // shape
    m_shapeRegEx.Compile("({{[[:space:]]*?shape[[:space:]]*?}})", wxRE_ADVANCED + wxRE_ICASE);
    m_shapeIdRegEx.Compile("({{[[:space:]]*?shape\.id[[:space:]]*?}})", wxRE_ADVANCED + wxRE_ICASE);
    m_shapeTopPositionRegEx.Compile("({{[[:space:]]*?shape\.top[[:space:]]*?}})", wxRE_ADVANCED + wxRE_ICASE);
    m_shapeLeftPositionRegEx.Compile("({{[[:space:]]*?shape\.left[[:space:]]*?}})", wxRE_ADVANCED + wxRE_ICASE);

    // rect shape
    m_rectShapeWidthRegEx.Compile("({{[[:space:]]*?shape\.width[[:space:]]*?}})", wxRE_ADVANCED + wxRE_ICASE);;
    m_rectShapeHeightRegEx.Compile("({{[[:space:]]*?shape\.height[[:space:]]*?}})", wxRE_ADVANCED + wxRE_ICASE);;

    // bitmap shape
    m_bitmapShapePathRegEx.Compile("({{[[:space:]]*?shape\.path[[:space:]]*?}})", wxRE_ADVANCED + wxRE_ICASE);

    // text shape
    m_textShapeFontSizeRegEx.Compile("({{[[:space:]]*?shape\.fontSize[[:space:]]*?}})", wxRE_ADVANCED + wxRE_ICASE);
    m_textShapeFontFaceRegEx.Compile("({{[[:space:]]*?shape\.fontFace[[:space:]]*?}})", wxRE_ADVANCED + wxRE_ICASE);
    m_textShapeFontStyleRegEx.Compile("({{[[:space:]]*?shape\.fontStyle[[:space:]]*?}})", wxRE_ADVANCED + wxRE_ICASE);
    m_textShapeFontWeightRegEx.Compile("({{[[:space:]]*?shape\.fontWeight[[:space:]]*?}})", wxRE_ADVANCED + wxRE_ICASE);
    m_textShapeFontUnderlinedRegEx.Compile("({{[[:space:]]*?shape\.fontUnderlined[[:space:]]*?}})", wxRE_ADVANCED + wxRE_ICASE);
    m_textShapeFontColorRegEx.Compile("({{[[:space:]]*?shape\.fontColor[[:space:]]*?}})", wxRE_ADVANCED + wxRE_ICASE);
    m_textShapeTextRegEx.Compile("({{[[:space:]]*?shape\.text[[:space:]]*?}})", wxRE_ADVANCED + wxRE_ICASE);

    // events
    m_eventScreenChangeIdRegEx.Compile("({{[[:space:]]*?event\.newScreenId[[:space:]]*?}})", wxRE_ADVANCED + wxRE_ICASE);
    m_eventBitmapChangeNewPathRegEx.Compile("({{[[:space:]]*?event\.newPath[[:space:]]*?}})", wxRE_ADVANCED + wxRE_ICASE);;
    m_eventBitmapChangeOldPathRegEx.Compile("({{[[:space:]]*?event\.oldPath[[:space:]]*?}})", wxRE_ADVANCED + wxRE_ICASE);;

}

ContentGenerator::~ContentGenerator()
{
}

void ContentGenerator::FillProjectVariables(wxString& str)
{
    m_projectNameRegEx.Replace(&str, m_project->GetName());
    m_projectScreenHeightRegEx.Replace(&str, wxString::Format(wxT("%i"), m_project->GetScreenHeight()));
    m_projectScreenWidthRegEx.Replace(&str, wxString::Format(wxT("%i"), m_project->GetScreenWidth()));
}

void ContentGenerator::FillScreenVariables(wxString& str, Screen* screen)
{
    m_screenIdRegEx.Replace(&str, wxString::Format(wxT("%i"), screen->GetId()));
    m_screenNameRegEx.Replace(&str, screen->GetName());
    m_screenBgColorRegEx.Replace(&str, screen->GetBackgroundColor().GetAsString(wxC2S_HTML_SYNTAX));
}

void ContentGenerator::FillDiagram(wxString& str, wxString& shapesStr)
{
    m_screenDiagramRegEx.Replace(&str, shapesStr);
}

void ContentGenerator::FillShape(wxString& str, wxSFShapeBase* shape, wxString* shapeStr)
{
    wxPoint point = shape->GetAbsolutePosition();
    m_shapeIdRegEx.Replace(&str, wxString::Format(wxT("%i"), shape->GetId()));
    m_shapeTopPositionRegEx.Replace(&str, wxString::Format(wxT("%i"), point.y));
    m_shapeLeftPositionRegEx.Replace(&str, wxString::Format(wxT("%i"), point.x));

    if(shapeStr != NULL)
        m_shapeRegEx.Replace(&str, *shapeStr);
}

void ContentGenerator::FillRectShape(wxString& str, wxSFRectShape* shape)
{
    wxSize size(shape->GetRectSize().x, shape->GetRectSize().y);
    m_rectShapeWidthRegEx.Replace(&str, wxString::Format(wxT("%i"), size.GetWidth()));
    m_rectShapeHeightRegEx.Replace(&str, wxString::Format(wxT("%i"), size.GetHeight()));
}

void ContentGenerator::FillBitmapShape(wxString& str, BitmapShape* shape, const wxString& pathAppend)
{
    wxFileName bitmapPath(shape->GetBitmapPath());
    bitmapPath.MakeAbsolute(BitmapShape::s_relativePath);
    wxFileName projectPath(m_project->GetPath());
    projectPath.AppendDir(pathAppend);

    bitmapPath.MakeRelativeTo(projectPath.GetFullPath());
    wxString bitmapPathUrl(bitmapPath.GetFullPath());

    m_bitmapShapePathRegEx.Replace(&str, bitmapPathUrl);
}

void ContentGenerator::FillTextShape(wxString& str, TextShape* shape, const wxString* fontStyleArray, const wxString* fontWeightArray, const wxString* underlineIndicator)
{
    wxFont font = shape->GetFont();
    int arrayIndex;
    m_textShapeFontSizeRegEx.Replace(&str, wxString::Format(wxT("%i"), font.GetPointSize()));
    m_textShapeFontFaceRegEx.Replace(&str, font.GetFaceName());

    switch(font.GetStyle())
    {
        case wxFONTSTYLE_NORMAL:
            arrayIndex = 0;
            break;
        case wxFONTSTYLE_ITALIC:
            arrayIndex = 1;
            break;
        case wxFONTSTYLE_SLANT:
            arrayIndex = 2;
            break;
        default:
            arrayIndex = 0;
            break;
    }
    m_textShapeFontStyleRegEx.Replace(&str, fontStyleArray[arrayIndex]);

    switch(font.GetWeight())
    {
        case wxFONTWEIGHT_NORMAL:
            arrayIndex = 0;
            break;
        case wxFONTWEIGHT_LIGHT:
            arrayIndex = 1;
            break;
        case wxFONTWEIGHT_BOLD:
            arrayIndex = 2;
            break;
        default:
            arrayIndex = 0;
            break;
    }
    m_textShapeFontWeightRegEx.Replace(&str, fontWeightArray[arrayIndex]);

    m_textShapeFontUnderlinedRegEx.Replace(&str, underlineIndicator[font.GetUnderlined()]);

    m_textShapeFontColorRegEx.Replace(&str, shape->GetTextColour().GetAsString(wxC2S_HTML_SYNTAX));
    m_textShapeTextRegEx.Replace(&str, shape->GetText());

}

void ContentGenerator::FillScreenChangeEvent(wxString& str, ScreenChangeEvent* event)
{
    int screenId = event->GetNewScreenId();
    if(screenId == m_project->GetIndexScreenId())
        m_eventScreenChangeIdRegEx.Replace(&str, "index");
    else
        m_eventScreenChangeIdRegEx.Replace(&str, wxString::Format(wxT("%i"), event->GetNewScreenId()));
}

void ContentGenerator::FillBitmapChangeEvent(wxString& str, BitmapChangeEvent* event, const wxString& pathAppend)
{
    wxFileName projectPath(m_project->GetPath());
    projectPath.AppendDir(pathAppend);

    wxFileName newBitmapPath(event->GetNewBitmapPath());
    newBitmapPath.MakeAbsolute(BitmapShape::s_relativePath);
    newBitmapPath.MakeRelativeTo(projectPath.GetFullPath());
    m_eventBitmapChangeNewPathRegEx.Replace(&str, newBitmapPath.GetFullPath());

    wxFileName oldBitmapPath(event->GetOldBitmapPath());
    oldBitmapPath.MakeAbsolute(BitmapShape::s_relativePath);
    oldBitmapPath.MakeRelativeTo(projectPath.GetFullPath());
    m_eventBitmapChangeOldPathRegEx.Replace(&str, oldBitmapPath.GetFullPath());
}

void ContentGenerator::FillButtonShape(wxString& str, ButtonShape* shape, const wxString* fontStyleArray, const wxString* fontWeightArray, const wxString* underlineIndicator)
{
    wxFont font = shape->GetFont();
    int arrayIndex;
    m_textShapeFontSizeRegEx.Replace(&str, wxString::Format(wxT("%i"), font.GetPointSize()));
    m_textShapeFontFaceRegEx.Replace(&str, font.GetFaceName());

    switch(font.GetStyle())
    {
        case wxFONTSTYLE_NORMAL:
            arrayIndex = 0;
            break;
        case wxFONTSTYLE_ITALIC:
            arrayIndex = 1;
            break;
        case wxFONTSTYLE_SLANT:
            arrayIndex = 2;
            break;
        default:
            arrayIndex = 0;
            break;
    }
    m_textShapeFontStyleRegEx.Replace(&str, fontStyleArray[arrayIndex]);

    switch(font.GetWeight())
    {
        case wxFONTWEIGHT_NORMAL:
            arrayIndex = 0;
            break;
        case wxFONTWEIGHT_LIGHT:
            arrayIndex = 1;
            break;
        case wxFONTWEIGHT_BOLD:
            arrayIndex = 2;
            break;
        default:
            arrayIndex = 0;
            break;
    }
    m_textShapeFontWeightRegEx.Replace(&str, fontWeightArray[arrayIndex]);

    m_textShapeFontUnderlinedRegEx.Replace(&str, underlineIndicator[font.GetUnderlined()]);

    m_textShapeFontColorRegEx.Replace(&str, shape->GetTextColour().GetAsString(wxC2S_HTML_SYNTAX));
    m_textShapeTextRegEx.Replace(&str, shape->GetText());
}
