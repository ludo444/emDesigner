/***************************************************************
 * Name:      NewProjectDialog.h
 * Purpose:   New project dialog
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   02/24/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#ifndef NEWPROJECTDIALOG_H
#define NEWPROJECTDIALOG_H

#include "NewProjectDialogBase.h"
#include "Project.h"

/**
 * @class NewProjectDialog
 * @author Marek Hrušovský
 * @brief Class declaring dialog for setting up new project
 */
class NewProjectDialog : public NewProjectDialogBase
{
private:
    /**
     * @brief Pointer to new project
     */
    Project* m_project;
    /**
     * @brief On create project event, creates project instance and inits it
     * @param event Event object
     */
    void OnCreate(wxCommandEvent& event);
    /**
     * @brief Processing closing of dialog event
     * @param event Event object
     */
    void OnClose(wxCloseEvent& event);

public:
    /**
     * @brief Constructor
     * @param parent Parent wxWindow
     * @param title Title of dialog
     * @param id Id of wxWindow
     * @param pos Position of dialog
     * @param size Size of dialog
     * @param style Specified style of dialog
     */
    NewProjectDialog(wxWindow* parent = NULL, const wxString& title = "New Project", wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE);
    /**
     * @brief Destructor
     */
    ~NewProjectDialog();
    /**
     * @brief Getting project
     * @return Project object
     */
    Project* GetProject()
    {
        return m_project;
    }
};

#endif // NEWPROJECTDIALOG_H
