/***************************************************************
 * Name:      PropgridItem.cpp
 * Purpose:   Implementation of property grid container
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   03/20/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#include "PropgridItem.h"

#include "main.h"
#include "TreeCtrl.h"
#include "Event.h"

PropgridItem::PropgridItem()
{
    m_mainFrame = (MainFrame*) wxGetApp().GetTopWindow();
    ClearData();
}

PropgridItem::~PropgridItem()
{
}

void PropgridItem::ClearData()
{
    m_itemType = TYPE_NONE;
    m_workPanel = NULL;
    m_screen = NULL;
    m_treeItem = NULL;
    m_shape = NULL;
}

PropgridItem& PropgridItem::SetScreen(Screen* screen, wxTreeItemId treeItem, WorkPanel* workPanel)
{
    ClearData();
    m_treeItem = treeItem;
    m_itemType = TYPE_SCREEN;
    m_screen = screen;
    m_workPanel = workPanel;
    return *this;
}

void PropgridItem::ScreenPropertyChange(wxPropertyGridEvent& event)
{
    wxPGProperty* property = event.GetProperty();
    wxAny value = property->GetValue();

    if(property->GetName() == "Name")
    {
        wxString name(value.As<wxString>());
        m_screen->SetName(name);
        m_mainFrame->GetScreensTreeCtrl()->SetItemText(m_treeItem, name);
        if(m_workPanel)
            m_mainFrame->SetCanvasAuiNotebookPageName(m_workPanel, name);
    }
    else if(property->GetName() == "Background color")
    {
        wxColor color(value.As<wxColor>());
        m_screen->SetBackgroundColor(color);
        if(m_workPanel)
            m_workPanel->SetBackgroundColor(color);
    }
    else if(property->GetName() == "Index screen")
    {
        bool isIndexScreen = value.As<bool>();
        if(isIndexScreen)
            m_mainFrame->GetProject()->SetIndexScreenId(m_screen->GetId());
    }
}

void PropgridItem::RemoveWorkPanel(Screen* screen)
{
    if(screen == m_screen)
        m_workPanel = NULL;
}

PropgridItem& PropgridItem::SetShape(wxSFShapeBase* shape, wxTreeItemId treeItem, Screen* screen, WorkPanel* workPanel)
{
    ClearData();
    m_treeItem = treeItem;
    m_shape = shape;
    m_screen = screen;
    m_workPanel = workPanel;

    if(shape->GetClassInfo() == CLASSINFO(BitmapShape))
        m_itemType = TYPE_BITMAP_SHAPE;
    else if(shape->GetClassInfo() == CLASSINFO(TextShape))
        m_itemType = TYPE_TEXT_SHAPE;
    else if(shape->GetClassInfo() == CLASSINFO(ButtonShape))
        m_itemType = TYPE_BUTTON_SHAPE;
    else
        m_itemType = TYPE_SHAPE;

    return *this;
}

void PropgridItem::ShapePropertyChange(wxPropertyGridEvent& event)
{
    wxPGProperty* property = event.GetProperty();
    wxString propName = property->GetName();
    wxAny value = property->GetValue();

    if(propName == "Name")
    {
        wxString name(value.As<wxString>());
        ((CommonShapeData*) m_shape->GetFirstChild())->SetName(name);
        m_mainFrame->GetScreensTreeCtrl()->SetItemText(m_treeItem, name);
    }
    else if(propName == "Position X")
    {
        int x = value.As<int>();
        m_shape->MoveTo(x, m_shape->GetAbsolutePosition().y);
    }
    else if(propName == "Position Y")
    {
        int y = value.As<int>();
        m_shape->MoveTo(m_shape->GetAbsolutePosition().x, y);
    }
    else if(propName == "Events")
        EventsPropertyChange(property);
    else if(propName == "Events.Screens")
        EventScreensPropertyChange(property);
    else
    {
        switch(m_itemType)
        {
            case TYPE_BUTTON_SHAPE:
            case TYPE_TEXT_SHAPE:
                FontsPropertyChange(property);

            case TYPE_BITMAP_SHAPE:
                ShapeSizePropertyChange(property);
                BitmapEventPropertyChange(property);
                break;
        }
    }

    if(m_workPanel)
    {
        m_shape->Refresh();
        m_workPanel->GetScreenCanvas()->Refresh();
    }
}

void PropgridItem::ShapeSizePropertyChange(wxPGProperty* property)
{
    wxString propName = property->GetName();
    wxSFRectShape* shape = (wxSFRectShape*) m_shape;
    int size;
    if(propName == "Width" || propName == "Height")
        size = ((wxAny) property->GetValue()).As<int>();

    if(propName == "Width")
        shape->Scale(size/shape->GetRectSize().x, 1.);
    else if(propName == "Height")
        shape->Scale(1., size/shape->GetRectSize().y);
}

void PropgridItem::EventsPropertyChange(wxPGProperty* property)
{
    EventType eventType = (EventType)((wxAny) property->GetValue()).As<int>();
    CommonShapeData* shapeData = (CommonShapeData*) m_shape->GetFirstChild();
    switch(eventType)
    {
        case SCREEN_CHANGE_EVENT:
        {
            shapeData->AddChild(new ScreenChangeEvent());
            m_mainFrame->GetElementsPropgrid()->AppendScreenChangeEventProp(property);
        }
        break;

        case BITMAP_CHANGE_EVENT:
        {
            shapeData->AddChild(new BitmapChangeEvent(((BitmapShape*) m_shape)->GetBitmapPath()));
            m_mainFrame->GetElementsPropgrid()->AppendBitmapChangeEventProp(property);
        }
        break;

        case NO_EVENT:
        default:
        {
            shapeData->RemoveChildren();
            property->DeleteChildren();
        }
        break;
    }
    m_mainFrame->GetElementsPropgrid()->Refresh();
}

wxEnumProperty* PropgridItem::GetScreensArrayProperty()
{
    wxArrayString screensArray;
    wxArrayInt idsArray;
    int screenId;

    const SerializableList& screenList = m_mainFrame->GetProject()->GetScreenList();
    for(SerializableList::const_iterator i = screenList.begin(); i != screenList.end(); i++)
    {
        Screen* screen = (Screen*) *i;
        screensArray.Add(screen->GetName());
        idsArray.Add(screen->GetId());
        screenId = screen->GetId();
    }

    CommonShapeData* shapeData = (CommonShapeData*) m_shape->GetFirstChild();
    if(shapeData->GetEventTypeOfShape() == SCREEN_CHANGE_EVENT)
        screenId = ((ScreenChangeEvent*) shapeData->GetFirstChild())->GetNewScreenId();

    return new wxEnumProperty("Screens", "Screens", screensArray, idsArray, screenId);
}

wxImageFileProperty* PropgridItem::GetImagePathProperty()
{
    CommonShapeData* shapeData = (CommonShapeData*) m_shape->GetFirstChild();
    wxString newBitmapPath;
    if(shapeData->GetEventTypeOfShape() == BITMAP_CHANGE_EVENT)
        newBitmapPath = ((BitmapChangeEvent*) shapeData->GetFirstChild())->GetNewBitmapPath();

    wxFileName filename(newBitmapPath);
    filename.MakeAbsolute(BitmapShape::s_relativePath);
    wxImageFileProperty* imageProp = new wxImageFileProperty("New bitmap", "New bitmap", filename.GetFullPath());
    imageProp->SetAttribute("Wildcard", IMAGE_WILDCARD);
    return imageProp;
}

void PropgridItem::EventScreensPropertyChange(wxPGProperty* property)
{
    int screenId = ((wxAny) property->GetValue()).As<int>();
    CommonShapeData* shapeData = (CommonShapeData*) m_shape->GetFirstChild();
    ScreenChangeEvent* event = (ScreenChangeEvent*) shapeData->GetFirstChild();
    event->SetNewScreenId(screenId);
}

void PropgridItem::FontsPropertyChange(wxPGProperty* property)
{
    wxString propName = property->GetName();
    wxAny value = property->GetValue();

    if(propName == "Text")
    {
        wxString text(value.As<wxString>());
        switch(m_itemType)
        {
            case TYPE_BUTTON_SHAPE:
                ((ButtonShape*) m_shape)->SetText(text);
                break;

            case TYPE_TEXT_SHAPE:
                ((TextShape*) m_shape)->SetText(text);
                break;
        }
    }
    else if(propName == "Font")
    {
        wxFont font(value.As<wxFont>());
        switch(m_itemType)
        {
            case TYPE_BUTTON_SHAPE:
                ((ButtonShape*) m_shape)->SetFont(font);
                break;

            case TYPE_TEXT_SHAPE:
                ((TextShape*) m_shape)->SetFont(font);
                break;
        }
    }
    else if(propName == "Font color")
    {
        wxColor color(value.As<wxColor>());
        switch(m_itemType)
        {
            case TYPE_BUTTON_SHAPE:
                ((ButtonShape*) m_shape)->SetTextColour(color);
                break;

            case TYPE_TEXT_SHAPE:
                ((TextShape*) m_shape)->SetTextColour(color);
                break;
        }
    }
}

void PropgridItem::BitmapEventPropertyChange(wxPGProperty* property)
{
    wxString propName = property->GetName();
    wxAny value = property->GetValue();
    CommonShapeData* shapeData = (CommonShapeData*) m_shape->GetFirstChild();

    if(propName == "Events.New bitmap")
    {
        BitmapChangeEvent* event = (BitmapChangeEvent*) shapeData->GetFirstChild();
        event->SetNewBitmapPath(value.As<wxString>());
        event->MakeNewPathRelativeTo(BitmapShape::s_relativePath);
    }
}
