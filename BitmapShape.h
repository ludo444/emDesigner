/***************************************************************
 * Name:      BitmapShape.h
 * Purpose:   Edited shape for insterting bitmap to canvas
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   02/28/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#ifndef BITMAPSHAPE_H
#define BITMAPSHAPE_H

#include <wx/wxsf/wxShapeFramework.h>
#include "CommonShapeData.h"

#define IMAGE_WILDCARD "Image files|*.bmp;*.gif;*.png;*.jpg;*.jpeg;*.pcx;*.tif,*.tiff|BMP files (*.bmp)|*.bmp|GIF files (*.gif)|*.gif|PNG files (*.png)|*.png|JPEG files (*.jpg, *.jpeg)|*.jpg;*.jpeg|PCX files (*.pcx)|*.pcx|TIFF files (*.tiff;*.tif)|*.tiff;*.tif|Any file (*.*)|*"

/**
 * @class BitmapShape
 * @author Marek Hrušovský
 * @brief Shape for designer that contains bitmap
 */
class BitmapShape : public wxSFBitmapShape
{
private:
    /**
     * @brief Init function for constructors
     */
    void InitShape();

public:
    XS_DECLARE_CLONABLE_CLASS(BitmapShape);
    /**
     * @brief Array of possible events
     */
    static EventType s_events[];
    /**
     * @brief Count of possible events
     */
    static int s_eventsCount;
    /**
     * @brief Path to which are bitmaps images paths relative
     */
    static wxString s_relativePath;
    /**
     * @brief Default constructor
     */
    BitmapShape();
    /**
     * @brief User constructor
     * @param pos Position
     * @param bitmapPath Path to bitmap
     * @param manager Diagram manager
     */
    BitmapShape(const wxRealPoint& pos, const wxString& bitmapPath, wxSFDiagramManager* manager);
    /*!
     * \brief Copy constructor
     * \param obj Source shape
     */
    BitmapShape(const BitmapShape& obj);
    /**
     * @brief Destructor
     */
    ~BitmapShape();
    /**
     * @brief Create bitmap shape from file image
     * @param file Path to file
     * @param type Type of file it should open
     * @return True, if successfully loaded
     */
    bool CreateFromFile(const wxString &file, wxBitmapType type = wxBITMAP_TYPE_ANY);
    /**
     * @brief Process bitmap shape after project loaded from file to preserve sizing information
     * @param path Path to file
     */
    void PostProcessBitmap(const wxString& path);

};

#endif // BITMAPSHAPE_H
