/***************************************************************
 * Name:      Screen.h
 * Purpose:   Declaration of class holding screen information
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   02/23/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#ifndef SCREEN_H
#define SCREEN_H

#include <wx/wxsf/DiagramManager.h>

/**
 * @class Screen
 * @author Marek Hrušovský
 * @brief Class holding information about each designed screen
 */
class Screen : public xsSerializable
{
public:
    DECLARE_DYNAMIC_CLASS(Screen);
    /**
    * @brief Max ID ever assigned to Screen in current Project
    */
    static int s_maxId;

private:
    /**
     * @brief Id of screen
     */
    int m_id;
    /**
     * @brief Name of screen
     */
    wxString m_name;
    /**
     * @brief Background color
     */
    wxColour m_backgroundColor;
    /**
    * @brief Diagram manager of chart/screen
    */
    wxSFDiagramManager* m_diagramManager;
    /**
     * @brief Initialize screen properties
     */
    void InitScreen();
    /**
     * @brief Set ID of new screen
     */
    void SetNewScreenId();

public:
    /**
     * @brief Default constructor
     */
    Screen();
    /**
     * @brief Constructor
     * @param name Name of screen
     * @param diagramManager Pointer to diagram manager
     * @param backgroundColor Background color of Screen
     */
    Screen(wxString name, wxSFDiagramManager* diagramManager = new wxSFDiagramManager(), wxColour backgroundColor = wxColour(0, 0, 0));
    /**
     * @brief Destructor
     */
    ~Screen();
    /**
     * @brief Generate proper filename for Screen
     * @return Filename string
     */
    wxString GetFilename();
    /**
     * @brief Process screen and its children after all variables were initialized/deserialized
     */
    void PostProcessScreen();
    /**
     * @brief Set background color
     * @param backgroundColor Background color
     * @return Reference to this
     */
    Screen& SetBackgroundColor(const wxColour& backgroundColor)
    {
        this->m_backgroundColor = backgroundColor;
        return *this;
    }
    /**
     * @brief Set DiagramManager
     * @param diagramManager Diagram manager pointer
     * @return Reference to this
     */
    Screen& SetDiagramManager(wxSFDiagramManager* diagramManager)
    {
        this->m_diagramManager = diagramManager;
        return *this;
    }
    /**
     * @brief Set name of screen
     * @param name Name
     * @return Reference to this
     */
    Screen& SetName(const wxString& name)
    {
        this->m_name = name;
        return *this;
    }
    /**
     * @brief Get background color
     * @return Background color
     */
    const wxColour& GetBackgroundColor() const
    {
        return m_backgroundColor;
    }
    /**
     * @brief Get DiagramManager
     * @return DiagramManager
     */
    wxSFDiagramManager* GetDiagramManager()
    {
        return m_diagramManager;
    }
    /**
     * @brief Get name of screen
     * @return Name wxString
     */
    const wxString& GetName() const
    {
        return m_name;
    }
    /**
     * @brief Get id of screen
     * @return Id of screen
     */
    int GetId()
    {
        return m_id;
    }
};

#endif // SCREEN_H
