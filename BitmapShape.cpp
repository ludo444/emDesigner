/***************************************************************
 * Name:      BitmapShape.cpp
 * Purpose:   Implementation of BitmapShape
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   02/28/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#include "BitmapShape.h"

#include "wx/filename.h"

XS_IMPLEMENT_CLONABLE_CLASS(BitmapShape, wxSFBitmapShape);

EventType BitmapShape::s_events[] = {NO_EVENT, SCREEN_CHANGE_EVENT, BITMAP_CHANGE_EVENT};

int BitmapShape::s_eventsCount = 3;

wxString BitmapShape::s_relativePath = wxGetCwd();

BitmapShape::BitmapShape() : wxSFBitmapShape()
{
    InitShape();
}

BitmapShape::BitmapShape(const wxRealPoint& pos, const wxString& bitmapPath, wxSFDiagramManager* manager) : wxSFBitmapShape(pos, bitmapPath, manager)
{
    InitShape();
}

BitmapShape::BitmapShape(const BitmapShape& obj) : wxSFBitmapShape(obj)
{
    InitShape();
}

BitmapShape::~BitmapShape()
{
}

void BitmapShape::InitShape()
{
    AddStyle(sfsEMIT_EVENTS);
}

bool BitmapShape::CreateFromFile(const wxString& file, wxBitmapType type)
{
    wxFileName path(file);
    path.MakeAbsolute(BitmapShape::s_relativePath);
    bool loaded = wxSFBitmapShape::CreateFromFile(path.GetFullPath(), type);
    path.MakeRelativeTo(BitmapShape::s_relativePath);
    m_sBitmapPath = path.GetFullPath();

    wxSFShapeCanvas* canvas = GetParentCanvas();
    if(canvas)
        RescaleImage(wxRealPoint(GetBoundingBox().width, GetBoundingBox().height));

    return loaded;
}

void BitmapShape::PostProcessBitmap(const wxString& path)
{
    wxRealPoint origSize(m_nRectSize);
    CreateFromFile(path);

    Scale(origSize.x/m_Bitmap.GetWidth(), origSize.y/m_Bitmap.GetHeight());
    m_nRectSize = origSize;
}
