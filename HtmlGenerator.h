/***************************************************************
 * Name:      HtmlGenerator.h
 * Purpose:   Generator of HTML from project
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   03/30/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#ifndef HTMLGENERATOR_H
#define HTMLGENERATOR_H

#include "Project.h"
#include "BitmapShape.h"
#include "ContentGenerator.h"
#include <wx/filename.h>

/**
 * @class HtmlGenerator
 * @author Marek Hrušovský
 * @brief Class used for generating HTML, CSS and JavaScript code based on project
 */
class HtmlGenerator : public ContentGenerator
{
private:
    /**
     * @brief Path to template files
     */
    static wxString s_templatePath;
    /**
     * @brief Names/text strings of wxFontStyle in CSS
     */
    static wxString s_fontStyleStrings[];
    /**
     * @brief Names/text strings of wxFontWeight in CSS
     */
    static wxString s_fontWeightStrings[];
    /**
     * @brief Names/text strings of underlining text decoration in CSS
     */
    static wxString s_fontUnderlineStrings[];
    /**
     * @brief True if generator initialization successful
     */
    bool m_generatorLoaded;
    /**
     * @brief Path were to store generated files
     */
    wxFileName m_generatorPath;

    ////////////////////////////////////////////////////////////////////////////
    // HTML TEMPLATES
    /**
     * @brief Template HTML string
     */
    wxString m_templateHtml;
    /**
     * @brief Bitmap shape HTML template string
     */
    wxString m_bitmapHtmlTemplate;
    /**
     * @brief Text shape HTML template string
     */
    wxString m_textHtmlTemplate;
    /**
     * @brief Screen change HTML template string
     */
    wxString m_screenChangeHtmlTemplate;
    /**
     * @brief Bitmap change HTML template
     */
    wxString m_bitmapChangeHtmlTemplate;
    /**
     * @brief Button shape HTML template string
     */
    wxString m_buttonHtmlTemplate;

    ////////////////////////////////////////////////////////////////////////////
    // CSS TEMPLATES
    /**
     * @brief Template CSS string
     */
    wxString m_templateCss;
    /**
     * @brief Bitmap shape CSS template string
     */
    wxString m_bitmapCssTemplate;
    /**
     * @brief Text shape CSS template string
     */
    wxString m_textCssTemplate;
    /**
     * @brief Bitmap change CSS template string
     */
    wxString m_bitmapChangeCssTemplate;
    /**
     * @brief Button shape CSS template string
     */
    wxString m_buttonCssTemplate;

    ////////////////////////////////////////////////////////////////////////////
    // JS TEMPLATES
    /**
     * @brief Bitmap change JS template
     */
    wxString m_bitmapChangeJsTemplate;

public:
    /**
     * @brief Constructor
     * @param project Pointer to project
     */
    HtmlGenerator(Project* project);
    /**
     * @brief Destructor
     */
    ~HtmlGenerator();
    /**
     * @brief Checks if generator template files are loaded
     * @return True if all files loaded
     */
    bool IsGeneratorLoaded() const
    {
        return m_generatorLoaded;
    }
    /**
     * @brief Get path to index document
     * @return String with path to index
     */
    wxString GetIndexUrl();
    /**
     * @brief Get browser path to index document
     * @return Path to index in web browser format, with file:// prefix
     */
    wxString GetValidIndexUrl();
    /**
     * @brief Generate HTML/CSS of Project
     * @return True, if success
     */
    bool GenerateProject();
    /**
     * @brief Generate HTML/CSS of Screen
     * @param screen Screen object
     * @return True, if success
     */
    bool GenerateScreen(Screen* screen);

    ////////////////////////////////////////////////////////////////////////////
    // HTML GENERATION
    /**
     * @brief Generate HTML of BitmapShape
     * @param shape BitmapShape
     * @param screen Screen this shape belongs to
     * @return Generated HTML code in string
     */
    wxString GenerateHtmlBitmap(BitmapShape* shape, Screen* screen);
    /**
     * @brief Generate HTML of TextShape
     * @param shape TextShape
     * @param screen Screen this shape belongs to
     * @return Generated HTML code in string
     */
    wxString GenerateHtmlText(TextShape* shape, Screen* screen);
    /**
     * @brief Generate HTML of ButtonShape
     * @param shape ButtonShape
     * @param screen Screen this shape belongs to
     * @return Generated HTML code in string
     */
    wxString GenerateHtmlButton(ButtonShape* shape, Screen* screen);
    /**
     * @brief Generate HTML of events
     * @param shape Shape
     * @param screen Screen the shape belongs to
     * @param shapeStr String already generated that needs to be capsulated by event
     * @return Generated HTML code in string
     */
    wxString GenerateHtmlEvents(wxSFShapeBase* shape, Screen* screen, wxString& shapeStr);

    ////////////////////////////////////////////////////////////////////////////
    // CSS GENERATION
    /**
     * @brief Generate CSS of BitmapShape
     * @param shape BitmapShape
     * @param screen Screen this shape belongs to
     * @return Generated CSS code in string
     */
    wxString GenerateCssBitmap(BitmapShape* shape, Screen* screen);
    /**
     * @brief Generate CSS of TextShape
     * @param shape TextShape
     * @param screen Screen this shape belongs to
     * @return Generated CSS code in string
     */
    wxString GenerateCssText(TextShape* shape, Screen* screen);
    /**
     * @brief Generate CSS of ButtonShape
     * @param shape ButtonShape
     * @param screen Screen this shape belongs to
     * @return Generated CSS code in string
     */
    wxString GenerateCssButton(ButtonShape* shape, Screen* screen);
    /**
     * @brief Generate CSS of events
     * @param shape Shape
     * @param screen Screen the shape belongs to
     * @param shapeStr String already generated that needs to be capsulated by event
     * @return Generated CSS code in string
     */
    wxString GenerateCssEvents(wxSFShapeBase* shape, Screen* screen, wxString& shapeStr);

    ////////////////////////////////////////////////////////////////////////////
    // JS GENERATION
    /**
     * @brief Generate JS of events
     * @param shape Shape
     * @param screen Screen the shape belongs to
     * @param shapeStr String already generated that needs to be capsulated by event
     * @return Generated JS code in string
     */
    wxString GenerateJsEvents(wxSFShapeBase* shape, Screen* screen, wxString& shapeStr);
};

#endif // HTMLGENERATOR_H
