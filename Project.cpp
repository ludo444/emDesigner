/***************************************************************
 * Name:      Project.cpp
 * Purpose:   Implementation of Project class
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   02/09/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#include "Project.h"

#include "BitmapShape.h"

IMPLEMENT_DYNAMIC_CLASS(Project, xsSerializable);

Project::Project(wxString name, wxString path, int screenHeight, int screenWidth) : xsSerializable(), m_screenHeight(screenHeight), m_screenWidth(screenWidth), m_name(name), m_path(path)
{
    InitProject();
    BitmapShape::s_relativePath = path;
}

Project::Project() : xsSerializable()
{
    InitProject();
}

void Project::InitProject()
{
    m_isModified = true;
    XS_SERIALIZE(m_name, "project-name");
    XS_SERIALIZE_INT(m_screenHeight, "screen-height");
    XS_SERIALIZE_INT(m_screenWidth, "screen-width");
    XS_SERIALIZE_INT(m_indexScreenId, "index-screen-id");
}

Project::~Project()
{
    RemoveChildren();
}

Screen* Project::AddScreen(wxString name)
{
    Screen* screen;
    int count = GetChildrenList().GetCount();
    if(!name.IsEmpty())
        screen = new Screen(name);
    else
    {
        wxString number = wxString::Format("%i", count+1);
        screen = new Screen(wxString("Screen ").Append(number));
        if(count == 0)
            m_indexScreenId = screen->GetId();
    }
    AddChild(screen);
    m_isModified = true;
    return screen;
}

Project& Project::SetPath(const wxString& path)
{
    this->m_path = path;
    BitmapShape::s_relativePath = path;
    return *this;
}

void Project::PostProcessProject()
{
    SerializableList screens;
    GetChildren(CLASSINFO(Screen), screens);
    for(SerializableList::const_iterator i = screens.begin(); i != screens.end(); i++)
    {
        Screen* screen = (Screen*) *i;
        screen->PostProcessScreen();
    }
}

bool Project::IsProjectModified()
{
    bool isModified = false;
    SerializableList& screenList = GetScreenList();
    for(SerializableList::const_iterator i = screenList.begin(); i != screenList.end(); i++)
    {
        Screen* screen = (Screen*) *i;

        if(screen->GetDiagramManager()->IsModified())
        {
            isModified = true;
            break;
        }
    }

    if(m_isModified)
        isModified = true;

    return isModified;
}
