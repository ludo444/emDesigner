/***************************************************************
 * Name:      ContentGenerator.h
 * Purpose:   Class intented for replacing of parts of string with
 *            values from Project
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   04/08/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#ifndef CONTENTGENERATOR_H
#define CONTENTGENERATOR_H

#include <wx/regex.h>
#include "Project.h"
#include "BitmapShape.h"
#include "TextShape.h"
#include "ButtonShape.h"

/**
 * @class ContentGenerator
 * @author Marek Hrušovský
 * @brief Class intented for replacing of parts of string with values from Project
 */
class ContentGenerator
{
private:
    ////////////////////////////////////////////////////////////////////////////
    // PROJECT REGEX
    /**
     * @brief RegEx for Project name
     */
    wxRegEx m_projectNameRegEx;
    /**
     * @brief RegEx for Project Screen height
     */
    wxRegEx m_projectScreenHeightRegEx;
    /**
     * @brief RegEx for Project Screen width
     */
    wxRegEx m_projectScreenWidthRegEx;

    ////////////////////////////////////////////////////////////////////////////
    // SCREEN REGEX
    /**
     * @brief RegEx for Screen id
     */
    wxRegEx m_screenIdRegEx;
    /**
     * @brief RegEx for Screen name
     */
    wxRegEx m_screenNameRegEx;
    /**
     * @brief RegEx for Screen background color
     */
    wxRegEx m_screenBgColorRegEx;
    /**
     * @brief RegEx for Screen shapes/diagram
     */
    wxRegEx m_screenDiagramRegEx;

    ////////////////////////////////////////////////////////////////////////////
    // SHAPES/DIAGRAM REGEX
    /**
     * @brief RegEx for Shape insert
     */
    wxRegEx m_shapeRegEx;
    /**
     * @brief RegEx for Shape id
     */
    wxRegEx m_shapeIdRegEx;
    /**
     * @brief RegEx for top position of shape
     */
    wxRegEx m_shapeTopPositionRegEx;
    /**
     * @brief RegEx for left position of shape
     */
    wxRegEx m_shapeLeftPositionRegEx;

    // RECT SHAPES REGEX
    /**
     * @brief RegEx for width of shape
     */
    wxRegEx m_rectShapeWidthRegEx;
    /**
     * @brief RegEx for height of shape
     */
    wxRegEx m_rectShapeHeightRegEx;

    // BITMAP SHAPES REGEX
    /**
     * @brief RegEx for Bitmap image path
     */
    wxRegEx m_bitmapShapePathRegEx;

    // TEXT SHAPES REGEX
    /**
     * @brief RegEx for font size
     */
    wxRegEx m_textShapeFontSizeRegEx;
    /**
     * @brief RegEx for font face
     */
    wxRegEx m_textShapeFontFaceRegEx;
    /**
     * @brief RegEx for font style
     */
    wxRegEx m_textShapeFontStyleRegEx;
    /**
     * @brief RegEx for font weight
     */
    wxRegEx m_textShapeFontWeightRegEx;
    /**
     * @brief RegEx for font underlining
     */
    wxRegEx m_textShapeFontUnderlinedRegEx;
    /**
     * @brief RegEx for font color
     */
    wxRegEx m_textShapeFontColorRegEx;
    /**
     * @brief RegEx for text content
     */
    wxRegEx m_textShapeTextRegEx;

    ////////////////////////////////////////////////////////////////////////////
    // EVENTS REGEX
    /**
     * @brief RegEx for Screen id to which shape action leads
     */
    wxRegEx m_eventScreenChangeIdRegEx;
    /**
     * @brief RegEx for image path to new path
     */
    wxRegEx m_eventBitmapChangeNewPathRegEx;
    /**
     * @brief RegEx for image path to old path
     */
    wxRegEx m_eventBitmapChangeOldPathRegEx;

protected:
    /**
     * @brief Project object
     */
    Project* m_project;

public:
    /**
     * @brief Constructor
     * @param project Pointer to project
     */
    ContentGenerator(Project* project);
    /**
     * @brief Destructor
     */
    ~ContentGenerator();
    /**
     * @brief Replace Project variables in provided wxString
     * @param str Edited string
     */
    void FillProjectVariables(wxString& str);
    /**
     * @brief Replace Screen variables in provided wxString
     * @param str Edited string
     * @param screen Screen object pointer
     */
    void FillScreenVariables(wxString& str, Screen* screen);
    /**
     * @brief Fill diagram in str with shapes
     * @param str Edited string
     * @param shapesStr String with generated shapes
     */
    void FillDiagram(wxString& str, wxString& shapesStr);
    /**
     * @brief Fill str with general shape variables
     * @param str Edited string
     * @param shape Shape
     * @param shapeStr In case of filling entired already generated shape occurence
     */
    void FillShape(wxString& str, wxSFShapeBase* shape, wxString* shapeStr = NULL);
    /**
     * @brief Fill str with rect shape variables
     * @param str Edited string
     * @param shape Shape
     */
    void FillRectShape(wxString& str, wxSFRectShape* shape);
    /**
     * @brief Fill str with BitmapShape variables
     * @param str Edited string
     * @param shape BitmapShape
     * @param pathAppend Subfolder of bitmap relative path
     */
    void FillBitmapShape(wxString& str, BitmapShape* shape, const wxString& pathAppend);
    /**
     * @brief Fill str with with TextShape variables
     * @param str Edited string
     * @param shape TextShape
     * @param fontStyleArray Array of font style namings where index is fitted to wxFontStyle enum
     * @param fontWeightArray Array of font weight namings where index is fitted to wxFontWeight enum
     * @param underlineIndicator Array of two possible underlining indicators
     */
    void FillTextShape(wxString& str, TextShape* shape, const wxString* fontStyleArray, const wxString* fontWeightArray, const wxString* underlineIndicator);
    /**
     * @brief Fill str with with ButtonShape variables
     * @param str Edited string
     * @param shape ButtonShape
     * @param fontStyleArray Array of font style namings where index is fitted to wxFontStyle enum
     * @param fontWeightArray Array of font weight namings where index is fitted to wxFontWeight enum
     * @param underlineIndicator Array of two possible underlining indicators
     */
    void FillButtonShape(wxString& str, ButtonShape* shape, const wxString* fontStyleArray, const wxString* fontWeightArray, const wxString* underlineIndicator);
    /**
     * @brief Fill str with ScreenChangeEvent variables
     * @param str Edited string
     * @param event Event object
     */
    void FillScreenChangeEvent(wxString& str, ScreenChangeEvent* event);
    /**
     * @brief Fill str with BitmapChangeEvent variables
     * @param str Edited string
     * @param event Event object
     * @param pathAppend Subfolder of bitmap relative path
     */
    void FillBitmapChangeEvent(wxString& str, BitmapChangeEvent* event, const wxString& pathAppend);
};

#endif // CONTENTGENERATOR_H
