/***************************************************************
 * Name:      CommonShapeData.h
 * Purpose:   Declaration of CommonShapeData,
 *            which are shared across all shapes
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   05/06/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#ifndef COMMONSHAPEDATA_H
#define COMMONSHAPEDATA_H

#include <wx/wxsf/wxShapeFramework.h>
#include "Ids.h"
#include "Event.h"

/**
 * @class CommonShapeData
 * @author Marek Hrušovský
 * @brief Container for data and variables that are common for all used shapes
 */
class CommonShapeData : public wxSFShapeBase
{
public:
    XS_DECLARE_CLONABLE_CLASS(CommonShapeData);

private:
    /**
     * @brief Name of shape
     */
    wxString m_name;

public:
    /**
     * @brief Default constructor
     */
    CommonShapeData();
    /**
     * @brief Constructor
     * @param name Name of shape
     */
    CommonShapeData(wxString name);
    /**
     * @brief Copy constructor
     * @param obj Reference to CommonShapeData object
     */
    CommonShapeData(const CommonShapeData& obj);
    /**
     * @brief Destructor
     */
    ~CommonShapeData();
    /**
     * @brief Set name of shape
     * @param name Name of shape
     * @return Reference to this
     */
    CommonShapeData& SetName(const wxString& name)
    {
        this->m_name = name;
        return *this;
    }
    /**
     * @brief Get name of shape
     * @return Name of shape
     */
    const wxString& GetName() const
    {
        return m_name;
    }
    /**
     * @brief Get event type mapped to shape
     * @return Type of event mapped to shape
     */
    EventType GetEventTypeOfShape();

};

#endif // COMMONSHAPEDATA_H
