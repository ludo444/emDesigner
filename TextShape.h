/***************************************************************
 * Name:      TextShape.h
 * Purpose:   Declaration of TextShape, text element in canvas
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   05/10/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#ifndef TEXTSHAPE_H
#define TEXTSHAPE_H

#include <wx/wxsf/wxShapeFramework.h>
#include "CommonShapeData.h"

/**
 * @class TextShape
 * @author Marek Hrušovský
 * @brief Shape that holds text as canvas element
 */
class TextShape : public wxSFTextShape
{
public:
    XS_DECLARE_CLONABLE_CLASS(TextShape);

private:
    /**
     * @brief Configure shape
     */
    void InitShape();

public:
    /**
     * @brief Array of possible events
     */
    static EventType s_events[];
    /**
     * @brief Count of possible events
     */
    static int s_eventsCount;
    /**
     * @brief Default constructor
     */
    TextShape();
    /**
     * @brief User constructor with necessary information
     * @param pos Position in canvas
     * @param txt Text content of shape
     * @param manager Manager of shapes
     */
    TextShape(const wxRealPoint& pos, const wxString& txt, wxSFDiagramManager* manager);
    /**
     * @brief Copy constructor
     * @param obj Source object
     */
    TextShape(const wxSFTextShape& obj);
    /**
     * @brief Destructor
     */
    ~TextShape();

};

#endif // TEXTSHAPE_H
