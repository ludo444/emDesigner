/***************************************************************
 * Name:      ScreenCanvas.cpp
 * Purpose:   Implementation of ScreenCanvas
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   02/09/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#include "ScreenCanvas.h"

#include "main.h"
#include "WorkPanel.h"

ScreenCanvas::ScreenCanvas(WorkPanel* panel, wxTreeItemId treeItem, Screen* screen, Project* project, wxSFDiagramManager* manager, wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size) : wxSFShapeCanvas(manager, parent, id, pos, size, wxSTATIC_BORDER), m_workPanel(panel), m_treeItem(treeItem), m_screen(screen), m_project(project), m_selectedShape(NULL)
{
    m_mainFrame = (MainFrame*) wxGetApp().GetTopWindow();
    SetScale(1.);

    GetDiagramManager()->ClearAcceptedShapes();
    GetDiagramManager()->AcceptShape("BitmapShape");
    GetDiagramManager()->AcceptShape("TextShape");
    GetDiagramManager()->AcceptShape("ButtonShape");
    GetDiagramManager()->AcceptShape("CommonShapeData");
    GetDiagramManager()->AcceptShape("ScreenChangeEvent");
    GetDiagramManager()->AcceptShape("BitmapChangeEvent");
    GetDiagramManager()->ClearAcceptedTopShapes();
    GetDiagramManager()->AcceptTopShape("BitmapShape");
    GetDiagramManager()->AcceptTopShape("TextShape");
    GetDiagramManager()->AcceptTopShape("ButtonShape");
    GetDiagramManager()->AcceptTopShape("CommonShapeData");
    GetDiagramManager()->AcceptTopShape("ScreenChangeEvent");
    GetDiagramManager()->AcceptTopShape("BitmapChangeEvent");
    GetHistoryManager().SetMode(wxSFCanvasHistory::histUSE_SERIALIZATION);
    SaveCanvasState();

    screen->PostProcessScreen();

    this->Connect(wxEVT_SF_SHAPE_DRAG_END, wxSFShapeEventHandler(ScreenCanvas::OnDragEnd), NULL, this);

}

ScreenCanvas::~ScreenCanvas()
{
    this->Disconnect(wxEVT_SF_SHAPE_DRAG_END, wxSFShapeEventHandler(ScreenCanvas::OnDragEnd), NULL, this);

    // remove wxWidgets controls from shapes
    ShapeList controlShapes;
    GetDiagramManager()->GetShapes(CLASSINFO(ButtonShape), controlShapes);
    for(ShapeList::const_iterator i = controlShapes.begin(); i != controlShapes.end(); i++)
    {
        ControlShape* shape = (ControlShape*) *i;
        shape->SetControl(NULL);
    }

    // remove diagram manager to avoid it being deleted
    SetDiagramManager(NULL);
}

void ScreenCanvas::OnLeftDown(wxMouseEvent& event)
{
    wxSFShapeBase* shape = NULL;

    switch(m_mainFrame->GetWorkMode())
    {
        case BITMAP_MODE:
            shape = AddBitmap(event);
            break;

        case TEXT_MODE:
            shape = AddText(event);
            break;

        case BUTTON_MODE:
            shape = AddButton(event);
            break;

        default:
        {
            wxSFShapeCanvas::OnLeftDown(event);
            wxSFShapeBase* shape = GetShapeUnderCursor();
            if(shape != NULL)
                OnShapeSelect(shape);
        }
    }

    if(shape)
    {
        SaveCanvasState();
        if(!event.ControlDown())
            m_mainFrame->SetWorkMode(DESIGN_MODE);
        shape->Refresh();
    }
}

BitmapShape* ScreenCanvas::AddBitmap(wxMouseEvent& event)
{
    BitmapShape* shape = NULL;
    wxFileDialog dialog(m_mainFrame, "Load bitmap image...", wxGetCwd(), "", IMAGE_WILDCARD, wxFD_OPEN | wxFD_FILE_MUST_EXIST);

    if(dialog.ShowModal() == wxID_OK)
    {
        shape = (BitmapShape*) GetDiagramManager()->AddShape(CLASSINFO(BitmapShape), event.GetPosition(), sfDONT_SAVE_STATE);
        if(shape)
        {
            wxFileName path(dialog.GetPath());
            path.MakeRelativeTo(BitmapShape::s_relativePath);
            shape->CreateFromFile(path.GetFullPath());
            shape->AddChild(new CommonShapeData("Bitmap shape"));
            m_mainFrame->GetScreensTreeCtrl()->AppendItem(m_treeItem, "Bitmap shape", -1, -1, new TreeItem(shape, m_screen));
        }
    }

    return shape;
}

void ScreenCanvas::OnShapeSelect(wxSFShapeBase* shape)
{
    wxTreeItemId shapeItem = m_mainFrame->GetScreensTreeCtrl()->GetShapeTreeItem(shape, m_treeItem);
    m_mainFrame->GetScreensTreeCtrl()->SelectItem(shapeItem);
    m_selectedShape = shape;
}

void ScreenCanvas::OnLeftUp(wxMouseEvent& event)
{
    if(m_selectedShape)
        if(m_selectedShape == m_mainFrame->GetElementsPropgrid()->GetData().GetShape())
        {
            wxTreeItemId shapeItem = m_mainFrame->GetScreensTreeCtrl()->GetShapeTreeItem(m_selectedShape, m_treeItem);
            m_mainFrame->GetElementsPropgrid()->SetShapeProps(m_selectedShape, shapeItem, m_screen, m_workPanel);
        }
    wxSFShapeCanvas::OnLeftUp(event);
}

void ScreenCanvas::OnKeyDown(wxKeyEvent& event)
{
    switch(event.GetKeyCode())
    {
        case WXK_DELETE:
            DeleteSelectedShapesFromTree();
            break;
    }

    wxSFShapeCanvas::OnKeyDown(event);
}

void ScreenCanvas::OnDragEnd(wxSFShapeEvent& event)
{
    double scale = GetScale();
    wxSFShapeBase* shape = event.GetShape();
    wxSize canvasSize = GetSize();
    canvasSize.x /= scale;
    canvasSize.y /= scale;
    wxSize shapeSize = shape->GetBoundingBox().GetSize();
    wxRealPoint topPosition = shape->GetAbsolutePosition();
    wxRealPoint bottomPositon(topPosition.x + shapeSize.GetX(), topPosition.y + shapeSize.GetY());
    wxRealPoint newTopPosition(topPosition);

    if(topPosition.x < 0.)
        newTopPosition.x = 0.;
    if(topPosition.y < 0.)
        newTopPosition.y = 0.;

    if(bottomPositon.x > canvasSize.GetX())
        newTopPosition.x = canvasSize.GetX() - shapeSize.GetX();
    if(bottomPositon.y > canvasSize.GetY())
        newTopPosition.y = canvasSize.GetY() - shapeSize.GetY();

    shape->MoveTo(newTopPosition);
    shape->Refresh();
    wxTreeItemId shapeItem = m_mainFrame->GetScreensTreeCtrl()->GetShapeTreeItem(shape, m_treeItem);
    m_mainFrame->GetElementsPropgrid()->SetShapeProps(m_selectedShape, shapeItem, m_screen, m_workPanel);
}

TextShape* ScreenCanvas::AddText(wxMouseEvent& event)
{
    TextShape* shape = NULL;
    wxTextEntryDialog dlg(this, "", "Enter text", "");
    if(dlg.ShowModal() == wxID_OK)
    {
        shape = (TextShape*) GetDiagramManager()->AddShape(CLASSINFO(TextShape), event.GetPosition(), sfDONT_SAVE_STATE);
        shape->SetText(dlg.GetValue());
        shape->AddChild(new CommonShapeData("Text shape"));
        m_mainFrame->GetScreensTreeCtrl()->AppendItem(m_treeItem, "Text shape", -1, -1, new TreeItem(shape, m_screen));
    }
    return shape;
}

void ScreenCanvas::OnPaste(const ShapeList& pasted)
{
    for(ShapeList::const_iterator i = pasted.begin(); i != pasted.end(); i++)
    {
        wxSFShapeBase* shape = *i;
        if(shape->GetClassInfo() == CLASSINFO(BitmapShape))
        {
            BitmapShape* bitmapShape = (BitmapShape*) shape;
            bitmapShape->PostProcessBitmap(bitmapShape->GetBitmapPath());
        }
        else if(shape->GetClassInfo() == CLASSINFO(ButtonShape))
        {
            ControlShape* controlShape = (ControlShape*) shape;
            controlShape->PostProcess();
        }

        wxSFShapeBase* shapeDataCandidate = (wxSFShapeBase*) shape->GetFirstChild();
        if(shapeDataCandidate)
        {
            if(shapeDataCandidate->GetClassInfo() == CLASSINFO(CommonShapeData))
            {
                CommonShapeData* shapeData = (CommonShapeData*) shapeDataCandidate;
                m_mainFrame->GetScreensTreeCtrl()->AppendItem(m_treeItem, shapeData->GetName(), -1, -1, new TreeItem(shape, m_screen));
            }
        }
    }

    wxSFShapeCanvas::OnPaste(pasted);
}

void ScreenCanvas::Cut()
{
    DeleteSelectedShapesFromTree();
    wxSFShapeCanvas::Cut();
}

void ScreenCanvas::DeleteSelectedShapesFromTree()
{
    ShapeList shapeList;
    this->GetSelectedShapes(shapeList);
    for(ShapeList::const_iterator i = shapeList.begin(); i != shapeList.end(); i++)
    {
        wxSFShapeBase* shape = *i;
        wxTreeItemId item = m_mainFrame->GetScreensTreeCtrl()->GetShapeTreeItem(shape, m_treeItem);
        m_mainFrame->GetScreensTreeCtrl()->Delete(item);
    }
}

bool ScreenCanvas::ExportToImage(const wxString& path, wxBitmapType type)
{
    wxBitmap outbmp(GetSize());
    wxMemoryDC outdc(outbmp);

    if(outdc.IsOk())
    {
        this->DrawBackground(outdc, sfNOT_FROM_PAINT);
        this->DrawContent(outdc, sfNOT_FROM_PAINT);
        this->DrawForeground(outdc, sfNOT_FROM_PAINT);

        return outbmp.SaveFile(path, type);
    }

    return false;
}

void ScreenCanvas::Undo()
{
    wxSFShapeCanvas::Undo();
    m_mainFrame->GetScreensTreeCtrl()->ProcessScreenShapes(m_treeItem , m_screen);
    m_mainFrame->GetElementsPropgrid()->Clear();
    m_screen->PostProcessScreen();
}

void ScreenCanvas::Redo()
{
    wxSFShapeCanvas::Redo();
    m_mainFrame->GetScreensTreeCtrl()->ProcessScreenShapes(m_treeItem , m_screen);
    m_mainFrame->GetElementsPropgrid()->Clear();
    m_screen->PostProcessScreen();
}

ButtonShape* ScreenCanvas::AddButton(wxMouseEvent& event)
{
    ButtonShape* shape = (ButtonShape*) GetDiagramManager()->AddShape(CLASSINFO(ButtonShape), event.GetPosition(), sfDONT_SAVE_STATE);
    wxButton* button = new wxButton(this, wxID_ANY, "Button", wxDefaultPosition, wxSize(100, 50));
    button->SetMinSize(wxDefaultSize);
    button->SetMaxSize(wxDefaultSize);

    shape->SetText(button->GetLabel());
    shape->SetFont(button->GetFont());
    shape->SetTextColour(button->GetForegroundColour());
    shape->ClearAcceptedChilds();
    shape->SetControlOffset(0);
    shape->SetControl(button);
    shape->SetEventProcessing(ControlShape::evtMOUSE2CANVAS | ControlShape::evtKEY2CANVAS);
    shape->AddChild(new CommonShapeData("Button shape"));

    m_mainFrame->GetScreensTreeCtrl()->AppendItem(m_treeItem, "Button shape", -1, -1, new TreeItem(shape, m_screen));
    return shape;
}
