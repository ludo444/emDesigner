/***************************************************************
 * Name:      TextShape.cpp
 * Purpose:   Implementation of TextShape
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   05/10/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#include "TextShape.h"

XS_IMPLEMENT_CLONABLE_CLASS(TextShape, wxSFTextShape);

EventType TextShape::s_events[] = {NO_EVENT, SCREEN_CHANGE_EVENT};

int TextShape::s_eventsCount = 2;

TextShape::TextShape() : wxSFTextShape()
{
    InitShape();
}

TextShape::TextShape(const wxRealPoint& pos, const wxString& txt, wxSFDiagramManager* manager) : wxSFTextShape(pos, txt, manager)
{
    InitShape();
}

TextShape::TextShape(const wxSFTextShape& obj) : wxSFTextShape(obj)
{
    InitShape();
}

TextShape::~TextShape()
{
}

void TextShape::InitShape()
{
    AddStyle(sfsEMIT_EVENTS);
}
