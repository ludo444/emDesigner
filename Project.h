/***************************************************************
 * Name:      Project.h
 * Purpose:   Declaration of project class for keeping of designed screens
 *            and project properties
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   02/09/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#ifndef PROJECT_H
#define PROJECT_H

#include <wx/wxxmlserializer/XmlSerializer.h>
#include <wx/string.h>

#include "Screen.h"

/**
 * @class Project
 * @author Marek Hrušovský
 * @brief Class holding properties of project
 */
class Project : public xsSerializable
{
public:
    DECLARE_DYNAMIC_CLASS(Project);

private:
    /**
     * @brief States if project was modified
     */
    bool m_isModified;
    /**
     * @brief Height of screen in pixels
     */
    int m_screenHeight;
    /**
     * @brief Width of screen in pixels
     */
    int m_screenWidth;
    /**
     * @brief Name of project
     */
    wxString m_name;
    /**
     * @brief Path to project, is independent of stored information
     */
    wxString m_path;
    /**
     * @brief Id of starting screen
     */
    int m_indexScreenId;
    /**
     * @brief Initializes project, used in constructors
     */
    void InitProject();

public:
    /**
     * @brief Constructor
     * @param name Name of project
     * @param path Path to project directory
     * @param screenHeight Height of screen in pixels
     * @param screenWidth Width of screen in pixels
     */
    Project(wxString name, wxString path, int screenHeight, int screenWidth);
    /**
     * @brief Default constructor
     */
    Project();
    /**
     * @brief Destructor
     */
    ~Project();
    /**
     * @brief Check if project is modified
     * @return True, if any of project data and its children are modified
     */
    bool IsProjectModified();
    /**
     * @brief Add new screen to project
     * @param name Name of screen
     * @return Created Screen object
     */
    Screen* AddScreen(wxString name = wxString());
    /**
     * @brief Process project and its children after all variables were initialized/deserialized
     */
    void PostProcessProject();
    /**
     * @brief Set height of screen
     * @param screenHeight height of screen
     * @return Project reference
     */
    Project& SetScreenHeight(int screenHeight)
    {
        this->m_screenHeight = screenHeight;
        return *this;
    }
    /**
     * @brief Set width of screen
     * @param screenWidth width of screen
     * @return Project reference
     */
    Project& SetScreenWidth(int screenWidth)
    {
        this->m_screenWidth = screenWidth;
        return *this;
    }
    /**
     * @brief Get screen height
     * @return Screen height
     */
    int GetScreenHeight() const
    {
        return m_screenHeight;
    }
    /**
     * @brief Get screen width
     * @return Screen width
     */
    int GetScreenWidth() const
    {
        return m_screenWidth;
    }
    /**
     * @brief Set project name
     * @param projectName New project name
     * @return Project reference
     */
    Project& SetName(const wxString& projectName)
    {
        this->m_name = projectName;
        return *this;
    }
    /**
     * @brief Get project name
     * @return Project name
     */
    const wxString& GetName() const
    {
        return m_name;
    }
    /**
     * @brief Get reference to screen list
     * @return SerializableList of Screens
     */
    SerializableList& GetScreenList()
    {
        return GetChildrenList();
    }
    /**
     * @brief Get path in which Project is stored
     * @return Path string
     */
    const wxString& GetPath() const
    {
        return m_path;
    }
    /**
     * @brief Set path to Project (and BitmapShape relative path)
     * @param path Path to Project
     * @return Reference to this
     */
    Project& SetPath(const wxString& path);
    /**
     * @brief Set index screen id
     * @param indexScreenId Id of starting screen
     * @return Reference to this
     */
    Project& SetIndexScreenId(int indexScreenId)
    {
        this->m_indexScreenId = indexScreenId;
        return *this;
    }
    /**
     * @brief Get index screen id
     * @return Id of starting screen
     */
    int GetIndexScreenId() const
    {
        return m_indexScreenId;
    }
    /**
     * @brief Set modified flag
     * @param isModified Modified flag
     * @return Reference to this
     */
    Project& SetModified(bool m_isModified)
    {
        this->m_isModified = m_isModified;
        return *this;
    }
    /**
     * @brief Get modified flag
     * @return True, if marked as modified
     */
    bool IsModified() const
    {
        return m_isModified;
    }
};

#endif // PROJECT_H
