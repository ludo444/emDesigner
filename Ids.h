/***************************************************************
 * Name:      Ids.h
 * Purpose:   IDs used in application
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   05/03/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#ifndef EMDESIGNER_IDS_H
#define EMDESIGNER_IDS_H

/**
 * @brief Enum of application wxWidgets IDs
 */
enum Emdesigner_IDs
{
    // button IDs
    ID_OPEN,
    ID_SAVE,
    ID_UNDO,
    ID_REDO,

    // mode IDs
    ID_DESIGN,
    ID_BITMAP,
    ID_TEXT,
    ID_BUTTON
};

/**
 * @brief Enum of possible design work modes
 */
enum WorkMode
{
    DESIGN_MODE,
    BITMAP_MODE,
    TEXT_MODE,
    BUTTON_MODE
};

/**
 * @brief Enum specifying what's stored item container
 */
enum ItemType
{
    TYPE_NONE,
    TYPE_SCREEN,
    TYPE_SHAPE,
    TYPE_BITMAP_SHAPE,
    TYPE_TEXT_SHAPE,
    TYPE_BUTTON_SHAPE
};

/**
 * @brief Enum specifying what events can be mapped
 */
enum EventType
{
    NO_EVENT,
    SCREEN_CHANGE_EVENT,
    BITMAP_CHANGE_EVENT
};

#endif // EMDESIGNER_IDS_H
