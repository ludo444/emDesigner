/***************************************************************
 * Name:      Propgrid.cpp
 * Purpose:   Implementation of Propgrid class
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   05/02/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#include "Propgrid.h"

#include "main.h"

Propgrid::Propgrid(wxWindow *parent, const wxSize& size) : wxPropertyGrid(parent, wxID_ANY, wxDefaultPosition, size)
{
    m_mainFrame = (MainFrame*) wxGetApp().GetTopWindow();

    this->Connect(wxEVT_PG_CHANGED, wxPropertyGridEventHandler(Propgrid::OnPropertyChange), NULL, this);
}

Propgrid::~Propgrid()
{
    this->Disconnect(wxEVT_PG_CHANGED, wxPropertyGridEventHandler(Propgrid::OnPropertyChange), NULL, this);
}

void Propgrid::OnPropertyChange(wxPropertyGridEvent& event)
{
    wxPGProperty* property = event.GetProperty();
    if(!property)
        return;
    wxAny value = property->GetValue();
    if(value.IsNull())
        return;

    switch(m_data.GetItemType())
    {
        case TYPE_TEXT_SHAPE:
        case TYPE_BITMAP_SHAPE:
        case TYPE_BUTTON_SHAPE:
        case TYPE_SHAPE:
            m_data.ShapePropertyChange(event);
            break;

        case TYPE_SCREEN:
            m_data.ScreenPropertyChange(event);
            break;

        default:
            break;
    }

    m_mainFrame->GetProject()->SetModified(true);
}

void Propgrid::Clear()
{
    wxPropertyGrid::Clear();
    m_data.ClearData();
}

void Propgrid::SetScreenProps(Screen* screen, wxTreeItemId treeItem, WorkPanel* workPanel)
{
    wxPropertyGrid::Clear();
    m_data.SetScreen(screen, treeItem, workPanel);
    Append(new wxStringProperty("Name", "Name", screen->GetName()));
    Append(new wxColourProperty("Background color", "Background color", screen->GetBackgroundColor()));

    bool isIndexScreen = false;
    if(screen->GetId() == m_mainFrame->GetProject()->GetIndexScreenId())
        isIndexScreen = true;
    Append(new wxBoolProperty("Index screen", "Index screen", isIndexScreen));
}

void Propgrid::SetShapeProps(wxSFShapeBase* shape, wxTreeItemId treeItem, Screen* screen, WorkPanel* workPanel)
{
    wxPropertyGrid::Clear();
    m_data.SetShape(shape, treeItem, screen, workPanel);
    Append(new wxStringProperty("Name", "Name", ((CommonShapeData*) shape->GetFirstChild())->GetName()));
    Append(new wxIntProperty("Position X", "Position X", shape->GetAbsolutePosition().x));
    Append(new wxIntProperty("Position Y", "Position Y", shape->GetAbsolutePosition().y));

    switch(m_data.GetItemType())
    {
        case TYPE_BUTTON_SHAPE:
        case TYPE_TEXT_SHAPE:
            AppendFontProps(shape);

        case TYPE_BITMAP_SHAPE:
            AppendSizeProps((wxSFRectShape*) shape);
            break;

        default:
            break;
    }

    AppendEventProps(shape);
    Refresh();
}

void Propgrid::AppendSizeProps(wxSFRectShape* shape)
{
    Append(new wxIntProperty("Width", "Width", shape->GetRectSize().x));
    Append(new wxIntProperty("Height", "Height", shape->GetRectSize().y));
}

void Propgrid::AppendEventProps(wxSFShapeBase* shape)
{
    wxArrayInt events = GetEventTypes(shape);
    wxArrayString names;

    for(unsigned i=0; i < events.GetCount(); i++)
    {
        switch(events[i])
        {
            case NO_EVENT:
                names.Add("");
                break;

            case SCREEN_CHANGE_EVENT:
                names.Add("Screen change");
                break;

            case BITMAP_CHANGE_EVENT:
                names.Add("Bitmap change");
        }
    }

    EventType eventType = ((CommonShapeData*) shape->GetFirstChild())->GetEventTypeOfShape();
    wxPGProperty* property = NULL;
    if(!events.IsEmpty())
        property = Append(new wxEnumProperty("Events", "Events", names, events, eventType));

    AppendEventSubProps(eventType, property);
}

wxArrayInt Propgrid::GetEventTypes(wxSFShapeBase* shape)
{
    EventType* events;
    unsigned count;
    wxArrayInt eventsArray;

    if(shape->GetClassInfo() == CLASSINFO(BitmapShape))
    {
        events = BitmapShape::s_events;
        count = BitmapShape::s_eventsCount;
    }
    else if(shape->GetClassInfo() == CLASSINFO(TextShape))
    {
        events = TextShape::s_events;
        count = TextShape::s_eventsCount;
    }
    else if(shape->GetClassInfo() == CLASSINFO(ButtonShape))
    {
        events = ButtonShape::s_events;
        count = ButtonShape::s_eventsCount;
    }

    for(unsigned i=0; i < count; i++)
        eventsArray.Add(events[i]);

    return eventsArray;
}

void Propgrid::AppendEventSubProps(EventType eventType, wxPGProperty* property)
{
    if(property)
        switch(eventType)
        {
            case SCREEN_CHANGE_EVENT:
                AppendScreenChangeEventProp(property);
                break;

            case BITMAP_CHANGE_EVENT:
                AppendBitmapChangeEventProp(property);

            default:
                break;
        }
}

void Propgrid::AppendScreenChangeEventProp(wxPGProperty* property)
{
    AppendIn(property, m_data.GetScreensArrayProperty());
}

void Propgrid::AppendBitmapChangeEventProp(wxPGProperty* property)
{
    AppendIn(property, m_data.GetImagePathProperty());
}

void Propgrid::AppendFontProps(wxSFShapeBase* shape)
{
    switch(m_data.GetItemType())
    {
        case TYPE_BUTTON_SHAPE:
        {
            Append(new wxStringProperty("Text", "Text", ((ButtonShape*) shape)->GetText()));
            Append(new wxFontProperty("Font", "Font", ((ButtonShape*) shape)->GetFont()));
            Append(new wxColourProperty("Font color", "Font color", ((ButtonShape*) shape)->GetTextColour()));
        }
        break;

        case TYPE_TEXT_SHAPE:
        {
            Append(new wxStringProperty("Text", "Text", ((TextShape*) shape)->GetText()));
            Append(new wxFontProperty("Font", "Font", ((TextShape*) shape)->GetFont()));
            Append(new wxColourProperty("Font color", "Font color", ((TextShape*) shape)->GetTextColour()));
        }
        break;
    }
}
