/***************************************************************
 * Name:      ScreenCanvas.h
 * Purpose:   Declaration of ScreenCanvas, screens editor
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   02/09/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#ifndef SCREENCANVAS_H
#define SCREENCANVAS_H

#include <wx/wxsf/wxShapeFramework.h>
#include <wx/treebase.h>

#include "Project.h"
#include "Ids.h"
#include "BitmapShape.h"
#include "TextShape.h"
#include "ButtonShape.h"

class WorkPanel;
class MainFrame;
/**
 * @class ScreenCanvas
 * @author Marek Hrušovský
 * @brief Class for work canvas where graphic objects are put
 */
class ScreenCanvas : public wxSFShapeCanvas
{
private:
    /**
     * @brief Main wxFrame of application
     */
    MainFrame* m_mainFrame;
    /**
     * @brief WorkPanel which contains ScreenCanvas
     */
    WorkPanel* m_workPanel;
    /**
     * @brief TreeItem ID of screen
     */
    wxTreeItemId m_treeItem;
    /**
     * @brief Screen object of canvas
     */
    Screen* m_screen;
    /**
     * @brief Current project
     */
    Project* m_project;
    /**
     * @brief Shape that was selected
     */
    wxSFShapeBase* m_selectedShape;

    ////////////////////////////////////////////////////////////////////////////
    // PRIVATE METHODS
    /**
     * @brief Add bitmap to canvas
     * @param Mouse event at place of bitmap addition
     * @return Loaded BitmapShape pointer
     */
    BitmapShape* AddBitmap(wxMouseEvent& event);
    /**
     * @brief Add text to canvas
     * @param event Mouse event at place of shape addition
     * @return Created shape
     */
    TextShape* AddText(wxMouseEvent& event);
    /**
     * @brief Add button to canvas
     * @param event Mouse event at place of shape addition
     * @return Pointer to created shape
     */
    ButtonShape* AddButton(wxMouseEvent& event);
    /**
     * @brief When shape is selected, it's selected also in MainFrame Screens TreeCtrl
     * @param shape Pointer to shape
     */
    void OnShapeSelect(wxSFShapeBase* shape);
    /**
     * @brief When shapes are pasted to canvas
     * @param pasted List of pasted shapes
     */
    void OnPaste(const ShapeList& pasted);
    /**
     * @brief Delete selected shapes from TreeCtrl in main frame
     */
    void DeleteSelectedShapesFromTree();

    ////////////////////////////////////////////////////////////////////////////
    // EVENT HANDLERS
    /**
     * @brief Catching pressing down of left mouse button, putting shapes to canvas
     * @param event Event object
     */
    void OnLeftDown(wxMouseEvent& event);
    /**
     * @brief Processing of mouse button release
     * @param event Event object
     */
    void OnLeftUp(wxMouseEvent& event);
    /**
     * @brief On key down event
     * @param event Event object
     */
    void OnKeyDown(wxKeyEvent& event);
    /**
     * @brief On end of shape dragging
     * @param event Event object
     */
    void OnDragEnd(wxSFShapeEvent& event);

public:
    /**
     * @brief Constructor
     * @param workPanel WorkPanel which contains ScreenCanvas
     * @param treeItem Id of screen in wxTreeCtrl
     * @param screen Pointer to Screen designed in this canvas
     * @param project Pointer to project this canvas belongs to Parent window
     * @param manager wxSFDiagramManager instance that is managing canvas
     * @param parent Parent window
     * @param id Id of window
     * @param pos Position
     * @param size Size
     */
    ScreenCanvas(WorkPanel* workPanel, wxTreeItemId treeItem, Screen* screen, Project* project, wxSFDiagramManager* manager, wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize);
    /**
     * @brief Destructor
     */
    ~ScreenCanvas();
    /**
     * @brief Cut selected shapes from canvas and TreeCtrl and save them in clipboard
     */
    void Cut();
    /**
     * @brief Export canvas to image file
     * @param path To which path save
     * @param type What type of bitmap should save
     * @return True, if success
     */
    bool ExportToImage(const wxString& path, wxBitmapType type = wxBITMAP_TYPE_PNG);
    /**
     * @brief Perform undo operation on canvas
     */
    void Undo();
    /**
     * @brief Perform redo operation on canvas
     */
    void Redo();

};

#endif // SCREENCANVAS_H
