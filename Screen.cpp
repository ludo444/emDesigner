/***************************************************************
 * Name:      Screen.cpp
 * Purpose:   Implementation of class holding screen information
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   02/23/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#include "Screen.h"

#include "BitmapShape.h"
#include "ButtonShape.h"

IMPLEMENT_DYNAMIC_CLASS(Screen, xsSerializable);

int Screen::s_maxId = 0;

Screen::Screen(wxString name, wxSFDiagramManager* diagramManager, wxColour backgroundColor) : xsSerializable(), m_name(name), m_backgroundColor(backgroundColor), m_diagramManager(diagramManager)
{
    InitScreen();
    SetNewScreenId();
}

Screen::Screen() : xsSerializable()
{
    InitScreen();
    SetNewScreenId();
}

Screen::~Screen()
{
    m_diagramManager->SetShapeCanvas(NULL);
    if(m_diagramManager)
        delete m_diagramManager;
}

void Screen::SetNewScreenId()
{
    m_id = Screen::s_maxId = Screen::s_maxId + 1;
}

wxString Screen::GetFilename()
{
    wxString fileName(m_name);
    fileName << "-" << m_id;
    fileName.Replace(" ", "_");
    return fileName;
}

void Screen::InitScreen()
{
    XS_SERIALIZE(m_id, "identification");
    XS_SERIALIZE(m_name, "name");
    XS_SERIALIZE(m_backgroundColor, "background-color");
}

void Screen::PostProcessScreen()
{
    SerializableList bitmapShapes;
    m_diagramManager->GetItems(CLASSINFO(BitmapShape), bitmapShapes);
    for(SerializableList::const_iterator i = bitmapShapes.begin(); i != bitmapShapes.end(); i++)
    {
        BitmapShape* bitmapShape = (BitmapShape*) *i;
        wxString bitmapPath = bitmapShape->GetBitmapPath();
        bitmapShape->PostProcessBitmap(bitmapPath);
        bitmapShape->Refresh();
    }

    SerializableList controlShapes;
    m_diagramManager->GetItems(CLASSINFO(ControlShape), controlShapes);
    for(SerializableList::const_iterator i = controlShapes.begin(); i != controlShapes.end(); i++)
    {
        ControlShape* controlShape = (ControlShape*) *i;
        controlShape->PostProcess();
        controlShape->Refresh();
    }
}
