/***************************************************************
 * Name:      WorkPanel.cpp
 * Purpose:   Implementation of WorkPanel
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   02/26/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#include "WorkPanel.h"
#include "MainFrame.h"
#include "main.h"

#include "WorkPanel.h"
#include "ButtonShape.h"

WorkPanel::WorkPanel(wxTreeItemId treeItem, Screen* screen, Project* project, wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size) : wxPanel(parent, id, pos, size), m_treeItem(treeItem), m_screen(screen), m_project(project)
{
    m_mainFrame = (MainFrame*) wxGetApp().GetTopWindow();

    wxBoxSizer* bSizer = new wxBoxSizer(wxVERTICAL);

    m_canvasScrolledWindow = new wxScrolledWindow(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHSCROLL|wxVSCROLL);
    m_canvasScrolledWindow->SetScrollRate(5, 5);
    wxBoxSizer* innerBSizer = new wxBoxSizer(wxVERTICAL);

    m_screenCanvas = new ScreenCanvas(this, treeItem, screen, project, screen->GetDiagramManager(), m_canvasScrolledWindow, wxID_ANY);
    SetCanvasSize(wxSize(project->GetScreenWidth(), project->GetScreenHeight()));
    SetBackgroundColor(screen->GetBackgroundColor());
    innerBSizer->Add(m_screenCanvas, 1, wxEXPAND | wxALIGN_CENTER | wxALL, 0);

    m_canvasScrolledWindow->SetSizer(innerBSizer);
    m_canvasScrolledWindow->Layout();
    innerBSizer->Fit(m_canvasScrolledWindow);
    bSizer->Add(m_canvasScrolledWindow, 1, wxEXPAND | wxALL, 5);

    wxPanel* bottomBar = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxDefaultSize);
    wxBoxSizer* barSizer = new wxBoxSizer(wxHORIZONTAL);

    wxStaticText* zoomText = new wxStaticText(bottomBar, wxID_ANY, "Zoom", wxDefaultPosition, wxDefaultSize, 0);
    zoomText->Wrap(-1);
    barSizer->Add(zoomText, 0, wxRIGHT, 5);

    m_zoomSlider = new wxSlider(bottomBar, wxID_ANY, 100, 0, 500, wxDefaultPosition, wxSize(200, -1), wxSL_HORIZONTAL);
    m_zoomSlider->SetMinSize(wxSize(100, -1));
    barSizer->Add(m_zoomSlider, 0, wxRIGHT, 5);

    m_zoomValueText = new wxStaticText(bottomBar, wxID_ANY, "100 %", wxDefaultPosition, wxDefaultSize, 0);
    m_zoomValueText->Wrap(-1);
    barSizer->Add(m_zoomValueText, 0, wxRIGHT, 5);

    bottomBar->SetSizer(barSizer);
    bottomBar->Layout();
    barSizer->Fit(bottomBar);
    bSizer->Add(bottomBar, 0, wxALL, 0);

    this->SetSizer(bSizer);
    this->Layout();
    bSizer->Fit(this);

    m_zoomSlider->Connect(wxEVT_SCROLL_TOP, wxScrollEventHandler(WorkPanel::OnZoom), NULL, this);
    m_zoomSlider->Connect(wxEVT_SCROLL_BOTTOM, wxScrollEventHandler(WorkPanel::OnZoom), NULL, this);
    m_zoomSlider->Connect(wxEVT_SCROLL_LINEUP, wxScrollEventHandler(WorkPanel::OnZoom), NULL, this);
    m_zoomSlider->Connect(wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler(WorkPanel::OnZoom), NULL, this);
    m_zoomSlider->Connect(wxEVT_SCROLL_PAGEUP, wxScrollEventHandler(WorkPanel::OnZoom), NULL, this);
    m_zoomSlider->Connect(wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler(WorkPanel::OnZoom), NULL, this);
    m_zoomSlider->Connect(wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler(WorkPanel::OnZoom), NULL, this);
    m_zoomSlider->Connect(wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler(WorkPanel::OnZoom), NULL, this);
    m_zoomSlider->Connect(wxEVT_SCROLL_CHANGED, wxScrollEventHandler(WorkPanel::OnZoom), NULL, this);
    m_zoomSlider->Connect(wxEVT_LEFT_DCLICK, wxMouseEventHandler(WorkPanel::OnSliderDClick), NULL, this);
    m_zoomValueText->Connect(wxEVT_LEFT_DCLICK, wxMouseEventHandler(WorkPanel::OnSliderDClick), NULL, this);
}

WorkPanel::~WorkPanel()
{
    // remove assigned WorkPanel from TreeCtrl item
    if(m_treeItem)
    {
        TreeItem* treeItem = (TreeItem*) m_mainFrame->GetScreensTreeCtrl()->GetItemData(m_treeItem);
        treeItem->SetWorkPanel(NULL);
    }

    // remove assigned WorkPanel from Propgrid
    PropgridItem& propgridItem = m_mainFrame->GetElementsPropgrid()->GetData();
    propgridItem.RemoveWorkPanel(m_screen);

    m_zoomSlider->Disconnect(wxEVT_SCROLL_TOP, wxScrollEventHandler(WorkPanel::OnZoom), NULL, this);
    m_zoomSlider->Disconnect(wxEVT_SCROLL_BOTTOM, wxScrollEventHandler(WorkPanel::OnZoom), NULL, this);
    m_zoomSlider->Disconnect(wxEVT_SCROLL_LINEUP, wxScrollEventHandler(WorkPanel::OnZoom), NULL, this);
    m_zoomSlider->Disconnect(wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler(WorkPanel::OnZoom), NULL, this);
    m_zoomSlider->Disconnect(wxEVT_SCROLL_PAGEUP, wxScrollEventHandler(WorkPanel::OnZoom), NULL, this);
    m_zoomSlider->Disconnect(wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler(WorkPanel::OnZoom), NULL, this);
    m_zoomSlider->Disconnect(wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler(WorkPanel::OnZoom), NULL, this);
    m_zoomSlider->Disconnect(wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler(WorkPanel::OnZoom), NULL, this);
    m_zoomSlider->Disconnect(wxEVT_SCROLL_CHANGED, wxScrollEventHandler(WorkPanel::OnZoom), NULL, this);
    m_zoomSlider->Disconnect(wxEVT_LEFT_DCLICK, wxMouseEventHandler(WorkPanel::OnSliderDClick), NULL, this);
    m_zoomValueText->Disconnect(wxEVT_LEFT_DCLICK, wxMouseEventHandler(WorkPanel::OnSliderDClick), NULL, this);
}

void WorkPanel::SetBackgroundColor(const wxColor& bgColor)
{
    m_screenCanvas->SetCanvasColour(bgColor);
    m_screenCanvas->SetBackgroundColour(bgColor);
}

void WorkPanel::OnZoom(wxScrollEvent& event)
{
    // calculate scale
    int position = event.GetPosition();
    m_zoomValueText->SetLabel(wxString::Format("%i", position) + " %");
    Zoom(position);
}

void WorkPanel::SetCanvasSize(const wxSize& size)
{
    m_screenCanvas->SetSize(size);
    m_screenCanvas->SetMinSize(size);
    m_screenCanvas->SetMaxSize(size);
}

void WorkPanel::OnSliderDClick(wxMouseEvent& event)
{
    m_zoomSlider->SetValue(100);
    m_zoomValueText->SetLabel(wxString::Format("%i", 100) + " %");
    Zoom(100);
}

void WorkPanel::Zoom(int position)
{
    double scale = position / 100.;
    int sizeX = m_project->GetScreenWidth() * scale;
    int sizeY = m_project->GetScreenHeight() * scale;

    // hide control shapes while scaling
    ShapeList controlShapes;
    m_screenCanvas->GetDiagramManager()->GetShapes(CLASSINFO(ControlShape), controlShapes);
    for(ShapeList::const_iterator i = controlShapes.begin(); i != controlShapes.end(); i++)
    {
        ControlShape* shape = (ControlShape*) *i;
        shape->PrepareScale();
    }

    // scale
    SetCanvasSize(wxSize(sizeX, sizeY));
    m_screenCanvas->SetScale(scale);

    // show control shapes again
    for(ShapeList::const_iterator i = controlShapes.begin(); i != controlShapes.end(); i++)
    {
        ControlShape* shape = (ControlShape*) *i;
        shape->ProcessScale(scale);
    }

    Layout();
}
