/***************************************************************
 * Name:      MainFrame.h
 * Purpose:   GUI of main application frame of emDesigner
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   01/26/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#ifndef MAINFRAME_H
#define MAINFRAME_H

#include "MainFrameBase.h"
#include <wx/aui/auibar.h>

#include "Propgrid.h"
#include "TreeCtrl.h"
#include "Ids.h"
#include "WorkPanel.h"

#include "Project.h"
#include "FileHandler.h"
#include "SimulatorDialog.h"

/**
 * @class MainFrame
 * @author Marek Hrušovský
 * @brief Main wxFrame of application
 */
class MainFrame : public MainFrameBase
{
private:
    ////////////////////////////////////////////////////////////////////////////
    // WXWIDGETS ELEMENTS VARIABLES
    /**
     * @brief Property grid for elements and screens editing
     */
    Propgrid* m_elementsPropgrid;
    /**
     * @brief TreeCtrl showing hierarchy of elements assigned to particular screens
     */
    TreeCtrl* m_screensTreeCtrl;
    /**
     * @brief Toolbar holding typical application buttons as open, save, ...
     */
    wxAuiToolBar* m_buttonsAuiToolBar;
    /**
     * @brief Toolbar for designing elements
     */
    wxAuiToolBar* m_elementsAuiToolBar;

    ////////////////////////////////////////////////////////////////////////////
    // DATA VARIABLES
    /**
     * @brief Project container
     */
    Project* m_project;
    /**
     * @brief Indicates which tool is activated
     */
    WorkMode m_workMode;
    /**
     * @brief Container with project and supporting file operations
     */
    FileHandler* m_fileHandler;
    /**
     * @brief Simulator dialog
     */
    SimulatorDialog* m_simulatorDialog;

    ////////////////////////////////////////////////////////////////////////////
    // PRIVATE METHODS
    /**
     * @brief Activate menu and toolbar items after project is loaded/created
     */
    void ActivateProjectMenu();
    /**
     * @brief Deactivate menu and toolbar items after project is deleted
     */
    void DeactivateProjectMenu();
    /**
     * @brief Clear current project from application
     */
    void ClearCurrentProject();
    /**
     * @brief Exit application
     */
    void ExitApp();

    ////////////////////////////////////////////////////////////////////////////
    // EVENT HANDLERS
    /**
     * @brief Update of toolbar with tradional app buttons as Open, Save, ...
     * @param event Event object
     */
    void OnButtonsBarUpdate(wxUpdateUIEvent& event);
    /**
     * @brief Update of elements toolbar
     * @param event Event object
     */
    void OnElementsBarUpdate(wxUpdateUIEvent& event);
    /**
     * @brief Update of if cut is possible
     * @param event Event object
     */
    void OnUpdateCut(wxUpdateUIEvent& event);
    /**
     * @brief Update of if copy is possible
     * @param event Event object
     */
    void OnUpdateCopy(wxUpdateUIEvent& event);
    /**
     * @brief Update of if paste is possible
     * @param event Event object
     */
    void OnUpdatePaste(wxUpdateUIEvent& event);
    /**
     * @brief On tool clicked, changing designing mode
     * @param event Event object
     */
    void OnElementsTool(wxCommandEvent& event);
    /**
     * @brief Event handler after clicking on window close
     * @param event Event object
     */
    void OnCloseFrame(wxCloseEvent& event);
    /**
     * @brief Event handler after clicking on exit
     * @param event Event object
     */
    void OnExit(wxCommandEvent& event);
    /**
     * @brief On About event, showing info dialog
     * @param event Event object
     */
    void OnAbout(wxCommandEvent& event);
    /**
     * @brief On open existing project form a file event
     * @param event Event object
     */
    void OnOpen(wxCommandEvent& event);
    /**
     * @brief On save project to a file
     * @param event Event object
     */
    void OnSave(wxCommandEvent& event);
    /**
     * @brief Event handler when creating new project
     * @param event Event object
     */
    void OnNewProject(wxCommandEvent& event);
    /**
     * @brief Event handler when adding screen
     * @param event Event object
     */
    void OnAddScreen(wxCommandEvent& event);
    /**
     * @brief Generate HTML event handler
     * @param event Event object
     */
    void OnGenerateHtml(wxCommandEvent& event);
    /**
     * @brief Generate images event handler
     * @param event Event object
     */
    void OnGenerateImages(wxCommandEvent& event);
    /**
     * @brief Run simulator event handler
     * @param event Event object
     */
    void OnSimulate(wxCommandEvent& event);
    /**
     * @brief On cut
     * @param event Event object
     */
    void OnCut(wxCommandEvent& event);
    /**
     * @brief On copy
     * @param event Event object
     */
    void OnCopy(wxCommandEvent& event);
    /**
     * @brief On paste
     * @param event Event object
     */
    void OnPaste(wxCommandEvent& event);
    /**
     * @brief On update of select all
     * @param event Event object
     */
    void OnUpdateSelectAll(wxUpdateUIEvent& event);
    /**
     * @brief On select all
     * @param event Event object
     */
    void OnSelectAll(wxCommandEvent& event);
    /**
     * @brief On undo
     * @param event Event object
     */
    void OnUndo(wxCommandEvent& event);
    /**
     * @brief On update of undo
     * @param event Event object
     */
    void OnUpdateUndo(wxUpdateUIEvent& event);
    /**
     * @brief On redo
     * @param event Event object
     */
    void OnRedo(wxCommandEvent& event);
    /**
     * @brief On update redo
     * @param event Event object
     */
    void OnUpdateRedo(wxUpdateUIEvent& event);
    /**
     * @brief On application idle time
     * @param event Event object
     */
    void OnIdle(wxIdleEvent& event);
    /**
     * @brief On align shapes left
     * @param event Event object
     */
    void OnAlignLeft(wxCommandEvent& event);
    /**
     * @brief On update align
     * @param event Event object
     */
    void OnUpdateAlign(wxUpdateUIEvent& event);
    /**
     * @brief On align shapes right
     * @param event Event object
     */
    void OnAlignRight(wxCommandEvent& event);
    /**
     * @brief On align shapes top
     * @param event Event object
     */
    void OnAlignTop(wxCommandEvent& event);
    /**
     * @brief On align shapes bottom
     * @param event Event object
     */
    void OnAlignBottom(wxCommandEvent& event);

public:
    /**
     * @brief Constructor of main wxFrame of application
     * @param parent Pointer to parent object
     */
    MainFrame(wxWindow *parent);
    /**
     * @brief Destructor of main application's wxFrame
     */
    ~MainFrame();

    ////////////////////////////////////////////////////////////////////////////
    // PUBLIC METHODS
    /**
     * @brief Add new tab with screen editing
     * @param screen Screen that's edited
     * @param treeItem Id of screen in wxTreeList
     * @return Created WorkPanel in tab
     */
    WorkPanel* NewScreenTab(Screen* screen, wxTreeItemId treeItem);
    /**
     * @brief Get propgrid
     * @return Propgrid object
     */
    Propgrid* GetElementsPropgrid()
    {
        return m_elementsPropgrid;
    }
    /**
     * @brief Get TreeCtrl
     * @return TreeCtrl object
     */
    TreeCtrl* GetScreensTreeCtrl()
    {
        return m_screensTreeCtrl;
    }
    /**
     * @brief Get canvas notebook
     * @return  Canvas notebook object pointer
     */
    wxAuiNotebook* GetCanvasAuiNotebook()
    {
        return m_canvasAuiNotebook;
    }
    /**
     * @brief Get project
     * @return Current project pointer
     */
    Project* GetProject()
    {
        return m_project;
    }
    /**
     * @brief Get current designer work mode
     * @return Current work mode
     */
    const WorkMode& GetWorkMode() const
    {
        return m_workMode;
    }
    /**
     * @brief Set work mode
     * @param workMode Work mode value
     * @return Reference to this
     */
    MainFrame& SetWorkMode(const WorkMode& workMode)
    {
        this->m_workMode = workMode;
        return *this;
    }
    /**
     * @brief Set name of page that contains work panel
     * @param panel wxWindow panel in notebook
     * @param name Name of page
     * @return Edited page
     */
    wxAuiNotebookPage* SetCanvasAuiNotebookPageName(wxWindow* panel, const wxString& name);
    /**
     * @brief Close page in notebook
     * @param panel Pointer to wxWindow in notebook
     * @return True if deleted/closed
     */
    bool DeleteCanvasAuiNotebookPage(wxWindow* panel);

};

#endif // MAINFRAME_H
