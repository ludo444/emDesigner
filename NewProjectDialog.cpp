/***************************************************************
 * Name:      NewProjectDialog.cpp
 * Purpose:   Implementation of new project dialog
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   02/24/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#include "NewProjectDialog.h"

#include "FileHandler.h"

NewProjectDialog::NewProjectDialog(wxWindow* parent, const wxString& title, wxWindowID id, const wxPoint& pos, const wxSize& size, long style) : NewProjectDialogBase(parent, id, title, pos, size, style)
{
}

NewProjectDialog::~NewProjectDialog()
{
}

void NewProjectDialog::OnCreate(wxCommandEvent& event)
{
    wxString path(m_dirPicker->GetPath());
    wxString name(m_nameTextCtrl->GetLineText(0));
    wxFileName filenamePath(path, "", "");

    // checks if dialog filled
    if(!name.IsEmpty() && !path.IsEmpty())
    {
        // checks path validity
        if(wxFileName::DirExists(path))
        {
            // if checkbox checked, create directory under project name
            if(m_folderCheckBox->IsChecked())
            {
                if(!FileHandler::MakeDir(path, name))
                {
                    wxMessageDialog(this, "Error when creating directory", "Directory creation error").ShowModal();
                    return;
                }
                filenamePath.AppendDir(name);
            }

            // create project instance
            m_project = new Project(name, filenamePath.GetFullPath(), m_heightSpinCtrl->GetValue(), m_widthSpinCtrl->GetValue());
            m_project->AddScreen();

            // close dialog
            if(IsModal())
                EndModal(wxID_OK);
            else
            {
                SetReturnCode(wxID_OK);
                Hide();
            }
        }
        else
            wxMessageDialog(this, "Chosen path doesn't exists", "Path").ShowModal();
    }
    else
        wxMessageDialog(this, "Project name or project path is empty", "Empty").ShowModal();
}

void NewProjectDialog::OnClose(wxCloseEvent& event)
{
    if(IsModal())
        EndModal(wxID_CANCEL);
    else
    {
        SetReturnCode(wxID_CANCEL);
        Hide();
    }
}
