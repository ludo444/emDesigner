/*********************************************************************
 * Name:      	main.h
 * Purpose:   	Declares wxWidgets application and main application
 *              loop
 * Author:      Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:     01/26/17
 * Copyright:   Marek Hrušovský
 * License:   	GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 *
 * Notes:		Note that all main wxFrame GUI creation code is
 *              declared in gui_base.h source file which is generated
 *              by wxFormBuilder.
 *********************************************************************/
#ifndef __main__
#define __main__

// main wxWidgets header file
#include <wx/wx.h>
// GUI classes
#include "MainFrame.h"

/**
 * @class MainApp
 * @author Marek Hrušovský
 * @brief Main wxWidgets application
 */
class MainApp : public wxApp
{
public:
    /**
     * @brief wxWidgets initializing function
     * @return True if success
     */
    virtual bool OnInit();
};

// declare global static function wxGetApp()
DECLARE_APP(MainApp)

#endif //__main__
