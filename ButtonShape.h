/***************************************************************
 * Name:      ButtonShape.h
 * Purpose:   Button shape for canvas
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   05/14/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#ifndef BUTTONSHAPE_H
#define BUTTONSHAPE_H

#include <wx/wxsf/wxShapeFramework.h>
#include "CommonShapeData.h"
#include "ControlShape.h"

/**
 * @class ButtonShape
 * @author Marek Hrušovský
 * @brief Button shape for canvas
 */
class ButtonShape : public ControlShape
{
private:
    /**
     * @brief Text shown on button
     */
    wxString m_text;
    /**
     * @brief Font shown on button text
     */
    wxFont m_font;
    /**
     * @brief Color shown on button text
     */
    wxColour m_textColor;
    /**
     * @brief Method for necessary initialization in consturctors
     */
    void InitShape();

public:
    XS_DECLARE_CLONABLE_CLASS(ButtonShape);
    /**
     * @brief Array of possible events
     */
    static EventType s_events[];
    /**
     * @brief Count of possible events
     */
    static int s_eventsCount;
    /**
     * @brief Default constructor
     */
    ButtonShape();
    /**
     * @brief Copy constructor
     * @param obj Reference to object
     */
    ButtonShape(const ButtonShape& obj);
    /**
     * @brief User constructor
     * @param button Pointer to button
     * @param pos Position
     * @param size Size
     * @param manager Pointer to diagram manager
     */
    ButtonShape(wxWindow* button, const wxRealPoint& pos, const wxRealPoint& size, wxSFDiagramManager* manager);
    /**
     * @brief Destructor
     */
    ~ButtonShape();

    /**
     * @brief Post process shape
     */
    void PostProcess();
    /**
     * @brief Should be called on end of canvas scale
     * @param scale Scale value
     */
    void ProcessScale(double scale);
    /**
     * @brief Event activated when shape's handle finished dragging
     * @param handle Reference to dragged handle
     */
    virtual void OnEndHandle(wxSFShapeHandle& handle);
    /**
     * @brief Set button control
     * @param ctrl Should be pointer to wxButton
     * @param fit Flag if control should fit to bounding box
     */
    void SetControl(wxWindow *ctrl, bool fit = sfFIT_CONTROL_TO_SHAPE);
    /**
     * @brief Set font of button text
     * @param font Font
     * @return Reference to this
     */
    ButtonShape& SetFont(const wxFont& font)
    {
        this->m_font = font;
        if(m_pControl)
        {
            m_pControl->SetFont(m_font);
            m_pControl->Refresh();
        }
        return *this;
    }
    /**
     * @brief Set text of button
     * @param text Text
     * @return Reference to this
     */
    ButtonShape& SetText(const wxString& text)
    {
        this->m_text = text;
        if(m_pControl)
        {
            m_pControl->SetLabel(text);
            m_pControl->Refresh();
        }
        return *this;
    }
    /**
     * @brief Set color of button text
     * @param textColor Color
     * @return Reference to this
     */
    ButtonShape& SetTextColour(const wxColour& textColor)
    {
        this->m_textColor = textColor;
        if(m_pControl)
        {
            m_pControl->SetForegroundColour(textColor);
            m_pControl->Refresh();
        }
        return *this;
    }
    /**
     * @brief Get font of button
     * @return Reference to font
     */
    wxFont GetFont()
    {
        return m_font;
    }
    /**
     * @brief Get text of button
     * @return Reference to text string
     */
    wxString GetText()
    {
        return m_text;
    }
    /**
     * @brief Get color of button text
     * @return Rereference to color
     */
    wxColour GetTextColour()
    {
        return m_textColor;
    }

};

#endif // BUTTONSHAPE_H
