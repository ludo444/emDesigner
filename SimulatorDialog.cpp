/***************************************************************
 * Name:      SimulatorDialog.cpp
 * Purpose:   Implementation of SimulatorDialog
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   05/01/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#include "SimulatorDialog.h"

SimulatorDialog::SimulatorDialog(Project* project, const wxString& url, wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style) : wxDialog(parent, id, title, pos, size, style)
{
    this->SetSizeHints(wxDefaultSize, wxDefaultSize);
    wxBoxSizer* bSizer = new wxBoxSizer(wxVERTICAL);

    m_webView = wxWebView::New(this, wxID_ANY, url, wxDefaultPosition, wxSize(project->GetScreenWidth(), project->GetScreenHeight()));

    bSizer->Add(m_webView, 0, wxALL, 0);

    this->SetSizer(bSizer);
    this->Layout();
    wxSize fitSize = bSizer->Fit(this);
    this->Centre(wxBOTH);

    SetMinSize(fitSize);
    SetMaxSize(fitSize);

    this->Connect(wxEVT_CLOSE_WINDOW, wxCloseEventHandler(SimulatorDialog::OnClose));
}

SimulatorDialog::~SimulatorDialog()
{
    this->Disconnect(wxEVT_CLOSE_WINDOW, wxCloseEventHandler(SimulatorDialog::OnClose));
}

void SimulatorDialog::OnClose(wxCloseEvent& event)
{
    Hide();
}

void SimulatorDialog::HardReload()
{
    m_webView->Reload(wxWEBVIEW_RELOAD_NO_CACHE);
}
