/***************************************************************
 * Name:      MainFrame.cpp
 * Purpose:   Implementation of main application's wxFrame
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   01/26/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#include "MainFrame.h"

#include <wx/aboutdlg.h>

#include "resources/art/Tool.xpm"
#include "resources/art/Bitmap.xpm"
#include "resources/art/Text.xpm"
#include "resources/art/Rect.xpm"
#include "NewProjectDialog.h"

#include "HtmlGenerator.h"

MainFrame::MainFrame(wxWindow *parent) : MainFrameBase(parent)
{
    m_project = NULL;
    m_workMode = DESIGN_MODE;
    m_fileHandler = new FileHandler();
    m_simulatorDialog = NULL;
    wxInitAllImageHandlers();

    m_elementsPropgrid = new Propgrid(this, wxSize(150,-1));
    m_elementsPropgrid->SetMinSize(wxSize(100,-1));
    m_mgr.AddPane(m_elementsPropgrid, wxAuiPaneInfo().Right().Caption("Properties").CloseButton(false).Dock().Resizable().FloatingSize(wxDefaultSize));

    m_screensTreeCtrl = new TreeCtrl(this, wxSize(150,-1));
    m_screensTreeCtrl->SetMinSize(wxSize(100,-1));
    m_mgr.AddPane(m_screensTreeCtrl, wxAuiPaneInfo().Right().Caption("Screens").CloseButton(false).Dock().Resizable().FloatingSize(wxDefaultSize));

    m_buttonsAuiToolBar = new wxAuiToolBar(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxAUI_TB_DEFAULT_STYLE);
    m_buttonsAuiToolBar->AddTool(ID_OPEN, "Open", wxArtProvider::GetBitmap(wxART_FILE_OPEN, wxART_TOOLBAR), wxNullBitmap, wxITEM_NORMAL, "Open", wxEmptyString, NULL);
    m_buttonsAuiToolBar->AddTool(ID_SAVE, "Save", wxArtProvider::GetBitmap(wxART_FILE_SAVE, wxART_TOOLBAR), wxNullBitmap, wxITEM_NORMAL, "Save", wxEmptyString, NULL);
    m_buttonsAuiToolBar->AddSeparator();
    m_buttonsAuiToolBar->AddTool(ID_UNDO, "Undo", wxArtProvider::GetBitmap(wxART_UNDO, wxART_TOOLBAR), wxNullBitmap, wxITEM_NORMAL, "Undo", wxEmptyString, NULL);
    m_buttonsAuiToolBar->AddTool(ID_REDO, "Redo", wxArtProvider::GetBitmap(wxART_REDO, wxART_TOOLBAR), wxNullBitmap, wxITEM_NORMAL, "Redo", wxEmptyString, NULL);

    m_buttonsAuiToolBar->Realize();
    m_mgr.AddPane(m_buttonsAuiToolBar, wxAuiPaneInfo().Top().CaptionVisible(false).CloseButton(false).Dock().Resizable().FloatingSize(wxDefaultSize).Layer(10).ToolbarPane());

    m_elementsAuiToolBar = new wxAuiToolBar(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxAUI_TB_DEFAULT_STYLE);
    m_elementsAuiToolBar->AddTool(ID_DESIGN, "Design", wxBitmap(Tool_xpm), wxNullBitmap, wxITEM_RADIO, "Design tool", wxEmptyString, NULL);
    m_elementsAuiToolBar->AddTool(ID_BITMAP, "Bitmap", wxBitmap(Bitmap_xpm), wxNullBitmap, wxITEM_RADIO, "Bitmap tool", wxEmptyString, NULL);
    m_elementsAuiToolBar->AddTool(ID_TEXT, "Text", wxBitmap(Text_xpm), wxNullBitmap, wxITEM_RADIO, "Text tool", wxEmptyString, NULL);
    m_elementsAuiToolBar->AddTool(ID_BUTTON, "Button", wxBitmap(Rect_xpm), wxNullBitmap, wxITEM_RADIO, "Button tool", wxEmptyString, NULL);
    m_elementsAuiToolBar->Realize();
    m_mgr.AddPane(m_elementsAuiToolBar, wxAuiPaneInfo().Left().CaptionVisible(false).CloseButton(false).Dock().Resizable().FloatingSize(wxDefaultSize).Layer(10).ToolbarPane());

    m_mgr.Update();
    this->Centre(wxBOTH);

    m_buttonsAuiToolBar->Connect(wxEVT_UPDATE_UI, wxUpdateUIEventHandler(MainFrame::OnButtonsBarUpdate), NULL, this);
    m_elementsAuiToolBar->Connect(wxEVT_UPDATE_UI, wxUpdateUIEventHandler(MainFrame::OnElementsBarUpdate), NULL, this);
    this->Connect(ID_DESIGN, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler(MainFrame::OnElementsTool));
    this->Connect(ID_BITMAP, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler(MainFrame::OnElementsTool));
    this->Connect(ID_TEXT, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler(MainFrame::OnElementsTool));
    this->Connect(ID_BUTTON, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler(MainFrame::OnElementsTool));
    this->Connect(ID_OPEN, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler(MainFrame::OnOpen));
    this->Connect(ID_SAVE, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler(MainFrame::OnSave));
    this->Connect(ID_UNDO, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler(MainFrame::OnUndo));
    this->Connect(ID_REDO, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler(MainFrame::OnRedo));
}

MainFrame::~MainFrame()
{
    m_buttonsAuiToolBar->Disconnect(wxEVT_UPDATE_UI, wxUpdateUIEventHandler(MainFrame::OnButtonsBarUpdate), NULL, this);
    m_elementsAuiToolBar->Disconnect(wxEVT_UPDATE_UI, wxUpdateUIEventHandler(MainFrame::OnElementsBarUpdate), NULL, this);
    this->Disconnect(ID_DESIGN, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler(MainFrame::OnElementsTool));
    this->Disconnect(ID_BITMAP, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler(MainFrame::OnElementsTool));
    this->Disconnect(ID_TEXT, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler(MainFrame::OnElementsTool));
    this->Disconnect(ID_BUTTON, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler(MainFrame::OnElementsTool));
    this->Disconnect(ID_OPEN, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler(MainFrame::OnOpen));
    this->Disconnect(ID_SAVE, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler(MainFrame::OnSave));
    this->Disconnect(ID_UNDO, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler(MainFrame::OnUndo));
    this->Disconnect(ID_REDO, wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler(MainFrame::OnRedo));
}

void MainFrame::OnCloseFrame(wxCloseEvent& event)
{
    ExitApp();
}

void MainFrame::OnExit(wxCommandEvent& event)
{
    ExitApp();
}

void MainFrame::ExitApp()
{
    bool saved = true;
    if(m_project)
        saved = !m_project->IsProjectModified();

    if(!saved)
    {
        wxMessageDialog saveCheckDialog(this, "Do you want to save project?", "Project is not saved", wxYES_NO | wxYES_DEFAULT | wxCENTRE);
        if(saveCheckDialog.ShowModal() == wxID_YES)
            m_fileHandler->SaveProjectToXml();
    }

    ClearCurrentProject();
    delete m_fileHandler;
    Destroy();
}

void MainFrame::ActivateProjectMenu()
{
    m_saveMenuItem->Enable();
    m_addScreenMenuItem->Enable();
    m_generateHtmlMenuItem->Enable();
    m_generateImagesMenuItem->Enable();
    m_simulateMenuItem->Enable();
}

void MainFrame::DeactivateProjectMenu()
{
    m_saveMenuItem->Enable(false);
    m_addScreenMenuItem->Enable(false);
    m_generateHtmlMenuItem->Enable(false);
    m_generateImagesMenuItem->Enable(false);
    m_simulateMenuItem->Enable(false);
}

void MainFrame::OnButtonsBarUpdate(wxUpdateUIEvent& event)
{
    wxWindow* currentTab = m_canvasAuiNotebook->GetCurrentPage();
    WorkPanel* workPanel = (WorkPanel*) currentTab;

    switch(event.GetId())
    {
        case ID_OPEN:
            break;

        case ID_SAVE:
            event.Enable(m_project != NULL);
            break;

        case ID_UNDO:
            if(workPanel)
                event.Enable(workPanel->GetScreenCanvas()->CanUndo());
            else
                event.Enable(false);
            break;

        case ID_REDO:
            if(workPanel)
                event.Enable(workPanel->GetScreenCanvas()->CanRedo());
            else
                event.Enable(false);
            break;
    }
}

void MainFrame::OnElementsBarUpdate(wxUpdateUIEvent& event)
{
    switch(event.GetId())
    {
        case ID_DESIGN:
            event.Check(m_workMode == DESIGN_MODE);
            break;

        case ID_BITMAP:
            event.Check(m_workMode == BITMAP_MODE);
            break;

        case ID_TEXT:
            event.Check(m_workMode == TEXT_MODE);
            break;

        case ID_BUTTON:
            event.Check(m_workMode == BUTTON_MODE);
            break;
    }
}

void MainFrame::OnElementsTool(wxCommandEvent& event)
{
    switch(event.GetId())
    {
        case ID_DESIGN:
            m_workMode = DESIGN_MODE;
            break;

        case ID_BITMAP:
            m_workMode = BITMAP_MODE;
            break;

        case ID_TEXT:
            m_workMode = TEXT_MODE;
            break;

        case ID_BUTTON:
            m_workMode = BUTTON_MODE;
            break;
    }
}

void MainFrame::OnAbout(wxCommandEvent& event)
{
    wxAboutDialogInfo info;
    info.SetName("emDesigner");
    info.SetVersion("0.1");
    info.SetDescription("Embedded GUI designer");
    info.SetCopyright("2017 Marek Hrusovsky <hrusovsky.marek@gmail.com>");
    wxAboutBox(info);
}

void MainFrame::OnOpen(wxCommandEvent& event)
{
    wxFileDialog dialog(this, "Open project file", wxGetCwd(), "", "Files  (*.emDesigner)|*.emDesigner", wxFD_OPEN | wxFD_FILE_MUST_EXIST);

    if(dialog.ShowModal() == wxID_OK)
    {
        ClearCurrentProject();
        if(!(m_project = m_fileHandler->LoadProjectFromXml(dialog.GetPath())))
        {
            wxMessageDialog(this, "Chosen path doesn't exists", "Path").ShowModal();
            DeactivateProjectMenu();
            return;
        }
        ActivateProjectMenu();
        m_screensTreeCtrl->SetProject(m_project);
        SerializableList& screenList = m_project->GetScreenList();
        for(SerializableList::const_iterator i = screenList.begin(); i != screenList.end(); i++)
        {
            Screen* screen = (Screen*) *i;
            wxSFDiagramManager* diagramManager = screen->GetDiagramManager();
            diagramManager->SetModified(false);
        }
        m_project->SetModified(false);
    }
}

void MainFrame::OnSave(wxCommandEvent& event)
{
    m_fileHandler->SaveProjectToXml();
}

void MainFrame::OnNewProject(wxCommandEvent& event)
{
    NewProjectDialog newProjectDialog(this);
    if(newProjectDialog.ShowModal() == wxID_OK)
    {
        ClearCurrentProject();
        m_project = newProjectDialog.GetProject();
        m_fileHandler->SetProject(m_project);
        ActivateProjectMenu();
        m_screensTreeCtrl->SetProject(m_project);
    }
}

void MainFrame::ClearCurrentProject()
{
    m_canvasAuiNotebook->DeleteAllPages();
    m_screensTreeCtrl->DeleteAllItems();
    m_elementsPropgrid->Clear();
    m_fileHandler->RemoveAll();
    m_project = NULL;
}

void MainFrame::OnAddScreen(wxCommandEvent& event)
{
    Screen* screen = m_project->AddScreen();
    wxTreeItemId treeItem = m_screensTreeCtrl->AppendItem(m_screensTreeCtrl->GetRootItem(), screen->GetName(), -1, -1, new TreeItem(screen));
    NewScreenTab(screen, treeItem);
}

WorkPanel* MainFrame::NewScreenTab(Screen* screen, wxTreeItemId treeItem)
{
    TreeItem* data = ((TreeItem*) m_screensTreeCtrl->GetItemData(treeItem));
    WorkPanel* tabPanel;
    if(data->GetWorkPanel() == NULL)
    {
        tabPanel = new WorkPanel(treeItem, screen, m_project, m_canvasAuiNotebook);
        data->SetWorkPanel(tabPanel);
        m_canvasAuiNotebook->AddPage(tabPanel, screen->GetName(), true, wxNullBitmap);
    }
    else
    {
        tabPanel = data->GetWorkPanel();
        int pageIdx = m_canvasAuiNotebook->GetPageIndex(tabPanel);
        m_canvasAuiNotebook->SetSelection(pageIdx);
    }
    return tabPanel;
}

wxAuiNotebookPage* MainFrame::SetCanvasAuiNotebookPageName(wxWindow* panel, const wxString& name)
{
    int pageIdx = m_canvasAuiNotebook->GetPageIndex(panel);
    m_canvasAuiNotebook->SetPageText(pageIdx, name);
    return (wxAuiNotebookPage*) m_canvasAuiNotebook->GetPage(pageIdx);
}

bool MainFrame::DeleteCanvasAuiNotebookPage(wxWindow* panel)
{
    int pageIdx = m_canvasAuiNotebook->GetPageIndex(panel);
    return m_canvasAuiNotebook->DeletePage(pageIdx);
}

void MainFrame::OnGenerateHtml(wxCommandEvent& event)
{
    HtmlGenerator generator(m_project);
    if(generator.IsGeneratorLoaded())
    {
        if(generator.GenerateProject())
            wxMessageDialog(this, "Screens exported in HTML format", "HTML generation").ShowModal();
        else
            wxMessageDialog(this, "Error during generation of HTML files", "Generator error").ShowModal();
    }
    else
        wxMessageDialog(this, "Template files were not loaded", "Generator error").ShowModal();
}

void MainFrame::OnSimulate(wxCommandEvent& event)
{
    HtmlGenerator generator(m_project);
    if(generator.IsGeneratorLoaded())
    {
        if(!generator.GenerateProject())
            wxMessageDialog(this, "Error during simulation start", "Simulator error").ShowModal();
    }
    else
        wxMessageDialog(this, "Template files were not loaded", "Generator error").ShowModal();

    if(m_simulatorDialog == NULL)
        m_simulatorDialog = new SimulatorDialog(m_project, generator.GetValidIndexUrl(), this);

    m_simulatorDialog->Show();
    m_simulatorDialog->HardReload();
}

void MainFrame::OnCut(wxCommandEvent& event)
{
    wxWindow* currentTab = m_canvasAuiNotebook->GetCurrentPage();
    if(currentTab)
    {
        WorkPanel* workPanel = (WorkPanel*) currentTab;
        workPanel->GetScreenCanvas()->Cut();
    }
}

void MainFrame::OnCopy(wxCommandEvent& event)
{
    wxWindow* currentTab = m_canvasAuiNotebook->GetCurrentPage();
    if(currentTab)
    {
        WorkPanel* workPanel = (WorkPanel*) currentTab;
        workPanel->GetScreenCanvas()->Copy();
    }
}

void MainFrame::OnPaste(wxCommandEvent& event)
{
    wxWindow* currentTab = m_canvasAuiNotebook->GetCurrentPage();
    if(currentTab)
    {
        WorkPanel* workPanel = (WorkPanel*) currentTab;
        workPanel->GetScreenCanvas()->Paste();
    }
}

void MainFrame::OnUpdateCut(wxUpdateUIEvent& event)
{
    wxWindow* currentTab = m_canvasAuiNotebook->GetCurrentPage();
    if(currentTab)
    {
        WorkPanel* workPanel = (WorkPanel*) currentTab;
        event.Enable(workPanel->GetScreenCanvas()->CanCut());
    }
}

void MainFrame::OnUpdateCopy(wxUpdateUIEvent& event)
{
    wxWindow* currentTab = m_canvasAuiNotebook->GetCurrentPage();
    if(currentTab)
    {
        WorkPanel* workPanel = (WorkPanel*) currentTab;
        event.Enable(workPanel->GetScreenCanvas()->CanCopy());
    }
}

void MainFrame::OnUpdatePaste(wxUpdateUIEvent& event)
{
    wxWindow* currentTab = m_canvasAuiNotebook->GetCurrentPage();
    if(currentTab)
    {
        WorkPanel* workPanel = (WorkPanel*) currentTab;
        event.Enable(workPanel->GetScreenCanvas()->CanPaste());
    }
}

void MainFrame::OnUpdateSelectAll(wxUpdateUIEvent& event)
{
    wxWindow* currentTab = m_canvasAuiNotebook->GetCurrentPage();
    event.Enable(currentTab);
}

void MainFrame::OnSelectAll(wxCommandEvent& event)
{
    wxWindow* currentTab = m_canvasAuiNotebook->GetCurrentPage();
    if(currentTab)
    {
        WorkPanel* workPanel = (WorkPanel*) currentTab;
        workPanel->GetScreenCanvas()->SelectAll();
    }
}

void MainFrame::OnGenerateImages(wxCommandEvent& event)
{
    SerializableList& screens = m_project->GetScreenList();
    for(SerializableList::const_iterator i = screens.begin(); i != screens.end(); i++)
    {
        Screen* screen = (Screen*) *i;
        WorkPanel* panel = new WorkPanel(NULL, screen, m_project, this);
        wxFileName generatorPath(m_project->GetPath(), screen->GetFilename(), "png");
        generatorPath.AppendDir("images");
        if(!generatorPath.DirExists())
            if(!generatorPath.Mkdir())
            {
                wxMessageDialog(this, "Could not create directory", "Generator error").ShowModal();
                return;
            }

        if(!panel->GetScreenCanvas()->ExportToImage(generatorPath.GetFullPath(), wxBITMAP_TYPE_PNG))
        {
            wxMessageDialog(this, "Could not export images", "Generator error").ShowModal();
            return;
        }

        delete panel;
    }

    wxMessageDialog(this, "Screens exported in PNG format", "Image generation").ShowModal();
}

void MainFrame::OnUndo(wxCommandEvent& event)
{
    wxWindow* currentTab = m_canvasAuiNotebook->GetCurrentPage();
    if(currentTab)
    {
        WorkPanel* workPanel = (WorkPanel*) currentTab;
        workPanel->GetScreenCanvas()->Undo();
    }
}

void MainFrame::OnUpdateUndo(wxUpdateUIEvent& event)
{
    wxWindow* currentTab = m_canvasAuiNotebook->GetCurrentPage();
    if(currentTab)
    {
        WorkPanel* workPanel = (WorkPanel*) currentTab;
        event.Enable(workPanel->GetScreenCanvas()->CanUndo());
    }
}

void MainFrame::OnRedo(wxCommandEvent& event)
{
    wxWindow* currentTab = m_canvasAuiNotebook->GetCurrentPage();
    if(currentTab)
    {
        WorkPanel* workPanel = (WorkPanel*) currentTab;
        workPanel->GetScreenCanvas()->Redo();
    }
}

void MainFrame::OnUpdateRedo(wxUpdateUIEvent& event)
{
    wxWindow* currentTab = m_canvasAuiNotebook->GetCurrentPage();
    if(currentTab)
    {
        WorkPanel* workPanel = (WorkPanel*) currentTab;
        event.Enable(workPanel->GetScreenCanvas()->CanRedo());
    }
}

void MainFrame::OnIdle(wxIdleEvent& event)
{
    wxString title("emDesigner");

    if(m_project)
    {
        title.Append(" - ");
        if(m_project->IsProjectModified())
            title.Append("*");
        title.Append(m_project->GetName());
    }

    SetTitle(title);
}

void MainFrame::OnAlignLeft(wxCommandEvent& event)
{
    wxWindow* currentTab = m_canvasAuiNotebook->GetCurrentPage();
    if(currentTab)
    {
        WorkPanel* workPanel = (WorkPanel*) currentTab;
        workPanel->GetScreenCanvas()->AlignSelected(wxSFShapeCanvas::halignLEFT, wxSFShapeCanvas::valignNONE);
    }
}

void MainFrame::OnUpdateAlign(wxUpdateUIEvent& event)
{
    wxWindow* currentTab = m_canvasAuiNotebook->GetCurrentPage();
    event.Enable(currentTab);
}

void MainFrame::OnAlignRight(wxCommandEvent& event)
{
    wxWindow* currentTab = m_canvasAuiNotebook->GetCurrentPage();
    if(currentTab)
    {
        WorkPanel* workPanel = (WorkPanel*) currentTab;
        workPanel->GetScreenCanvas()->AlignSelected(wxSFShapeCanvas::halignRIGHT, wxSFShapeCanvas::valignNONE);
    }
}

void MainFrame::OnAlignTop(wxCommandEvent& event)
{
    wxWindow* currentTab = m_canvasAuiNotebook->GetCurrentPage();
    if(currentTab)
    {
        WorkPanel* workPanel = (WorkPanel*) currentTab;
        workPanel->GetScreenCanvas()->AlignSelected(wxSFShapeCanvas::halignNONE, wxSFShapeCanvas::valignTOP);
    }
}

void MainFrame::OnAlignBottom(wxCommandEvent& event)
{
    wxWindow* currentTab = m_canvasAuiNotebook->GetCurrentPage();
    if(currentTab)
    {
        WorkPanel* workPanel = (WorkPanel*) currentTab;
        workPanel->GetScreenCanvas()->AlignSelected(wxSFShapeCanvas::halignNONE, wxSFShapeCanvas::valignBOTTOM);
    }
}
