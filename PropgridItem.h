/***************************************************************
 * Name:      PropgridItem.h
 * Purpose:   Container for storing property grid data
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   03/20/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#ifndef PROPGRIDITEM_H
#define PROPGRIDITEM_H

#include <wx/wxsf/wxShapeFramework.h>
#include <wx/propgrid/propgrid.h>
#include <wx/propgrid/advprops.h>

#include "WorkPanel.h"
#include "Ids.h"

class TreeCtrl;
class MainFrame;
/**
 * @class PropgridItem
 * @author Marek Hrušovský
 * @brief Container class for storing PropertyGrid data and handling data changes
 */
class PropgridItem
{
private:
    /**
     * @brief Main wxFrame of application
     */
    MainFrame* m_mainFrame;
    /**
     * @brief Type of current storage
     */
    ItemType m_itemType;
    /**
     * @brief Workpanel associated with grid item
     */
    WorkPanel* m_workPanel;
    /**
     * @brief Screen associated with grid item
     */
    Screen* m_screen;
    /**
     * @brief Shape associated with grid item
     */
    wxSFShapeBase* m_shape;
    /**
     * @brief Id of TreeItem
     */
    wxTreeItemId m_treeItem;

public:
    /**
     * @brief Constructor
     */
    PropgridItem();
    /**
     * @brief Destructor
     */
    ~PropgridItem();
    /**
     * @brief Clears stored information
     */
    void ClearData();
    /**
     * @brief Set complete TreeItem with screen
     * @param screen Screen object
     * @param treeItem Associated TreeCtrl item
     * @param workPanel Associated work panel
     */
    PropgridItem& SetScreen(Screen* screen, wxTreeItemId treeItem, WorkPanel* workPanel = NULL);
    /**
     * @brief Set complete TreeItem shape
     * @param shape Shape object
     * @param treeItem Associated TreeCtrl item
     * @param screen Screen on which is shape located
     * @param workPanel WorkPanel associated with screen
     * @return Reference to this
     */
    PropgridItem& SetShape(wxSFShapeBase* shape, wxTreeItemId treeItem, Screen* screen, WorkPanel* workPanel = NULL);
    /**
     * @brief Set Screen
     * @param screen Pointer to screen
     * @return Reference to this
     */
    PropgridItem& SetScreen(Screen* screen)
    {
        this->m_screen = screen;
        return *this;
    }
    /**
     * @brief Set WorkPanel
     * @param workPanel Pointer to WorkPanel
     * @return Reference to this
     */
    PropgridItem& SetWorkPanel(WorkPanel* workPanel)
    {
        this->m_workPanel = workPanel;
        return *this;
    }
    /**
     * @brief Get screen
     * @return Pointer to screen
     */
    Screen* GetScreen()
    {
        return m_screen;
    }
    /**
     * @brief Get WorkPanel
     * @return Pointer to WorkPanel
     */
    WorkPanel* GetWorkPanel()
    {
        return m_workPanel;
    }
    /**
     * @brief Set what type of item is currently stored
     * @param itemType Type of item
     * @return Reference to this
     */
    PropgridItem& SetItemType(const ItemType& itemType)
    {
        this->m_itemType = itemType;
        return *this;
    }
    /**
     * @brief Get currently stored ItemType
     * @return ItemType What item is stored
     */
    const ItemType& GetItemType() const
    {
        return m_itemType;
    }
    /**
     * @brief Set TreeItem
     * @param treeItem Set Id of TreeItem
     * @return Reference to this
     */
    PropgridItem& SetTreeItem(const wxTreeItemId& treeItem)
    {
        this->m_treeItem = treeItem;
        return *this;
    }
    /**
     * @brief Get TreeItem
     * @return Id of TreeItem
     */
    const wxTreeItemId& GetTreeItem() const
    {
        return m_treeItem;
    }
    /**
     * @brief Set associated shape
     * @param shape Shape object
     * @return Reference to this
     */
    PropgridItem& SetShape(wxSFShapeBase* shape)
    {
        this->m_shape = shape;
        return *this;
    }
    /**
     * @brief Get associated shape
     * @return Shape object
     */
    wxSFShapeBase* GetShape()
    {
        return m_shape;
    }
    /**
     * @brief Remove associated WorkPanel if current screen is on propgrid
     * @param screen Screen that should be associated with Propgrid
     */
    void RemoveWorkPanel(Screen* screen);
    /**
     * @brief Property of screen is changed
     * @param event Event object containing info about change
     */
    void ScreenPropertyChange(wxPropertyGridEvent& event);
    /**
     * @brief Property of shape is changed
     * @param event Event object containing info about change
     */
    void ShapePropertyChange(wxPropertyGridEvent& event);
    /**
     * @brief Change size of shapes
     * @param property Property object
     */
    void ShapeSizePropertyChange(wxPGProperty* property);
    /**
     * @brief Change of bitmap events
     * @param property Property object
     */
    void BitmapEventPropertyChange(wxPGProperty* property);
    /**
     * @brief Change of event mapped to shape
     * @param property Property object
     */
    void EventsPropertyChange(wxPGProperty* property);
    /**
     * @brief Change of target screen during Screen Change
     * @param property Property object
     */
    void EventScreensPropertyChange(wxPGProperty* property);
    /**
     * @brief Change of fonts
     * @param property Property object
     */
    void FontsPropertyChange(wxPGProperty* property);
    /**
     * @brief Get property with list of screens
     * @return Property with list of screens
     */
    wxEnumProperty* GetScreensArrayProperty();
    /**
     * @brief Get property with image path
     * @return Property with image
     */
    wxImageFileProperty* GetImagePathProperty();
};

#endif // PROPGRIDITEM_H
