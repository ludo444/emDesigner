/***************************************************************
 * Name:      FileHandler.h
 * Purpose:   Used for handling content of files
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   04/05/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#ifndef FILEHANDLER_H
#define FILEHANDLER_H

#include <wx/filename.h>

#include "Project.h"

/**
 * @class FileHandler
 * @author Marek Hrušovský
 * @brief Class used for loading and saving content of files, managind file operations
 */
class FileHandler : public wxXmlSerializer
{
public:
    /**
     * @brief Default constructor
     */
    FileHandler();
    /**
     * @brief Destructor
     */
    ~FileHandler();
    /**
     * @brief Create valid path to file based on platform
     * @param path Path string
     * @param filename Name of file
     * @param extension Extension of file
     * @return Path in wxString
     */
    static wxString CreateFullPath(const wxString& path, const wxString& filename = "", const wxString& extension = "");
    /**
     * @brief Make directory
     * @param path Path to folder
     * @param dir New directory name
     * @return True if directory successfuly created
     */
    static bool MakeDir(const wxString& path, const wxString& dir);
    /**
     * @brief Set Project for FileHandler
     * @param project Project object
     * @return Pointer to this
     */
    const FileHandler& SetProject(Project* project);
    /**
     * @brief Load file to string
     * @param str Pointer to string which should be filled with content of file
     * @param path Path to file
     * @return True if success
     */
    static bool LoadFile(wxString* str, const wxString& path);
    /**
     * @brief Write string to file
     * @param str String to write
     * @param path Path to file
     * @return True if success
     */
    static bool WriteFile(const wxString& str, const wxString& path);
    /**
     * @brief Save Project object to XML file
     * @return True if success
     */
    bool SaveProjectToXml();
    /**
     * @brief Load Project object from XML file
     * @param path Path to project
     */
    Project* LoadProjectFromXml(const wxString& path);
};

#endif // FILEHANDLER_H
