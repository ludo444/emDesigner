///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct  8 2016)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __NEWPROJECTDIALOGBASE_H__
#define __NEWPROJECTDIALOGBASE_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/intl.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/textctrl.h>
#include <wx/valtext.h>
#include <wx/spinctrl.h>
#include <wx/filepicker.h>
#include <wx/checkbox.h>
#include <wx/button.h>
#include <wx/sizer.h>
#include <wx/dialog.h>

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class NewProjectDialogBase
///////////////////////////////////////////////////////////////////////////////
class NewProjectDialogBase : public wxDialog 
{
	private:
	
	protected:
		wxStaticText* m_nameStaticText;
		wxTextCtrl* m_nameTextCtrl;
		wxStaticText* m_widthStaticText;
		wxSpinCtrl* m_widthSpinCtrl;
		wxStaticText* m_heightStaticText;
		wxSpinCtrl* m_heightSpinCtrl;
		wxStaticText* m_dirStaticText;
		wxDirPickerCtrl* m_dirPicker;
		wxCheckBox* m_folderCheckBox;
		wxButton* m_createButton;
		
		// Virtual event handlers, overide them in your derived class
		virtual void OnClose( wxCloseEvent& event ) { event.Skip(); }
		virtual void OnCreate( wxCommandEvent& event ) { event.Skip(); }
		
	
	public:
		
		NewProjectDialogBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("New Project"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE ); 
		~NewProjectDialogBase();
	
};

#endif //__NEWPROJECTDIALOGBASE_H__
