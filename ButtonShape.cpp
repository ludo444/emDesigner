/***************************************************************
 * Name:      ButtonShape.cpp
 * Purpose:   Implementation of ButtonShape
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   05/15/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#include "ButtonShape.h"

XS_IMPLEMENT_CLONABLE_CLASS(ButtonShape, ControlShape);

EventType ButtonShape::s_events[] = {NO_EVENT, SCREEN_CHANGE_EVENT};

int ButtonShape::s_eventsCount = 2;

ButtonShape::~ButtonShape()
{
}

void ButtonShape::InitShape()
{
    XS_SERIALIZE(m_textColor, "button-color");
    XS_SERIALIZE(m_text, "button-text");
    XS_SERIALIZE(m_font, "button-font");
}

ButtonShape::ButtonShape() : ControlShape()
{
    InitShape();
}

ButtonShape::ButtonShape(const ButtonShape& obj) : ControlShape(obj)
{
    m_text = obj.m_text;
    m_textColor = obj.m_textColor;
    m_font = obj.m_font;
    InitShape();
}

ButtonShape::ButtonShape(wxWindow* button, const wxRealPoint& pos, const wxRealPoint& size, wxSFDiagramManager* manager) : ControlShape(button, pos, size, manager)
{
    InitShape();
}

void ButtonShape::OnEndHandle(wxSFShapeHandle& handle)
{
    if(GetParentCanvas()->GetScale() == 1.)
    {
        wxButton* button = new wxButton(GetParentCanvas(), wxID_ANY, m_text, wxDefaultPosition, wxDefaultSize);
        button->SetFont(m_font);
        button->SetForegroundColour(m_textColor);
        button->SetMinSize(wxDefaultSize);
        button->SetMaxSize(wxDefaultSize);

        SetText(button->GetLabel());
        SetFont(button->GetFont());
        SetTextColour(button->GetForegroundColour());
        SetControl(button);
    }

    ControlShape::OnEndHandle(handle);
}

void ButtonShape::SetControl(wxWindow* ctrl, bool fit)
{
    if(ctrl)
    {
        m_text = ctrl->GetLabel();
        m_font = ctrl->GetFont();
        m_textColor = ctrl->GetForegroundColour();
    }

    ControlShape::SetControl(ctrl, fit);
}

void ButtonShape::ProcessScale(double scale)
{
    PostProcess();
    ControlShape::ProcessScale(scale);
}

void ButtonShape::PostProcess()
{
    wxSFShapeCanvas* canvas;
    if(canvas = GetParentCanvas())
        if(GetParentCanvas()->GetScale() == 1.)
        {
            wxButton* button = new wxButton(GetParentCanvas(), wxID_ANY, m_text, wxDefaultPosition, wxDefaultSize);
            button->SetFont(m_font);
            button->SetForegroundColour(m_textColor);
            button->SetMinSize(wxDefaultSize);
            button->SetMaxSize(wxDefaultSize);

            SetText(button->GetLabel());
            SetFont(button->GetFont());
            SetTextColour(button->GetForegroundColour());
            SetControl(button);
        }
}
