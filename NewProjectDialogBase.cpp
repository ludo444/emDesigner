///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct  8 2016)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "NewProjectDialogBase.h"

///////////////////////////////////////////////////////////////////////////

NewProjectDialogBase::NewProjectDialogBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	
	wxFlexGridSizer* fgSizer;
	fgSizer = new wxFlexGridSizer( 0, 2, 0, 0 );
	fgSizer->SetFlexibleDirection( wxBOTH );
	fgSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_nameStaticText = new wxStaticText( this, wxID_ANY, _("Project name:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_nameStaticText->Wrap( -1 );
	fgSizer->Add( m_nameStaticText, 0, wxALL, 5 );
	
	m_nameTextCtrl = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer->Add( m_nameTextCtrl, 0, wxALL|wxEXPAND, 5 );
	
	m_widthStaticText = new wxStaticText( this, wxID_ANY, _("Width of screen:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_widthStaticText->Wrap( -1 );
	fgSizer->Add( m_widthStaticText, 0, wxALL, 5 );
	
	m_widthSpinCtrl = new wxSpinCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 10, 16000, 300 );
	fgSizer->Add( m_widthSpinCtrl, 0, wxALL|wxEXPAND, 5 );
	
	m_heightStaticText = new wxStaticText( this, wxID_ANY, _("Height of screen"), wxDefaultPosition, wxDefaultSize, 0 );
	m_heightStaticText->Wrap( -1 );
	fgSizer->Add( m_heightStaticText, 0, wxALL, 5 );
	
	m_heightSpinCtrl = new wxSpinCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 10, 16000, 300 );
	fgSizer->Add( m_heightSpinCtrl, 0, wxALL|wxEXPAND, 5 );
	
	m_dirStaticText = new wxStaticText( this, wxID_ANY, _("Directory:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_dirStaticText->Wrap( -1 );
	fgSizer->Add( m_dirStaticText, 0, wxALL, 5 );
	
	m_dirPicker = new wxDirPickerCtrl( this, wxID_ANY, wxEmptyString, _("Select a folder"), wxDefaultPosition, wxDefaultSize, wxDIRP_DIR_MUST_EXIST|wxDIRP_USE_TEXTCTRL );
	fgSizer->Add( m_dirPicker, 0, wxALL|wxEXPAND, 5 );
	
	
	fgSizer->Add( 0, 0, 1, wxEXPAND, 5 );
	
	m_folderCheckBox = new wxCheckBox( this, wxID_ANY, _("Create under separate folder"), wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer->Add( m_folderCheckBox, 0, wxALL, 5 );
	
	
	fgSizer->Add( 0, 0, 1, wxEXPAND, 5 );
	
	m_createButton = new wxButton( this, wxID_OK, _("Create project"), wxDefaultPosition, wxDefaultSize, 0 );
	m_createButton->SetDefault(); 
	fgSizer->Add( m_createButton, 0, wxALL, 5 );
	
	
	this->SetSizer( fgSizer );
	this->Layout();
	fgSizer->Fit( this );
	
	this->Centre( wxBOTH );
	
	// Connect Events
	this->Connect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( NewProjectDialogBase::OnClose ) );
	m_createButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( NewProjectDialogBase::OnCreate ), NULL, this );
}

NewProjectDialogBase::~NewProjectDialogBase()
{
	// Disconnect Events
	this->Disconnect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( NewProjectDialogBase::OnClose ) );
	m_createButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( NewProjectDialogBase::OnCreate ), NULL, this );
	
}
