/***************************************************************
 * Name:      FileHandler.cpp
 * Purpose:   Implementation of FileHandler
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   04/05/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#include "FileHandler.h"

#include <wx/file.h>
#include <wx/filename.h>

FileHandler::FileHandler()
{
    SetSerializerRootName("emDesigner");
    SetSerializerVersion("0.1");
}

FileHandler::~FileHandler()
{
}

wxString FileHandler::CreateFullPath(const wxString& path, const wxString& filename, const wxString& extension)
{
    wxFileName file(path, filename, extension);
    return file.GetFullPath();
}

bool FileHandler::MakeDir(const wxString& path, const wxString& dir)
{
    wxFileName dirName(path, dir);
    return wxFileName::Mkdir(dirName.GetFullPath());
}

const FileHandler& FileHandler::SetProject(Project* project)
{
    AddItem(GetRootItem(), project);
    return *this;
}

bool FileHandler::LoadFile(wxString* str, const wxString& path)
{
    wxFile file;
    if(!file.Open(path))
        return false;

    return file.ReadAll(str);
}

bool FileHandler::WriteFile(const wxString& str, const wxString& path)
{
    wxFile file;
    if(!file.Open(path, wxFile::write))
        return false;

    return file.Write(str);
}

bool FileHandler::SaveProjectToXml()
{
    Project* project = (Project*) GetRootItem()->GetFirstChild();
    wxFileName fileName(project->GetPath(), "", "screen");

    SerializableList& screenList = project->GetScreenList();
    for(SerializableList::const_iterator i = screenList.begin(); i != screenList.end(); i++)
    {
        Screen* screen = (Screen*) *i;
        fileName.SetName(screen->GetFilename());
        wxSFDiagramManager* screenManager = screen->GetDiagramManager();
        if(!screenManager->SerializeToXml(fileName.GetFullPath()))
            return false;
        screenManager->SetModified(false);
    }

    project->SetModified(false);
    fileName.SetName(project->GetName());
    fileName.SetExt("emDesigner");
    return SerializeToXml(fileName.GetFullPath());
}

Project* FileHandler::LoadProjectFromXml(const wxString& path)
{
    wxFileName fileName(path);
    if(!DeserializeFromXml(fileName.GetFullPath()))
        return NULL;
    Project* project = (Project*) GetRootItem()->GetFirstChild();
    project->SetPath(fileName.GetPath());

    fileName.SetExt("screen");
    Screen::s_maxId = 0;
    SerializableList& screenList = project->GetScreenList();
    for(SerializableList::const_iterator i = screenList.begin(); i != screenList.end(); i++)
    {
        Screen* screen = (Screen*) *i;
        if(Screen::s_maxId < screen->GetId())
            Screen::s_maxId = screen->GetId();

        fileName.SetName(screen->GetFilename());
        wxSFDiagramManager* diagramManager = new wxSFDiagramManager();
        if(!diagramManager->DeserializeFromXml(fileName.GetFullPath()))
            return NULL;
        screen->SetDiagramManager(diagramManager);
    }

    project->PostProcessProject();
    return project;
}
