/***************************************************************
 * Name:      Event.cpp
 * Purpose:   Implementation of event classes
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   05/07/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#include "Event.h"

#include <wx/filename.h>

////////////////////////////////////////////////////////////////////////////
// SCREEN CHANGE EVENT
XS_IMPLEMENT_CLONABLE_CLASS(ScreenChangeEvent, wxSFShapeBase);

ScreenChangeEvent::ScreenChangeEvent() : wxSFShapeBase()
{
    InitEvent();
}

ScreenChangeEvent::ScreenChangeEvent(const ScreenChangeEvent& obj) : wxSFShapeBase(obj)
{
    m_newScreenId = obj.m_newScreenId;
    InitEvent();
}

ScreenChangeEvent::~ScreenChangeEvent()
{
}

void ScreenChangeEvent::InitEvent()
{
    XS_SERIALIZE_INT(m_newScreenId, "new-screen-id");
}

////////////////////////////////////////////////////////////////////////////
// BITMAP CHANGE EVENT
XS_IMPLEMENT_CLONABLE_CLASS(BitmapChangeEvent, wxSFShapeBase);

BitmapChangeEvent::BitmapChangeEvent() : wxSFShapeBase()
{
    InitEvent();
}

BitmapChangeEvent::BitmapChangeEvent(wxString oldBitmapPath) : wxSFShapeBase(), m_oldBitmapPath(oldBitmapPath)
{
    InitEvent();
}

BitmapChangeEvent::BitmapChangeEvent(const BitmapChangeEvent& obj) : wxSFShapeBase(obj)
{
    m_oldBitmapPath = obj.m_oldBitmapPath;
    m_newBitmapPath = obj.m_newBitmapPath;
    InitEvent();
}

BitmapChangeEvent::~BitmapChangeEvent()
{
}

void BitmapChangeEvent::InitEvent()
{
    XS_SERIALIZE(m_oldBitmapPath, "old-bitmap-path");
    XS_SERIALIZE(m_newBitmapPath, "new-bitmap-path");
}

void BitmapChangeEvent::MakeNewPathRelativeTo(const wxString& path)
{
    wxFileName fileNew(m_newBitmapPath);
    fileNew.MakeRelativeTo(path);
    m_newBitmapPath = fileNew.GetFullPath();
}
