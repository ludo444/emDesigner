/*********************************************************************
 * Name:      	main.cpp
 * Purpose:   	Implements wxWidgets application and main application
 *              loop
 * Author:      Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:     01/26/17
 * Copyright:   Marek Hrušovský
 * License:   	GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 *
 * Notes:		Note that all main wxFrame GUI creation code is
 *              declared in gui_base.h source file which is generated
 *              by wxFormBuilder.
 *********************************************************************/

#include "main.h"

// initialize the application
IMPLEMENT_APP(MainApp);

bool MainApp::OnInit()
{
    SetTopWindow(new MainFrame(NULL));
    GetTopWindow()->Show();

    // true = enter the main loop
    return true;
}
