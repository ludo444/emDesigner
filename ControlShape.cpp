/***************************************************************
 * Name:      ControlShape.cpp
 * Purpose:   Reimplementation of control shape from wxShapeFramwork
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   05/15/17
 * Copyright: original done by Michal Bližňák
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/

#include "ControlShape.h"

#ifdef _DEBUG_MSVC
#define new DEBUG_NEW
#endif

#include "wx/wxsf/ControlShape.h"
#include "wx/wxsf/DiagramManager.h"
#include "wx/wxsf/ShapeCanvas.h"

XS_IMPLEMENT_CLONABLE_CLASS(ControlShape, wxSFRectShape);

ControlShape::ControlShape() : wxSFRectShape()
{
    m_pControl = NULL;
    m_nProcessEvents = sfdvCONTROLSHAPE_PROCESSEVENTS;
    m_ModFill = sfdvCONTROLSHAPE_MODFILL;
    m_ModBorder = sfdvCONTROLSHAPE_MODBORDER;
    m_nControlOffset = sfdvCONTROLSHAPE_CONTROLOFFSET;

    m_pEventSink = new emEventSink(this);

    m_Fill = *wxTRANSPARENT_BRUSH;
    m_Border = *wxTRANSPARENT_PEN;

    MarkSerializableDataMembers();
}

ControlShape::ControlShape(wxWindow *ctrl, const wxRealPoint& pos, const wxRealPoint& size, wxSFDiagramManager* manager)
    : wxSFRectShape(pos, size, manager)
{
    SetControl(ctrl);
    m_nProcessEvents = sfdvCONTROLSHAPE_PROCESSEVENTS;
    m_ModFill = sfdvCONTROLSHAPE_MODFILL;
    m_ModBorder = sfdvCONTROLSHAPE_MODBORDER;
    m_nControlOffset = sfdvCONTROLSHAPE_CONTROLOFFSET;

    m_pEventSink = new emEventSink(this);

    m_Fill = *wxTRANSPARENT_BRUSH;
    m_Border = *wxTRANSPARENT_PEN;

    MarkSerializableDataMembers();
}

ControlShape::ControlShape(const ControlShape& obj)
    : wxSFRectShape(obj)
{
    m_pControl = NULL;
    m_nProcessEvents = obj.m_nProcessEvents;
    m_ModFill = obj.m_ModFill;
    m_ModBorder = obj.m_ModBorder;
    m_nControlOffset = obj.m_nControlOffset;

    m_pEventSink = new emEventSink(this);

    MarkSerializableDataMembers();
}

ControlShape::~ControlShape()
{
    if(m_pControl) m_pControl->Destroy();

    if(m_pEventSink) delete m_pEventSink;
}

void ControlShape::MarkSerializableDataMembers()
{
    XS_SERIALIZE_EX(m_nProcessEvents, wxT("process_events"), sfdvCONTROLSHAPE_PROCESSEVENTS);
    XS_SERIALIZE_EX(m_nControlOffset, wxT("offset"), sfdvCONTROLSHAPE_CONTROLOFFSET);
    XS_SERIALIZE_EX(m_ModFill, wxT("modification_fill"), sfdvCONTROLSHAPE_MODFILL);
    XS_SERIALIZE_EX(m_ModBorder, wxT("modification_border"), sfdvCONTROLSHAPE_MODBORDER);
}

//----------------------------------------------------------------------------------//
// public functions
//----------------------------------------------------------------------------------//

void ControlShape::SetControl(wxWindow *ctrl, bool fit)
{
    if(m_pControl) m_pControl->Reparent(m_pPrevParent);

    m_pControl = ctrl;

    if(m_pControl)
    {
        m_pPrevParent = ctrl->GetParent();

        if(m_pParentManager)
        {
            wxSFShapeCanvas *pCanvas = ((wxSFDiagramManager*)m_pParentManager)->GetShapeCanvas();

            // reparent GUI control if necessary
            if(pCanvas && ((wxWindow*)pCanvas != m_pPrevParent)) m_pControl->Reparent((wxWindow*)pCanvas);

            // redirect mouse events to the event sink for their delayed processing
            m_pControl->Connect(wxEVT_LEFT_DOWN, wxMouseEventHandler(emEventSink::_OnMouseButton), NULL, m_pEventSink);
            m_pControl->Connect(wxEVT_RIGHT_DOWN, wxMouseEventHandler(emEventSink::_OnMouseButton), NULL, m_pEventSink);
            m_pControl->Connect(wxEVT_LEFT_UP, wxMouseEventHandler(emEventSink::_OnMouseButton), NULL, m_pEventSink);
            m_pControl->Connect(wxEVT_RIGHT_UP, wxMouseEventHandler(emEventSink::_OnMouseButton), NULL, m_pEventSink);
            m_pControl->Connect(wxEVT_LEFT_DCLICK, wxMouseEventHandler(emEventSink::_OnMouseButton), NULL, m_pEventSink);
            m_pControl->Connect(wxEVT_RIGHT_DCLICK, wxMouseEventHandler(emEventSink::_OnMouseButton), NULL, m_pEventSink);
            m_pControl->Connect(wxEVT_MOTION, wxMouseEventHandler(emEventSink::_OnMouseMove), NULL, m_pEventSink);
            m_pControl->Connect(wxEVT_KEY_DOWN, wxKeyEventHandler(emEventSink::_OnKeyDown), NULL, m_pEventSink);
            m_pControl->Connect(wxEVT_SIZE, wxSizeEventHandler(emEventSink::_OnSize), NULL, m_pEventSink);
        }

        if(fit) UpdateShape();

        UpdateControl();
    }
}

//----------------------------------------------------------------------------------//
// public virtual functions
//----------------------------------------------------------------------------------//

void ControlShape::FitToChildren()
{
    wxRect ctrlRct;
    wxRect bbRct = GetBoundingBox();

    if(m_pControl) ctrlRct = wxRect(m_pControl->GetPosition(), m_pControl->GetSize());
    else
        ctrlRct = bbRct;

    wxSFRectShape::FitToChildren();

    if(bbRct.Intersects(ctrlRct) && !bbRct.Contains(ctrlRct)) UpdateShape();
}

void ControlShape::Scale(double x, double y, bool children)
{
    wxSFRectShape::Scale(x, y, children);
    UpdateControl();
}

void ControlShape::MoveTo(double x, double y)
{
    wxSFRectShape::MoveTo(x, y);
    UpdateControl();
}

void ControlShape::MoveBy(double x, double y)
{
    wxSFRectShape::MoveBy(x, y);
    UpdateControl();
}

void ControlShape::OnBeginDrag(const wxPoint& pos)
{
    wxUnusedVar(pos);

    m_PrevFill = m_Fill;
    m_Fill = m_ModFill;

    if(m_pParentManager)
    {
        wxSFShapeCanvas *pCanvas = ((wxSFDiagramManager*)m_pParentManager)->GetShapeCanvas();

        if(pCanvas)
        {
            m_nPrevStyle = pCanvas->GetStyle();
            pCanvas->RemoveStyle(wxSFShapeCanvas::sfsDND);
        }
    }

    if(m_pControl)
    {
        m_pControl->Hide();
        m_pControl->Disconnect(wxEVT_SIZE, wxSizeEventHandler(emEventSink::_OnSize), NULL, m_pEventSink);
    }

    wxSFShapeBase::OnBeginDrag(pos);
}

void ControlShape::OnEndDrag(const wxPoint& pos)
{
    m_Fill = m_PrevFill;

    if(m_pParentManager)
    {
        wxSFShapeCanvas *pCanvas = ((wxSFDiagramManager*)m_pParentManager)->GetShapeCanvas();

        if(pCanvas)
        {
            pCanvas->SetStyle(m_nPrevStyle);

            double scale = pCanvas->GetScale();
            wxSize canvasSize = GetParentCanvas()->GetSize();
            canvasSize.x /= scale;
            canvasSize.y /= scale;
            wxSize shapeSize = GetBoundingBox().GetSize();
            wxRealPoint topPosition = GetAbsolutePosition();
            wxRealPoint bottomPositon(topPosition.x + shapeSize.GetX(), topPosition.y + shapeSize.GetY());
            wxRealPoint newTopPosition(topPosition);

            if(topPosition.x < 0.)
                newTopPosition.x = 0.;
            if(topPosition.y < 0.)
                newTopPosition.y = 0.;

            if(bottomPositon.x > canvasSize.GetX())
                newTopPosition.x = canvasSize.GetX() - shapeSize.GetX();
            if(bottomPositon.y > canvasSize.GetY())
                newTopPosition.y = canvasSize.GetY() - shapeSize.GetY();

            MoveTo(newTopPosition.x, newTopPosition.y);
        }
    }

    UpdateControl();

    if(m_pControl)
    {
        m_pControl->Connect(wxEVT_SIZE, wxSizeEventHandler(emEventSink::_OnSize), NULL, m_pEventSink);

        m_pControl->Show();
        //m_pControl->SetFocus();
    }

    Refresh();

    wxSFShapeBase::OnEndDrag(pos);
}

void ControlShape::OnBeginHandle(wxSFShapeHandle& handle)
{
    m_PrevBorder = m_Border;
    m_Border = m_ModBorder;

    m_PrevFill = m_Fill;
    m_Fill = m_ModFill;

    if(m_pParentManager)
    {
        wxSFShapeCanvas *pCanvas = ((wxSFDiagramManager*)m_pParentManager)->GetShapeCanvas();

        if(pCanvas)
        {
            m_nPrevStyle = pCanvas->GetStyle();
            pCanvas->RemoveStyle(wxSFShapeCanvas::sfsDND);
        }
    }

    if(m_pControl)
    {
        //m_pControl->Hide();
        m_pControl->Disconnect(wxEVT_LEFT_DOWN, wxMouseEventHandler(emEventSink::_OnMouseButton), NULL, m_pEventSink);
        m_pControl->Disconnect(wxEVT_RIGHT_DOWN, wxMouseEventHandler(emEventSink::_OnMouseButton), NULL, m_pEventSink);
        m_pControl->Disconnect(wxEVT_LEFT_UP, wxMouseEventHandler(emEventSink::_OnMouseButton), NULL, m_pEventSink);
        m_pControl->Disconnect(wxEVT_RIGHT_UP, wxMouseEventHandler(emEventSink::_OnMouseButton), NULL, m_pEventSink);
        m_pControl->Disconnect(wxEVT_LEFT_DCLICK, wxMouseEventHandler(emEventSink::_OnMouseButton), NULL, m_pEventSink);
        m_pControl->Disconnect(wxEVT_RIGHT_DCLICK, wxMouseEventHandler(emEventSink::_OnMouseButton), NULL, m_pEventSink);
        m_pControl->Disconnect(wxEVT_MOTION, wxMouseEventHandler(emEventSink::_OnMouseMove), NULL, m_pEventSink);
        m_pControl->Disconnect(wxEVT_KEY_DOWN, wxKeyEventHandler(emEventSink::_OnKeyDown), NULL, m_pEventSink);
        m_pControl->Disconnect(wxEVT_SIZE, wxSizeEventHandler(emEventSink::_OnSize), NULL, m_pEventSink);
        m_pControl->Destroy();
        m_pControl = NULL;
        SetControl(NULL);
    }

    // call default handler
    wxSFRectShape::OnBeginHandle(handle);
}

void ControlShape::OnHandle(wxSFShapeHandle& handle)
{
    // call default handler
    wxSFRectShape::OnHandle(handle);

    //UpdateControl();
}

void ControlShape::OnEndHandle(wxSFShapeHandle& handle)
{
    m_Border = m_PrevBorder;
    m_Fill = m_PrevFill;

    // call default handler
    wxSFRectShape::OnEndHandle(handle);

    if(m_pControl)
    {
        m_pControl->Show();

        m_pControl->Connect(wxEVT_LEFT_DOWN, wxMouseEventHandler(emEventSink::_OnMouseButton), NULL, m_pEventSink);
        m_pControl->Connect(wxEVT_RIGHT_DOWN, wxMouseEventHandler(emEventSink::_OnMouseButton), NULL, m_pEventSink);
        m_pControl->Connect(wxEVT_LEFT_UP, wxMouseEventHandler(emEventSink::_OnMouseButton), NULL, m_pEventSink);
        m_pControl->Connect(wxEVT_RIGHT_UP, wxMouseEventHandler(emEventSink::_OnMouseButton), NULL, m_pEventSink);
        m_pControl->Connect(wxEVT_LEFT_DCLICK, wxMouseEventHandler(emEventSink::_OnMouseButton), NULL, m_pEventSink);
        m_pControl->Connect(wxEVT_RIGHT_DCLICK, wxMouseEventHandler(emEventSink::_OnMouseButton), NULL, m_pEventSink);
        m_pControl->Connect(wxEVT_MOTION, wxMouseEventHandler(emEventSink::_OnMouseMove), NULL, m_pEventSink);
        m_pControl->Connect(wxEVT_KEY_DOWN, wxKeyEventHandler(emEventSink::_OnKeyDown), NULL, m_pEventSink);
        m_pControl->Connect(wxEVT_SIZE, wxSizeEventHandler(emEventSink::_OnSize), NULL, m_pEventSink);
    }

    UpdateControl();
}

void ControlShape::Update()
{
    wxSFShapeBase::Update();
    UpdateControl();
}

void ControlShape::PrepareScale()
{
    m_PrevFill = m_Fill;
    m_Fill = m_ModFill;

    if(m_pControl)
    {
        //m_pControl->Hide();
        m_pControl->Disconnect(wxEVT_LEFT_DOWN, wxMouseEventHandler(emEventSink::_OnMouseButton), NULL, m_pEventSink);
        m_pControl->Disconnect(wxEVT_RIGHT_DOWN, wxMouseEventHandler(emEventSink::_OnMouseButton), NULL, m_pEventSink);
        m_pControl->Disconnect(wxEVT_LEFT_UP, wxMouseEventHandler(emEventSink::_OnMouseButton), NULL, m_pEventSink);
        m_pControl->Disconnect(wxEVT_RIGHT_UP, wxMouseEventHandler(emEventSink::_OnMouseButton), NULL, m_pEventSink);
        m_pControl->Disconnect(wxEVT_LEFT_DCLICK, wxMouseEventHandler(emEventSink::_OnMouseButton), NULL, m_pEventSink);
        m_pControl->Disconnect(wxEVT_RIGHT_DCLICK, wxMouseEventHandler(emEventSink::_OnMouseButton), NULL, m_pEventSink);
        m_pControl->Disconnect(wxEVT_MOTION, wxMouseEventHandler(emEventSink::_OnMouseMove), NULL, m_pEventSink);
        m_pControl->Disconnect(wxEVT_KEY_DOWN, wxKeyEventHandler(emEventSink::_OnKeyDown), NULL, m_pEventSink);
        m_pControl->Disconnect(wxEVT_SIZE, wxSizeEventHandler(emEventSink::_OnSize), NULL, m_pEventSink);
        m_pControl->Destroy();
        m_pControl = NULL;
        SetControl(NULL);
    }
}

void ControlShape::ProcessScale(double scale)
{
    if(m_pControl)
    {
        m_Border = m_PrevBorder;
        m_Fill = m_PrevFill;

        UpdateControl();

        m_pControl->Show();
        m_pControl->Connect(wxEVT_SIZE, wxSizeEventHandler(emEventSink::_OnSize), NULL, m_pEventSink);
        m_pControl->Connect(wxEVT_LEFT_DOWN, wxMouseEventHandler(emEventSink::_OnMouseButton), NULL, m_pEventSink);
        m_pControl->Connect(wxEVT_RIGHT_DOWN, wxMouseEventHandler(emEventSink::_OnMouseButton), NULL, m_pEventSink);
        m_pControl->Connect(wxEVT_LEFT_UP, wxMouseEventHandler(emEventSink::_OnMouseButton), NULL, m_pEventSink);
        m_pControl->Connect(wxEVT_RIGHT_UP, wxMouseEventHandler(emEventSink::_OnMouseButton), NULL, m_pEventSink);
        m_pControl->Connect(wxEVT_LEFT_DCLICK, wxMouseEventHandler(emEventSink::_OnMouseButton), NULL, m_pEventSink);
        m_pControl->Connect(wxEVT_RIGHT_DCLICK, wxMouseEventHandler(emEventSink::_OnMouseButton), NULL, m_pEventSink);
        m_pControl->Connect(wxEVT_MOTION, wxMouseEventHandler(emEventSink::_OnMouseMove), NULL, m_pEventSink);
        m_pControl->Connect(wxEVT_KEY_DOWN, wxKeyEventHandler(emEventSink::_OnKeyDown), NULL, m_pEventSink);
        m_pControl->Connect(wxEVT_SIZE, wxSizeEventHandler(emEventSink::_OnSize), NULL, m_pEventSink);
    }
}

void ControlShape::OnMouseOver(const wxPoint& pos)
{
    if(m_pControl)
        m_pControl->Hide();
}

void ControlShape::OnMouseLeave(const wxPoint& pos)
{
    if(m_pControl)
        m_pControl->Show();
}

//----------------------------------------------------------------------------------//
// protected functions
//----------------------------------------------------------------------------------//

void ControlShape::UpdateControl()
{
    if(m_pControl)
    {
        wxSFShapeCanvas* canvas;
        double scale = 1.;
        if(canvas = GetParentCanvas())
            scale = canvas->GetScale();

        int x = 0, y = 0;

        wxRect minBB = m_pControl->GetMinSize();
        wxRect rctBB = GetBoundingBox().Deflate(m_nControlOffset, m_nControlOffset);

        if(rctBB.GetWidth() < minBB.GetWidth())
        {
            rctBB.SetWidth(minBB.GetWidth());
            m_nRectSize.x = minBB.GetWidth() + 2*m_nControlOffset;
        }

        if(rctBB.GetHeight() < minBB.GetHeight())
        {
            rctBB.SetHeight(minBB.GetHeight());
            m_nRectSize.y = minBB.GetHeight() + 2*m_nControlOffset;
        }

        //GetParentCanvas()->CalcUnscrolledPosition(0, 0, &x, &y);

        // set the control's dimensions and position according to the parent control shape
        m_pControl->SetMinSize(wxDefaultSize);
        m_pControl->SetMaxSize(wxDefaultSize);
        m_pControl->SetSize(rctBB.GetWidth() * scale, rctBB.GetHeight() * scale);
        //m_pControl->SetSize(rctBB.GetWidth(), rctBB.GetHeight());
        m_pControl->Move((rctBB.GetLeft() - x) * scale, (rctBB.GetTop() - y) * scale);
        //m_pControl->Move((rctBB.GetLeft() - x), (rctBB.GetTop() - y));
    }
}

void ControlShape::UpdateShape()
{
    if(m_pControl)
    {
        wxSize nCtrlSize = m_pControl->GetSize();

        m_nRectSize.x = nCtrlSize.x + 2*m_nControlOffset;
        m_nRectSize.y = nCtrlSize.y + 2*m_nControlOffset;

        GetShapeManager()->GetShapeCanvas()->Refresh(false);
    }
}

void ControlShape::PostProcess()
{
    // process in children
}

//----------------------------------------------------------------------------------//
// private functions
//----------------------------------------------------------------------------------//


//----------------------------------------------------------------------------------//
// EventSink class
//----------------------------------------------------------------------------------//

emEventSink::emEventSink()
{
    m_pParentShape = NULL;
}

emEventSink::emEventSink(ControlShape *parent)
{
    wxASSERT(parent);
    m_pParentShape = parent;
}

emEventSink::~emEventSink()
{
}

//----------------------------------------------------------------------------------//
// public functions
//----------------------------------------------------------------------------------//

void emEventSink::_OnMouseButton(wxMouseEvent &event)
{
    if(m_pParentShape->GetEventProcessing() & ControlShape::evtMOUSE2CANVAS)
    {
        wxMouseEvent updatedEvent(event);

        UpdateMouseEvent(updatedEvent);
        SendEvent(updatedEvent);
    }

    // process the event also by an original handler if requested
    if(m_pParentShape->GetEventProcessing() & ControlShape::evtMOUSE2GUI) event.Skip();

    //m_pParentShape->GetControl()->SetFocus();
}

void emEventSink::_OnMouseMove(wxMouseEvent &event)
{
    if(m_pParentShape->GetEventProcessing() & ControlShape::evtMOUSE2CANVAS)
    {
        wxMouseEvent updatedEvent(event);

        UpdateMouseEvent(updatedEvent);
        SendEvent(updatedEvent);
    }

    // process the event also by an original handler if requested
    if(m_pParentShape->GetEventProcessing() & ControlShape::evtMOUSE2GUI) event.Skip();
}

void emEventSink::_OnKeyDown(wxKeyEvent &event)
{
    if(m_pParentShape->GetEventProcessing() & ControlShape::evtKEY2CANVAS) SendEvent(event);

    // process the event also by an original handler if requested
    if(m_pParentShape->GetEventProcessing() & ControlShape::evtKEY2GUI) event.Skip();
}

void emEventSink::_OnSize(wxSizeEvent &event)
{
    event.Skip();

    m_pParentShape->UpdateShape();
}

//----------------------------------------------------------------------------------//
// protected functions
//----------------------------------------------------------------------------------//

void emEventSink::SendEvent(wxEvent &event)
{
    if(m_pParentShape && m_pParentShape->GetParentManager())
    {
        wxSFShapeCanvas *pCanvas = ((wxSFDiagramManager*)m_pParentShape->GetParentManager())->GetShapeCanvas();

        // send copy of the event to the shape canvas
        if(pCanvas) wxPostEvent(pCanvas, event);
    }
}

void emEventSink::UpdateMouseEvent(wxMouseEvent &event)
{
    int x = 0, y = 0;
    wxRealPoint nAbsPos = m_pParentShape->GetAbsolutePosition();

    //m_pParentShape->GetParentCanvas()->CalcUnscrolledPosition(0, 0, &x, &y);

    event.m_x += ((int)nAbsPos.x + m_pParentShape->GetControlOffset() - x);
    event.m_y += ((int)nAbsPos.y + m_pParentShape->GetControlOffset() - y);
}
