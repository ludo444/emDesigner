///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct  8 2016)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "MainFrameBase.h"

///////////////////////////////////////////////////////////////////////////

MainFrameBase::MainFrameBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	m_mgr.SetManagedWindow(this);
	m_mgr.SetFlags(wxAUI_MGR_DEFAULT);
	
	m_menuBar = new wxMenuBar( 0 );
	m_fileMenu = new wxMenu();
	wxMenuItem* m_openMenuItem;
	m_openMenuItem = new wxMenuItem( m_fileMenu, wxID_ANY, wxString( _("Open") ) + wxT('\t') + wxT("Ctrl+O"), wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	m_openMenuItem->SetBitmaps( wxArtProvider::GetBitmap( wxART_FILE_OPEN, wxART_MENU ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	m_openMenuItem->SetBitmap( wxArtProvider::GetBitmap( wxART_FILE_OPEN, wxART_MENU ) );
	#endif
	m_fileMenu->Append( m_openMenuItem );
	
	m_saveMenuItem = new wxMenuItem( m_fileMenu, wxID_ANY, wxString( _("Save") ) + wxT('\t') + wxT("Ctrl+S"), wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	m_saveMenuItem->SetBitmaps( wxArtProvider::GetBitmap( wxART_FILE_SAVE, wxART_MENU ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	m_saveMenuItem->SetBitmap( wxArtProvider::GetBitmap( wxART_FILE_SAVE, wxART_MENU ) );
	#endif
	m_fileMenu->Append( m_saveMenuItem );
	m_saveMenuItem->Enable( false );
	
	m_fileMenu->AppendSeparator();
	
	wxMenuItem* m_exitMenuItem;
	m_exitMenuItem = new wxMenuItem( m_fileMenu, wxID_ANY, wxString( _("Exit") ) + wxT('\t') + wxT("Alt+X"), wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	m_exitMenuItem->SetBitmaps( wxArtProvider::GetBitmap( wxART_QUIT, wxART_MENU ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	m_exitMenuItem->SetBitmap( wxArtProvider::GetBitmap( wxART_QUIT, wxART_MENU ) );
	#endif
	m_fileMenu->Append( m_exitMenuItem );
	
	m_menuBar->Append( m_fileMenu, _("File") ); 
	
	m_editMenu = new wxMenu();
	wxMenuItem* m_undoMenuItem;
	m_undoMenuItem = new wxMenuItem( m_editMenu, wxID_ANY, wxString( _("Undo") ) + wxT('\t') + wxT("Ctrl+Z"), wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	m_undoMenuItem->SetBitmaps( wxArtProvider::GetBitmap( wxART_UNDO, wxART_MENU ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	m_undoMenuItem->SetBitmap( wxArtProvider::GetBitmap( wxART_UNDO, wxART_MENU ) );
	#endif
	m_editMenu->Append( m_undoMenuItem );
	m_undoMenuItem->Enable( false );
	
	wxMenuItem* m_redoMenuItem;
	m_redoMenuItem = new wxMenuItem( m_editMenu, wxID_ANY, wxString( _("Redo") ) + wxT('\t') + wxT("Ctrl+Y"), wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	m_redoMenuItem->SetBitmaps( wxArtProvider::GetBitmap( wxART_REDO, wxART_MENU ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	m_redoMenuItem->SetBitmap( wxArtProvider::GetBitmap( wxART_REDO, wxART_MENU ) );
	#endif
	m_editMenu->Append( m_redoMenuItem );
	m_redoMenuItem->Enable( false );
	
	m_editMenu->AppendSeparator();
	
	m_cutMenuItem = new wxMenuItem( m_editMenu, wxID_ANY, wxString( _("Cut") ) + wxT('\t') + wxT("Ctrl+X"), wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	m_cutMenuItem->SetBitmaps( wxArtProvider::GetBitmap( wxART_CUT, wxART_MENU ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	m_cutMenuItem->SetBitmap( wxArtProvider::GetBitmap( wxART_CUT, wxART_MENU ) );
	#endif
	m_editMenu->Append( m_cutMenuItem );
	m_cutMenuItem->Enable( false );
	
	m_copyMenuItem = new wxMenuItem( m_editMenu, wxID_ANY, wxString( _("Copy") ) + wxT('\t') + wxT("Ctrl+C"), wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	m_copyMenuItem->SetBitmaps( wxArtProvider::GetBitmap( wxART_COPY, wxART_MENU ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	m_copyMenuItem->SetBitmap( wxArtProvider::GetBitmap( wxART_COPY, wxART_MENU ) );
	#endif
	m_editMenu->Append( m_copyMenuItem );
	m_copyMenuItem->Enable( false );
	
	m_pasteMenuItem = new wxMenuItem( m_editMenu, wxID_ANY, wxString( _("Paste") ) + wxT('\t') + wxT("Ctrl+V"), wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	m_pasteMenuItem->SetBitmaps( wxArtProvider::GetBitmap( wxART_PASTE, wxART_MENU ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	m_pasteMenuItem->SetBitmap( wxArtProvider::GetBitmap( wxART_PASTE, wxART_MENU ) );
	#endif
	m_editMenu->Append( m_pasteMenuItem );
	m_pasteMenuItem->Enable( false );
	
	m_editMenu->AppendSeparator();
	
	wxMenuItem* m_selectAllMenuItem;
	m_selectAllMenuItem = new wxMenuItem( m_editMenu, wxID_ANY, wxString( _("Select All") ) + wxT('\t') + wxT("Ctrl+A"), wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	m_selectAllMenuItem->SetBitmaps( wxNullBitmap );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	m_selectAllMenuItem->SetBitmap( wxNullBitmap );
	#endif
	m_editMenu->Append( m_selectAllMenuItem );
	m_selectAllMenuItem->Enable( false );
	
	m_editMenu->AppendSeparator();
	
	wxMenuItem* m_alingLeftMenuItem;
	m_alingLeftMenuItem = new wxMenuItem( m_editMenu, wxID_ANY, wxString( _("Align Left") ) , wxEmptyString, wxITEM_NORMAL );
	m_editMenu->Append( m_alingLeftMenuItem );
	m_alingLeftMenuItem->Enable( false );
	
	wxMenuItem* m_alignRightMenuItem;
	m_alignRightMenuItem = new wxMenuItem( m_editMenu, wxID_ANY, wxString( _("Align Right") ) , wxEmptyString, wxITEM_NORMAL );
	m_editMenu->Append( m_alignRightMenuItem );
	m_alignRightMenuItem->Enable( false );
	
	wxMenuItem* m_alignTopMenuItem;
	m_alignTopMenuItem = new wxMenuItem( m_editMenu, wxID_ANY, wxString( _("Align Top") ) , wxEmptyString, wxITEM_NORMAL );
	m_editMenu->Append( m_alignTopMenuItem );
	m_alignTopMenuItem->Enable( false );
	
	wxMenuItem* m_alignBottomMenuItem;
	m_alignBottomMenuItem = new wxMenuItem( m_editMenu, wxID_ANY, wxString( _("Align Bottom") ) , wxEmptyString, wxITEM_NORMAL );
	m_editMenu->Append( m_alignBottomMenuItem );
	m_alignBottomMenuItem->Enable( false );
	
	m_menuBar->Append( m_editMenu, _("Edit") ); 
	
	m_projectMenu = new wxMenu();
	wxMenuItem* m_newProjectMenuItem;
	m_newProjectMenuItem = new wxMenuItem( m_projectMenu, wxID_ANY, wxString( _("New Project") ) + wxT('\t') + wxT("Ctrl+N"), wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	m_newProjectMenuItem->SetBitmaps( wxArtProvider::GetBitmap( wxART_NEW, wxART_MENU ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	m_newProjectMenuItem->SetBitmap( wxArtProvider::GetBitmap( wxART_NEW, wxART_MENU ) );
	#endif
	m_projectMenu->Append( m_newProjectMenuItem );
	
	m_addScreenMenuItem = new wxMenuItem( m_projectMenu, wxID_ANY, wxString( _("Add Screen") ) + wxT('\t') + wxT("Ctrl+T"), wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	m_addScreenMenuItem->SetBitmaps( wxArtProvider::GetBitmap( wxART_ADD_BOOKMARK, wxART_MENU ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	m_addScreenMenuItem->SetBitmap( wxArtProvider::GetBitmap( wxART_ADD_BOOKMARK, wxART_MENU ) );
	#endif
	m_projectMenu->Append( m_addScreenMenuItem );
	m_addScreenMenuItem->Enable( false );
	
	m_menuBar->Append( m_projectMenu, _("Project") ); 
	
	m_generateMenu = new wxMenu();
	m_generateHtmlMenuItem = new wxMenuItem( m_generateMenu, wxID_ANY, wxString( _("Generate HTML") ) + wxT('\t') + wxT("Ctrl+G"), wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	m_generateHtmlMenuItem->SetBitmaps( wxArtProvider::GetBitmap( wxART_NORMAL_FILE, wxART_MENU ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	m_generateHtmlMenuItem->SetBitmap( wxArtProvider::GetBitmap( wxART_NORMAL_FILE, wxART_MENU ) );
	#endif
	m_generateMenu->Append( m_generateHtmlMenuItem );
	m_generateHtmlMenuItem->Enable( false );
	
	m_generateImagesMenuItem = new wxMenuItem( m_generateMenu, wxID_ANY, wxString( _("Generate Images") ) , wxEmptyString, wxITEM_NORMAL );
	m_generateMenu->Append( m_generateImagesMenuItem );
	m_generateImagesMenuItem->Enable( false );
	
	m_menuBar->Append( m_generateMenu, _("Generate") ); 
	
	m_simulationMenu = new wxMenu();
	m_simulateMenuItem = new wxMenuItem( m_simulationMenu, wxID_ANY, wxString( _("Simulate") ) + wxT('\t') + wxT("Alt+S"), wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	m_simulateMenuItem->SetBitmaps( wxArtProvider::GetBitmap( wxART_EXECUTABLE_FILE, wxART_MENU ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	m_simulateMenuItem->SetBitmap( wxArtProvider::GetBitmap( wxART_EXECUTABLE_FILE, wxART_MENU ) );
	#endif
	m_simulationMenu->Append( m_simulateMenuItem );
	m_simulateMenuItem->Enable( false );
	
	m_menuBar->Append( m_simulationMenu, _("Simulation") ); 
	
	m_helpMenu = new wxMenu();
	wxMenuItem* m_aboutMenuItem;
	m_aboutMenuItem = new wxMenuItem( m_helpMenu, wxID_ANY, wxString( _("About") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	m_aboutMenuItem->SetBitmaps( wxArtProvider::GetBitmap( wxART_TIP, wxART_MENU ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	m_aboutMenuItem->SetBitmap( wxArtProvider::GetBitmap( wxART_TIP, wxART_MENU ) );
	#endif
	m_helpMenu->Append( m_aboutMenuItem );
	
	m_menuBar->Append( m_helpMenu, _("Help") ); 
	
	this->SetMenuBar( m_menuBar );
	
	m_canvasAuiNotebook = new wxAuiNotebook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxAUI_NB_DEFAULT_STYLE|wxNO_BORDER );
	m_mgr.AddPane( m_canvasAuiNotebook, wxAuiPaneInfo() .Center() .CaptionVisible( false ).CloseButton( false ).PaneBorder( false ).Movable( false ).Dock().Resizable().FloatingSize( wxDefaultSize ).BottomDockable( false ).TopDockable( false ).LeftDockable( false ).RightDockable( false ).Floatable( false ).CentrePane() );
	
	
	
	m_mgr.Update();
	this->Centre( wxBOTH );
	
	// Connect Events
	this->Connect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( MainFrameBase::OnCloseFrame ) );
	this->Connect( wxEVT_IDLE, wxIdleEventHandler( MainFrameBase::OnIdle ) );
	this->Connect( m_openMenuItem->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnOpen ) );
	this->Connect( m_saveMenuItem->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnSave ) );
	this->Connect( m_exitMenuItem->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnExit ) );
	this->Connect( m_undoMenuItem->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnUndo ) );
	this->Connect( m_undoMenuItem->GetId(), wxEVT_UPDATE_UI, wxUpdateUIEventHandler( MainFrameBase::OnUpdateUndo ) );
	this->Connect( m_redoMenuItem->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnRedo ) );
	this->Connect( m_redoMenuItem->GetId(), wxEVT_UPDATE_UI, wxUpdateUIEventHandler( MainFrameBase::OnUpdateRedo ) );
	this->Connect( m_cutMenuItem->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnCut ) );
	this->Connect( m_cutMenuItem->GetId(), wxEVT_UPDATE_UI, wxUpdateUIEventHandler( MainFrameBase::OnUpdateCut ) );
	this->Connect( m_copyMenuItem->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnCopy ) );
	this->Connect( m_copyMenuItem->GetId(), wxEVT_UPDATE_UI, wxUpdateUIEventHandler( MainFrameBase::OnUpdateCopy ) );
	this->Connect( m_pasteMenuItem->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnPaste ) );
	this->Connect( m_pasteMenuItem->GetId(), wxEVT_UPDATE_UI, wxUpdateUIEventHandler( MainFrameBase::OnUpdatePaste ) );
	this->Connect( m_selectAllMenuItem->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnSelectAll ) );
	this->Connect( m_selectAllMenuItem->GetId(), wxEVT_UPDATE_UI, wxUpdateUIEventHandler( MainFrameBase::OnUpdateSelectAll ) );
	this->Connect( m_alingLeftMenuItem->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnAlignLeft ) );
	this->Connect( m_alingLeftMenuItem->GetId(), wxEVT_UPDATE_UI, wxUpdateUIEventHandler( MainFrameBase::OnUpdateAlign ) );
	this->Connect( m_alignRightMenuItem->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnAlignRight ) );
	this->Connect( m_alignRightMenuItem->GetId(), wxEVT_UPDATE_UI, wxUpdateUIEventHandler( MainFrameBase::OnUpdateAlign ) );
	this->Connect( m_alignTopMenuItem->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnAlignTop ) );
	this->Connect( m_alignTopMenuItem->GetId(), wxEVT_UPDATE_UI, wxUpdateUIEventHandler( MainFrameBase::OnUpdateAlign ) );
	this->Connect( m_alignBottomMenuItem->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnAlignBottom ) );
	this->Connect( m_alignBottomMenuItem->GetId(), wxEVT_UPDATE_UI, wxUpdateUIEventHandler( MainFrameBase::OnUpdateAlign ) );
	this->Connect( m_newProjectMenuItem->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnNewProject ) );
	this->Connect( m_addScreenMenuItem->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnAddScreen ) );
	this->Connect( m_generateHtmlMenuItem->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnGenerateHtml ) );
	this->Connect( m_generateImagesMenuItem->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnGenerateImages ) );
	this->Connect( m_simulateMenuItem->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnSimulate ) );
	this->Connect( m_aboutMenuItem->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnAbout ) );
	m_canvasAuiNotebook->Connect( wxEVT_COMMAND_AUINOTEBOOK_PAGE_CLOSE, wxAuiNotebookEventHandler( MainFrameBase::OnNotebookPageClose ), NULL, this );
}

MainFrameBase::~MainFrameBase()
{
	// Disconnect Events
	this->Disconnect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( MainFrameBase::OnCloseFrame ) );
	this->Disconnect( wxEVT_IDLE, wxIdleEventHandler( MainFrameBase::OnIdle ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnOpen ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnSave ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnExit ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnUndo ) );
	this->Disconnect( wxID_ANY, wxEVT_UPDATE_UI, wxUpdateUIEventHandler( MainFrameBase::OnUpdateUndo ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnRedo ) );
	this->Disconnect( wxID_ANY, wxEVT_UPDATE_UI, wxUpdateUIEventHandler( MainFrameBase::OnUpdateRedo ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnCut ) );
	this->Disconnect( wxID_ANY, wxEVT_UPDATE_UI, wxUpdateUIEventHandler( MainFrameBase::OnUpdateCut ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnCopy ) );
	this->Disconnect( wxID_ANY, wxEVT_UPDATE_UI, wxUpdateUIEventHandler( MainFrameBase::OnUpdateCopy ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnPaste ) );
	this->Disconnect( wxID_ANY, wxEVT_UPDATE_UI, wxUpdateUIEventHandler( MainFrameBase::OnUpdatePaste ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnSelectAll ) );
	this->Disconnect( wxID_ANY, wxEVT_UPDATE_UI, wxUpdateUIEventHandler( MainFrameBase::OnUpdateSelectAll ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnAlignLeft ) );
	this->Disconnect( wxID_ANY, wxEVT_UPDATE_UI, wxUpdateUIEventHandler( MainFrameBase::OnUpdateAlign ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnAlignRight ) );
	this->Disconnect( wxID_ANY, wxEVT_UPDATE_UI, wxUpdateUIEventHandler( MainFrameBase::OnUpdateAlign ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnAlignTop ) );
	this->Disconnect( wxID_ANY, wxEVT_UPDATE_UI, wxUpdateUIEventHandler( MainFrameBase::OnUpdateAlign ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnAlignBottom ) );
	this->Disconnect( wxID_ANY, wxEVT_UPDATE_UI, wxUpdateUIEventHandler( MainFrameBase::OnUpdateAlign ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnNewProject ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnAddScreen ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnGenerateHtml ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnGenerateImages ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnSimulate ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnAbout ) );
	m_canvasAuiNotebook->Disconnect( wxEVT_COMMAND_AUINOTEBOOK_PAGE_CLOSE, wxAuiNotebookEventHandler( MainFrameBase::OnNotebookPageClose ), NULL, this );
	
	m_mgr.UnInit();
	
}
