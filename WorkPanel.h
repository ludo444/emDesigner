/***************************************************************
 * Name:      WorkPanel.h
 * Purpose:   Declaration of work panel for putting to MainFrame
 *            tabs
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   02/26/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#ifndef WORKPANEL_H
#define WORKPANEL_H

#include <wx/treebase.h>
#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/panel.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/string.h>
#include <wx/sizer.h>

#include "ScreenCanvas.h"

class MainFrame;
/**
 * @class WorkPanel
 * @author Marek Hrušovský
 * @brief Panel for adding to MainFrame tabbed notebook, contains canvas and some design adjustment elements
 */
class WorkPanel : public wxPanel
{
private:
    ////////////////////////////////////////////////////////////////////////////
    // WXWIDGETS VARIABLES
    /**
     * @brief Scrolled window to hold zoomable canvas that can change size
     */
    wxScrolledWindow* m_canvasScrolledWindow;
    /**
     * @brief Pointer to ScreenCanvas of this panel
     */
    ScreenCanvas* m_screenCanvas;
    /**
     * @brief Slider for zooming of screen
     */
    wxSlider* m_zoomSlider;
    /**
     * @brief Text showing current zoom level
     */
    wxStaticText* m_zoomValueText;
    /**
     * @brief Pointer to main frame of application
     */
    MainFrame* m_mainFrame;

    ////////////////////////////////////////////////////////////////////////////
    // DATA VARIABLES
    /**
     * @brief Id of TreeItem associated with screen in this panel
     */
    wxTreeItemId m_treeItem;
    /**
     * @brief Screen designed in this panel
     */
    Screen* m_screen;
    /**
     * @brief Current project
     */
    Project* m_project;

    ////////////////////////////////////////////////////////////////////////////
    // EVENT HANDLERS
    /**
     * @brief Process zooming of canvas
     */
    void OnZoom(wxScrollEvent& event);
    /**
     * @brief Double click on zoom slider, resets it
     * @param event Event object
     */
    void OnSliderDClick(wxMouseEvent& event);

    ////////////////////////////////////////////////////////////////////////////
    // PRIVATE METHODS
    /**
     * @brief Set zoom of canvas
     * @param position Position of zoom slider
     */
    void Zoom(int position);

public:
    /**
     * @brief Constructor
     * @param treeItem Id of screen in wxTreeCtrl
     * @param screen Pointer to Screen designed in this panel
     * @param project Pointer to project this panel belongs to Parent window
     * @param parent Parent window
     * @param id Id of window
     * @param pos Position
     * @param size Size
     */
    WorkPanel(wxTreeItemId treeItem, Screen* screen, Project* project, wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize);
    /**
     * @brief Destructor
     */
    ~WorkPanel();
    /**
     * @brief Set background color of canvas
     * @param bgColor Color
     */
    void SetBackgroundColor(const wxColor& bgColor);
    /**
     * @brief Set size of canvas
     * @param size Size of canvas
     */
    void SetCanvasSize(const wxSize& size);
    /**
     * @brief Get screen canvas
     * @return ScreenCanvas object
     */
    ScreenCanvas* GetScreenCanvas()
    {
        return m_screenCanvas;
    }
};

#endif // WORKPANEL_H
