/***************************************************************
 * Name:      HtmlGenerator.cpp
 * Purpose:   Implementation of generator from project
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   03/30/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#include "HtmlGenerator.h"

#include "FileHandler.h"
#include <wx/textfile.h>

// FIXME change template path
wxString HtmlGenerator::s_templatePath("/home/ludo/Codelite/School/emDesigner/resources/generator_html/");

wxString HtmlGenerator::s_fontStyleStrings[] = {"normal", "italic", "oblique"};
wxString HtmlGenerator::s_fontWeightStrings[] = {"normal", "lighter", "bold"};
wxString HtmlGenerator::s_fontUnderlineStrings[] = {"none", "underline"};

HtmlGenerator::HtmlGenerator(Project* project) : ContentGenerator(project)
{
    m_generatorPath.SetPath(project->GetPath());
    m_generatorPath.AppendDir("html");
    if(!m_generatorPath.DirExists())
        m_generatorLoaded = m_generatorPath.Mkdir();

    // project, screens general templates
    wxFileName name(s_templatePath, "template", "html");
    m_generatorLoaded = FileHandler::LoadFile(&m_templateHtml, name.GetFullPath());
    name.SetExt("css");
    m_generatorLoaded = FileHandler::LoadFile(&m_templateCss, name.GetFullPath());

    // shape templates
    wxFileName shapes(name);
    shapes.AppendDir("shapes");
    shapes.SetExt("html");
    shapes.SetName("bitmap");
    m_generatorLoaded = FileHandler::LoadFile(&m_bitmapHtmlTemplate, shapes.GetFullPath());
    shapes.SetName("text");
    m_generatorLoaded = FileHandler::LoadFile(&m_textHtmlTemplate, shapes.GetFullPath());
    shapes.SetName("button");
    m_generatorLoaded = FileHandler::LoadFile(&m_buttonHtmlTemplate, shapes.GetFullPath());
    shapes.SetExt("css");
    shapes.SetName("bitmap");
    m_generatorLoaded = FileHandler::LoadFile(&m_bitmapCssTemplate, shapes.GetFullPath());
    shapes.SetName("text");
    m_generatorLoaded = FileHandler::LoadFile(&m_textCssTemplate, shapes.GetFullPath());
    shapes.SetName("button");
    m_generatorLoaded = FileHandler::LoadFile(&m_buttonCssTemplate, shapes.GetFullPath());

    // events templates
    wxFileName events(name);
    events.AppendDir("events");
    events.SetExt("html");
    events.SetName("screen_change");
    m_generatorLoaded = FileHandler::LoadFile(&m_screenChangeHtmlTemplate, events.GetFullPath());
    events.SetName("bitmap_change");
    m_generatorLoaded = FileHandler::LoadFile(&m_bitmapChangeHtmlTemplate, events.GetFullPath());
    events.SetExt("css");
    m_generatorLoaded = FileHandler::LoadFile(&m_bitmapChangeCssTemplate, events.GetFullPath());
    events.SetExt("js");
    m_generatorLoaded = FileHandler::LoadFile(&m_bitmapChangeJsTemplate, events.GetFullPath());

    // copy jquery
    name.SetExt("js");
    name.SetName("jquery.min");
    m_generatorPath.SetExt("js");
    m_generatorPath.SetName("jquery.min");
    m_generatorLoaded = wxCopyFile(name.GetFullPath(), m_generatorPath.GetFullPath());
}

HtmlGenerator::~HtmlGenerator()
{
}

wxString HtmlGenerator::GetIndexUrl()
{
    m_generatorPath.SetName("index");
    m_generatorPath.SetExt("html");
    return m_generatorPath.GetFullPath();
}

wxString HtmlGenerator::GetValidIndexUrl()
{
    wxString path = GetIndexUrl();
    path.Prepend("file://");
    path.Replace(" ", "%20");
    return path;
}


bool HtmlGenerator::GenerateProject()
{
    if(!m_generatorLoaded)
        return false;

    SerializableList& screenList = m_project->GetScreenList();
    for(SerializableList::const_iterator i = screenList.begin(); i != screenList.end(); i++)
    {
        Screen* screen = (Screen*) *i;
        if(!GenerateScreen(screen))
            return false;
    }

    return true;
}

bool HtmlGenerator::GenerateScreen(Screen* screen)
{
    wxString screenHtmlStr(m_templateHtml);
    wxString screenCssStr(m_templateCss);
    wxString screenJsStr;

    FillProjectVariables(screenHtmlStr);
    FillScreenVariables(screenHtmlStr, screen);
    FillProjectVariables(screenCssStr);
    FillScreenVariables(screenCssStr, screen);

    // generate shapes
    wxString diagramHtmlStr;
    wxString diagramCssStr;
    wxString diagramJsStr;
    SerializableList shapeList = (*screen->GetDiagramManager()).GetRootItem()->GetChildrenList();
    for(SerializableList::const_iterator i = shapeList.begin(); i != shapeList.end(); i++)
    {
        xsSerializable* shape = (xsSerializable*) *i;
        if(shape->GetClassInfo() == CLASSINFO(BitmapShape))
        {
            diagramHtmlStr << GenerateHtmlBitmap((BitmapShape*) shape, screen) << wxString(wxTextFile::GetEOL());
            diagramCssStr << GenerateCssBitmap((BitmapShape*) shape, screen) << wxString(wxTextFile::GetEOL());
            GenerateJsEvents((BitmapShape*) shape, screen, diagramJsStr) << wxString(wxTextFile::GetEOL());;
        }
        else if(shape->GetClassInfo() == CLASSINFO(TextShape))
        {
            diagramHtmlStr << GenerateHtmlText((TextShape*) shape, screen) << wxString(wxTextFile::GetEOL());
            diagramCssStr << GenerateCssText((TextShape*) shape, screen) << wxString(wxTextFile::GetEOL());
        }
        else if(shape->GetClassInfo() == CLASSINFO(ButtonShape))
        {
            diagramHtmlStr << GenerateHtmlButton((ButtonShape*) shape, screen) << wxString(wxTextFile::GetEOL());
            diagramCssStr << GenerateCssButton((ButtonShape*) shape, screen) << wxString(wxTextFile::GetEOL());
        }
    }
    FillDiagram(screenHtmlStr, diagramHtmlStr);
    FillDiagram(screenCssStr, diagramCssStr);
    //FillDiagram(screenJsStr, diagramJsStr);
    screenJsStr = diagramJsStr;

    // write screen file
    if(screen->GetId() == m_project->GetIndexScreenId())
        m_generatorPath.SetName("index");
    else
        m_generatorPath.SetName(wxString::Format(wxT("%i"), screen->GetId()));
    m_generatorPath.SetExt("html");
    FileHandler::WriteFile(screenHtmlStr, m_generatorPath.GetFullPath());

    m_generatorPath.SetName(wxString::Format(wxT("%i"), screen->GetId()));
    m_generatorPath.SetExt("css");
    FileHandler::WriteFile(screenCssStr, m_generatorPath.GetFullPath());

    m_generatorPath.SetExt("js");
    FileHandler::WriteFile(screenJsStr, m_generatorPath.GetFullPath());

    return true;
}

wxString HtmlGenerator::GenerateHtmlBitmap(BitmapShape* shape, Screen* screen)
{
    wxString shapeStr(m_bitmapHtmlTemplate);

    FillProjectVariables(shapeStr);
    FillScreenVariables(shapeStr, screen);
    FillShape(shapeStr, shape);
    FillRectShape(shapeStr, shape);
    FillBitmapShape(shapeStr, shape, "html");

    return GenerateHtmlEvents(shape, screen, shapeStr);
}

wxString HtmlGenerator::GenerateHtmlText(TextShape* shape, Screen* screen)
{
    wxString shapeStr(m_textHtmlTemplate);

    FillProjectVariables(shapeStr);
    FillScreenVariables(shapeStr, screen);
    FillShape(shapeStr, shape);
    FillRectShape(shapeStr, shape);
    FillTextShape(shapeStr, shape, s_fontStyleStrings, s_fontWeightStrings, s_fontUnderlineStrings);

    return GenerateHtmlEvents(shape, screen, shapeStr);
}

wxString HtmlGenerator::GenerateHtmlEvents(wxSFShapeBase* shape, Screen* screen, wxString& shapeStr)
{
    wxSFShapeBase* event = (wxSFShapeBase*) shape->GetFirstChild()->GetFirstChild();
    if(event)
        if(event->GetClassInfo() == CLASSINFO(ScreenChangeEvent))
        {
            wxString eventStr(m_screenChangeHtmlTemplate);
            FillShape(eventStr, shape, &shapeStr);
            FillScreenVariables(eventStr, screen);
            FillScreenChangeEvent(eventStr, (ScreenChangeEvent*) event);
            return eventStr;
        }
        else if(event->GetClassInfo() == CLASSINFO(BitmapChangeEvent))
        {
            wxString eventStr(m_bitmapChangeHtmlTemplate);
            FillShape(eventStr, shape);
            FillScreenVariables(eventStr, screen);
            FillBitmapChangeEvent(eventStr, (BitmapChangeEvent*) event, "html");
            return eventStr;
        }

    return wxString(shapeStr);
}

wxString HtmlGenerator::GenerateCssBitmap(BitmapShape* shape, Screen* screen)
{
    wxString shapeStr(m_bitmapCssTemplate);

    FillProjectVariables(shapeStr);
    FillScreenVariables(shapeStr, screen);
    FillShape(shapeStr, shape);
    FillRectShape(shapeStr, shape);
    FillBitmapShape(shapeStr, shape, "html");

    return GenerateCssEvents(shape, screen, shapeStr);
}

wxString HtmlGenerator::GenerateCssText(TextShape* shape, Screen* screen)
{
    wxString shapeStr(m_textCssTemplate);

    FillProjectVariables(shapeStr);
    FillScreenVariables(shapeStr, screen);
    FillShape(shapeStr, shape);
    FillRectShape(shapeStr, shape);
    FillTextShape(shapeStr, shape, s_fontStyleStrings, s_fontWeightStrings, s_fontUnderlineStrings);

    return shapeStr;
}

wxString HtmlGenerator::GenerateCssEvents(wxSFShapeBase* shape, Screen* screen, wxString& shapeStr)
{
    wxSFShapeBase* event = (wxSFShapeBase*) shape->GetFirstChild()->GetFirstChild();
    if(event)
        if(event->GetClassInfo() == CLASSINFO(BitmapChangeEvent))
        {
            wxString eventStr(m_bitmapChangeCssTemplate);
            FillShape(eventStr, shape);
            FillScreenVariables(eventStr, screen);
            FillRectShape(eventStr, (wxSFRectShape*) shape);
            shapeStr << eventStr << wxString(wxTextFile::GetEOL());
        }

    return wxString(shapeStr);
}

wxString HtmlGenerator::GenerateJsEvents(wxSFShapeBase* shape, Screen* screen, wxString& shapeStr)
{
    wxSFShapeBase* event = (wxSFShapeBase*) shape->GetFirstChild()->GetFirstChild();
    if(event)
        if(event->GetClassInfo() == CLASSINFO(BitmapChangeEvent))
        {
            wxString eventStr(m_bitmapChangeJsTemplate);
            FillShape(eventStr, shape);
            FillScreenVariables(eventStr, screen);
            shapeStr << eventStr << wxString(wxTextFile::GetEOL());
        }

    return wxString(shapeStr);
}

wxString HtmlGenerator::GenerateHtmlButton(ButtonShape* shape, Screen* screen)
{
    wxString shapeStr(m_buttonHtmlTemplate);

    FillProjectVariables(shapeStr);
    FillScreenVariables(shapeStr, screen);
    FillShape(shapeStr, shape);
    FillRectShape(shapeStr, shape);
    FillButtonShape(shapeStr, shape, s_fontStyleStrings, s_fontWeightStrings, s_fontUnderlineStrings);

    return GenerateHtmlEvents(shape, screen, shapeStr);
}

wxString HtmlGenerator::GenerateCssButton(ButtonShape* shape, Screen* screen)
{
    wxString shapeStr(m_buttonCssTemplate);

    FillProjectVariables(shapeStr);
    FillScreenVariables(shapeStr, screen);
    FillShape(shapeStr, shape);
    FillRectShape(shapeStr, shape);
    FillButtonShape(shapeStr, shape, s_fontStyleStrings, s_fontWeightStrings, s_fontUnderlineStrings);

    return shapeStr;
}
