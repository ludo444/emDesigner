/***************************************************************
 * Name:      TreeItem.h
 * Purpose:   Declaration of TreeItem that's showing elements
 *            and screens in wxTreeCtrl
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   02/27/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#ifndef TREEITEM_H
#define TREEITEM_H

#include <wx/treebase.h>
#include <wx/wxsf/ShapeBase.h>

#include "Screen.h"
#include "WorkPanel.h"
#include "Ids.h"

/**
 * @class TreeItem
 * @author Marek Hrušovský
 * @brief Class that data in wxTreeCtrl (screens and elements)
 */
class TreeItem : public wxTreeItemData
{
private:
    /**
     * @brief What type is stored here
     */
    ItemType m_type;
    /**
     * @brief Pointer to Screen object
     */
    Screen* m_screen;
    /**
     * @brief Pointer to shape object
     */
    wxSFShapeBase* m_shape;
    /**
     * @brief WorkPanel associated with this TreeItem/Screen
     */
    WorkPanel* m_workPanel;

public:
    /**
     * @brief Constructor for storing pointer to Screen object
     * @param screen Pointer to screen
     * @param workPanel WorkPanel associated with this TreeItem/Screen
     */
    TreeItem(Screen* screen, WorkPanel* workPanel = NULL);
    /**
     * @brief Constructor for storing shape
     * @param shape Pointer to shape
     * @param screen Pointer to screen on which the shape is located
     */
    TreeItem(wxSFShapeBase* shape, Screen* screen = NULL);
    /**
     * @brief Destructor
     */
    ~TreeItem();
    /**
     * @brief Get stored type
     * @return Stored ItemType
     */
    const ItemType& GetType() const
    {
        return m_type;
    }
    /**
     * @brief Get screen
     * @return Screen pointer
     */
    Screen* GetScreen()
    {
        return m_screen;
    }
    /**
     * @brief Get shape
     * @return Shape pointer
     */
    wxSFShapeBase* GetShape()
    {
        return m_shape;
    }
    /**
     * @brief Get work panel
     * @return WorkPanel associated with TreeItem
     */
    WorkPanel* GetWorkPanel()
    {
        return m_workPanel;
    }
    /**
     * @brief Set WorkPanel associated with item
     * @param workPanel WorkPanel pointer
     * @return Reference to this
     */
    TreeItem& SetWorkPanel(WorkPanel* workPanel)
    {
        this->m_workPanel = workPanel;
        return *this;
    }
    /**
     * @brief Set shape
     * @param shape Set shape pointer
     * @return Reference to this
     */
    TreeItem& SetShape(wxSFShapeBase* shape)
    {
        this->m_shape = shape;
        return *this;
    }

};

#endif // TREEITEM_H
