/***************************************************************
 * Name:      Propgrid.h
 * Purpose:
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   05/06/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
/***************************************************************
 * Name:      Propgrid.h
 * Purpose:   Propgrid class declaration for editing elements and screens
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   05/02/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#ifndef PROPGRID_H
#define PROPGRID_H

#include <wx/propgrid/propgrid.h>
#include <wx/propgrid/advprops.h>

#include "Screen.h"
#include "PropgridItem.h"
#include "CommonShapeData.h"

class MainFrame;
/**
 * @class Propgrid
 * @author Marek Hrušovský
 * @brief Property grid for editing elements and screens
 */
class Propgrid : public wxPropertyGrid
{
private:
    /**
     * @brief Propgrid item object storing data for property grid
     */
    PropgridItem m_data;
    /**
     * @brief Main wxFrame of application
     */
    MainFrame* m_mainFrame;
    /**
     * @brief On change of property value
     * @param event Event object
     */
    void OnPropertyChange(wxPropertyGridEvent& event) ;

public:
    /**
     * @brief Constructor
     * @param parent Parent window
     * @param size Size of grid
     */
    Propgrid(wxWindow *parent, const wxSize& size = wxDefaultSize);
    /**
     * @brief Destructor
     */
    ~Propgrid();
    /**
     * @brief Get data associated with property grid
     * @return Propgrid data
     */
    PropgridItem& GetData()
    {
        return m_data;
    }
    /**
     * @brief Clear propgrid and associated data
     */
    void Clear();
    /**
     * @brief Sets property grid to selected Screen
     * @param screen Pointer to Screen
     * @param treeItem TreeItem object ID
     * @param panel Pointer to WorkPanel
     */
    void SetScreenProps(Screen* screen, wxTreeItemId treeItem, WorkPanel* workPanel);
    /**
     * @brief Set property grid to selected shape
     * @param shape Pointer to shape
     * @param treeItem TreeItem object ID
     * @param screen Screen on which is shape located
     * @param workPanel WorkPanel associated with screen
     */
    void SetShapeProps(wxSFShapeBase* shape, wxTreeItemId treeItem, Screen* screen, WorkPanel* workPanel = NULL);
    /**
     * @brief If append size properties of shape
     * @param shape Rect shape whose size can be edited
     */
    void AppendSizeProps(wxSFRectShape* shape);
    /**
     * @brief Adding font properties for text
     * @param shape Shape pointer
     */
    void AppendFontProps(wxSFShapeBase* shape);
    /**
     * @brief Add event choosing properties for shape
     * @param shape Shape object
     */
    void AppendEventProps(wxSFShapeBase* shape);
    /**
     * @brief Add subproperties to chosen event
     * @param eventType Type of event
     * @param property Pointer to property which will contain subproperties
     */
    void AppendEventSubProps(EventType eventType, wxPGProperty* property);
    /**
     * @brief Add screen change event property
     * @param property Property to which append as subproperty
     */
    void AppendScreenChangeEventProp(wxPGProperty* property);
    /**
     * @brief Add bitmap change property
     * @param property Property to which append as subproperty
     */
    void AppendBitmapChangeEventProp(wxPGProperty* property);
    /**
     * @brief Get event types of shape
     * @param shape Shape object
     * @return Array of event types
     */
    wxArrayInt GetEventTypes(wxSFShapeBase* shape);

};

#endif // PROPGRID_H
