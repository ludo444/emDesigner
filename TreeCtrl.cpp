/***************************************************************
 * Name:      TreeCtrl.cpp
 * Purpose:   Implements TreeCtrl class
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   05/02/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#include "TreeCtrl.h"

#include "main.h"

wxTreeItemId TreeCtrl::s_lastRightClickedItem = wxTreeItemId();

bool TreeCtrl::s_treeCtrlDeleteActivated = false;

TreeCtrl::TreeCtrl(wxWindow *parent, const wxSize& size) : wxTreeCtrl(parent, wxID_ANY, wxDefaultPosition, size, wxTR_DEFAULT_STYLE|wxTR_HIDE_ROOT)
{
    m_mainFrame = (MainFrame*) wxGetApp().GetTopWindow();
    m_propgrid = m_mainFrame->GetElementsPropgrid();

    this->Connect(wxEVT_COMMAND_TREE_ITEM_ACTIVATED, wxTreeEventHandler(TreeCtrl::OnTreeItemActivation), NULL, this);
    this->Connect(wxEVT_COMMAND_TREE_ITEM_RIGHT_CLICK, wxTreeEventHandler(TreeCtrl::OnTreeItemRightClick), NULL, this);
    this->Connect(wxEVT_COMMAND_TREE_SEL_CHANGED, wxTreeEventHandler(TreeCtrl::OnTreeItemSelection), NULL, this);
}

TreeCtrl::~TreeCtrl()
{
    this->Disconnect(wxEVT_COMMAND_TREE_ITEM_ACTIVATED, wxTreeEventHandler(TreeCtrl::OnTreeItemActivation), NULL, this);
    this->Disconnect(wxEVT_COMMAND_TREE_ITEM_RIGHT_CLICK, wxTreeEventHandler(TreeCtrl::OnTreeItemRightClick), NULL, this);
    this->Disconnect(wxEVT_COMMAND_TREE_SEL_CHANGED, wxTreeEventHandler(TreeCtrl::OnTreeItemSelection), NULL, this);
}

void TreeCtrl::SetProject(Project* project)
{
    wxTreeItemId root = AddRoot(project->GetName());

    const SerializableList& screenList = project->GetScreenList();
    for(SerializableList::const_iterator i = screenList.begin(); i != screenList.end(); i++)
    {
        Screen* screen = (Screen*) *i;
        wxTreeItemId screenItem = AppendItem(root, screen->GetName(), -1, -1, new TreeItem(screen));

        SerializableList shapeList = screen->GetDiagramManager()->GetRootItem()->GetChildrenList();
        for(SerializableList::const_iterator i = shapeList.begin(); i != shapeList.end(); i++)
        {
            wxSFShapeBase* shape = (wxSFShapeBase*) *i;
            AppendItem(screenItem, ((CommonShapeData*) shape->GetFirstChild())->GetName(), -1, -1, new TreeItem(shape, screen));
        }

        if(project->GetIndexScreenId() == screen->GetId())
        {
            m_mainFrame->NewScreenTab(screen, screenItem);
            SelectItem(screenItem);
        }
    }
}

void TreeCtrl::OnTreeItemActivation(wxTreeEvent& event)
{
    TreeItem* data = (TreeItem*) GetItemData(event.GetItem());
    switch(data->GetType())
    {
        case TYPE_SHAPE:
        case TYPE_BITMAP_SHAPE:
        {
            wxSFShapeBase* shape = data->GetShape();
            shape->Select(true);
            shape->Refresh();
        }
        break;

        case TYPE_SCREEN:
        {
            WorkPanel* workPanel = m_mainFrame->NewScreenTab(data->GetScreen(), event.GetItem());
            m_propgrid->SetScreenProps(data->GetScreen(), event.GetItem(), workPanel);
        }
        break;

        default:
            break;
    }
}

void TreeCtrl::OnTreeItemSelection(wxTreeEvent& event)
{
    if(!s_treeCtrlDeleteActivated)
    {
        TreeItem* data = (TreeItem*) GetItemData(event.GetItem());
        switch(data->GetType())
        {
            case TYPE_SHAPE:
            case TYPE_BITMAP_SHAPE:
            {
                TreeItem* screenData = (TreeItem*) GetItemData(GetItemParent(event.GetItem()));
                m_propgrid->SetShapeProps(data->GetShape(), event.GetItem(), data->GetScreen(), screenData->GetWorkPanel());
            }
            break;

            case TYPE_SCREEN:
                m_propgrid->SetScreenProps(data->GetScreen(), event.GetItem(), data->GetWorkPanel());
                break;

            default:
                break;
        }
    }
    else
        s_treeCtrlDeleteActivated = false;
}

void TreeCtrl::OnTreeItemRightClick(wxTreeEvent& event)
{
    wxTreeItemId itemId = event.GetItem();
    s_lastRightClickedItem = itemId;
    wxMenu menu;
    wxMenuItem* deleteMenuItem = new wxMenuItem(&menu, wxID_ANY, "Delete" , wxEmptyString, wxITEM_NORMAL);
    this->Connect(deleteMenuItem->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(TreeCtrl::OnDeleteTreeItem));
    menu.Append(deleteMenuItem);

    PopupMenu(&menu, wxDefaultPosition);
    this->Disconnect(wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(TreeCtrl::OnDeleteTreeItem));
}

void TreeCtrl::OnDeleteTreeItem(wxCommandEvent& event)
{
    s_treeCtrlDeleteActivated = true;
    TreeItem* itemData = (TreeItem*) GetItemData(s_lastRightClickedItem);

    switch(itemData->GetType())
    {
        case TYPE_SHAPE:
        case TYPE_BITMAP_SHAPE:
            itemData->GetScreen()->GetDiagramManager()->RemoveShape(itemData->GetShape());
            break;

        case TYPE_SCREEN:
        {
            m_mainFrame->DeleteCanvasAuiNotebookPage(itemData->GetWorkPanel());
            m_mainFrame->GetProject()->RemoveChild(itemData->GetScreen());
        }
        break;

        default:
            break;
    }

    m_mainFrame->GetProject()->SetModified(true);
    m_propgrid->Clear();
    Delete(s_lastRightClickedItem);
}

wxTreeItemId TreeCtrl::GetShapeTreeItem(wxSFShapeBase* shape, wxTreeItemId screenId)
{
    wxTreeItemId item = GetLastChild(screenId);

    while(item)
    {
        TreeItem* itemData = (TreeItem*) GetItemData(item);
        if(itemData->GetShape() == shape)
            return item;
        item = GetPrevSibling(item);
    }

    return NULL;
}

void TreeCtrl::ProcessScreenShapes(wxTreeItemId screenId, Screen* screen)
{
    wxTreeItemId item = GetLastChild(screenId);
    while(item)
    {
        TreeItem* itemData = (TreeItem*) GetItemData(item);

        // if diagram doesn't contain item shape, remove it
        if(!screen->GetDiagramManager()->Contains(itemData->GetShape()))
        {
            wxTreeItemId removeItem = item;
            item = GetPrevSibling(item);
            Delete(removeItem);
        }
        else
            item = GetPrevSibling(item);
    }

    SerializableList shapeList = screen->GetDiagramManager()->GetRootItem()->GetChildrenList();
    for(SerializableList::const_iterator i = shapeList.begin(); i != shapeList.end(); i++)
    {
        wxSFShapeBase* shape = (wxSFShapeBase*) *i;
        // if TreeCtrl doesn't contain diagram shape, add it
        if(!GetShapeTreeItem(shape, screenId))
            AppendItem(screenId, ((CommonShapeData*) shape->GetFirstChild())->GetName(), -1, -1, new TreeItem(shape, screen));
    }
}
