/***************************************************************
 * Name:      TreeItem.cpp
 * Purpose:   Implementation of TreeItem
 * Author:    Marek Hrušovský (hrusovsky.marek@gmail.com)
 * Created:   02/27/17
 * Copyright: Marek Hrušovský
 * License:   GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
 * Notes:
 **************************************************************/
#include "TreeItem.h"

TreeItem::TreeItem(Screen* screen, WorkPanel* workPanel) : wxTreeItemData(), m_type(TYPE_SCREEN), m_screen(screen), m_workPanel(workPanel)
{
}

TreeItem::~TreeItem()
{
}

TreeItem::TreeItem(wxSFShapeBase* shape, Screen* screen) : wxTreeItemData(), m_type(TYPE_SHAPE), m_screen(screen), m_shape(shape)
{
}
